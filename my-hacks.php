<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin">
    <img src="img/banners/my-hacks-banner.png" alt="">
</section>

<div class="banner-shadow-content text-center">
Listen to Season 1, 2 and 3 of My Hacks | Unboxing Advice. It features unplugged conversations with subject matter experts from the Tata group. 
</div>


<section class="section-episode-listing">
    <div class="container">
        <ul class="nav season-listing-navigation season-listing-blue">
            <li>
                <button class="active" id="season-1-tab" data-toggle="tab" data-target="#season-1" type="button">season 1</button>
            </li>
            <li>
                <button id="season-2-tab" data-toggle="tab" data-target="#season-2" type="button" role="tab">season 2</button>
            </li>
            <li>
                <button id="season-2-tab" data-toggle="tab" data-target="#season-3" type="button" role="tab">season 3</button>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="season-1">
                <div class="row">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                    <div class="col-12 col-sm-6 col-md-6 col-xl-4 mb-50">
                        <div>
                            <a href="my-hacks-episode" class="podcast-card podcast-card-blue">
                                <div class="podcast-card-image">
                                    <img src="img/backgrounds/my-hacks-user.png" class="img-fluid" alt="">
                                </div>
                                <div class="podcast-card-content-container">
                                    <div class="podcast-card-header">
                                        <img src="img/logo/podcast-my-hacks-logo.svg" alt="">
                                        <span class="se">S1: EP<?php echo $x; ?></span>
                                    </div>
                                    
                                    <div class="podcast-card-content ">
                                        <div>
                                            <h6 class="podcast-card-title">Anubhuti Banerjee</h6>
                                            <p class="podcast-card-description">Learning the ABC of LGBTQIA+ </p>
                                        </div>
                                    </div>
                                    
                                    <div class="podcast-card-footer">
                                        <span>Listen Now</span>&nbsp;&nbsp;
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                            <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                    <circle cx="9.5" cy="9.5" r="9.5" stroke="none"></circle>
                                                    <circle cx="9.5" cy="9.5" r="9" fill="none"></circle>
                                                </g>
                                                <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="tab-pane fade" id="season-2">
                <div class="row">
                    <?php for ($x = 1; $x <= 4; $x++) { ?>
                        <div class="col-12 col-sm-6 col-md-6 col-xl-4 mb-50">
                            <div>
                                <a href="my-hacks-episode" class="podcast-card podcast-card-blue">
                                    <div class="podcast-card-image">
                                        <img src="img/backgrounds/leadercraft-user.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="podcast-card-content-container">
                                        <div class="podcast-card-header">
                                            <img src="img/logo/podcast-my-hacks-logo.svg" alt="">
                                            <span class="se">S2: EP<?php echo $x; ?></span>
                                        </div>
                                        
                                        <div class="podcast-card-content ">
                                            <div>
                                                <h6 class="podcast-card-title">Anubhuti Banerjee</h6>
                                                <p class="podcast-card-description">Learning the ABC of LGBTQIA+ </p>
                                            </div>
                                        </div>
                                        
                                        <div class="podcast-card-footer">
                                            <span>Listen Now</span>&nbsp;&nbsp;
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                                <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                    <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none"></circle>
                                                        <circle cx="9.5" cy="9.5" r="9" fill="none"></circle>
                                                    </g>
                                                    <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                </div>
            </div>

            <div class="tab-pane fade" id="season-3">
                <div class="row">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                    <div class="col-12 col-sm-6 col-md-6 col-xl-4 mb-50">
                        <div>
                            <a href="my-hacks-episode" class="podcast-card podcast-card-blue">
                                <div class="podcast-card-image">
                                    <img src="img/backgrounds/my-hacks-user.png" class="img-fluid" alt="">
                                </div>
                                <div class="podcast-card-content-container">
                                    <div class="podcast-card-header">
                                        <img src="img/logo/podcast-my-hacks-logo.svg" alt="">
                                        <span class="se">S1: EP<?php echo $x; ?></span>
                                    </div>
                                    
                                    <div class="podcast-card-content ">
                                        <div>
                                            <h6 class="podcast-card-title">Anubhuti Banerjee</h6>
                                            <p class="podcast-card-description">Learning the ABC of LGBTQIA+ </p>
                                        </div>
                                    </div>
                                    
                                    <div class="podcast-card-footer">
                                        <span>Listen Now</span>&nbsp;&nbsp;
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                            <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                    <circle cx="9.5" cy="9.5" r="9.5" stroke="none"></circle>
                                                    <circle cx="9.5" cy="9.5" r="9" fill="none"></circle>
                                                </g>
                                                <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="section-suggested-podcast-category">
    <div class="container">
        <header class="section-header section-header-sm text-center mb-4">Also Listen To</header>
        <div class="row justify-content-center">
            <div class="col-6 col-md-4 mb-3">
                <a href="wonderful-collective" class="podcast-category-card">
                    <img src="img/cards/lt-onederful-collective.svg" class="img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</div>

<?php include "components/footer.php" ?>