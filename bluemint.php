<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/bluemint-banner.png" class="d-none d-lg-block" alt="">
    <img src="img/banners/bluemint-banner-mobile.jpg" class="d-block d-lg-none" alt>
</section>

<div class="banner-shadow-content text-center">
    Blue Mint is a 6-month leadership journey to raise the angle of ascent of the high-potential early career talent within the Tata group. Blue Mint enables a combination of classroom and experiential learning to help you develop capabilities in line with the <strong>Future ready</strong> and <strong>Future engaged</strong> Tata group talent architecture. It includes marquee academic capability building with the London Business School and several inimitable experiences including cross-functional capstone projects, reverse mentoring opportunities and much more
</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list bluemint">
            <li>
                <a class="hash-link" href="#background">Background</a>
            </li>
            <li>
                <a class="hash-link" href="#aim">Aim</a>
            </li>
            <li>
                <a class="hash-link" href="#structure">Structure</a>
            </li>
            <li>
                <a class="hash-link" href="#key-outcomes">Key Outcomes</a>
            </li>
            <li class="border-md-0">
                <a class="hash-link" href="#highlights">Highlights</a>
            </li>
            <li>
                <a class="hash-link" href="#leader-profile">Leader Profile</a>
            </li>
            <li>
                <a class="hash-link" href="#goals">Goals</a>
            </li>
            <li>
                <a class="hash-link" href="#application">Application</a>
            </li>
            <li>
                <a class="hash-link" href="#faculty">Faculty</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-objectives section-bluemint-objective" id="background">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-md-center">
                <header class="section-header mb-4">Objectives</header>
                <img src="img/backgrounds/bars-blumint-sm.svg" class="img-fluid pb-4" data-aos="fade-in">
            </div>
            <div class="col-md-8">
                <p>Blue Mint seeks to proactively engage, prepare and advance the voices of early career talent, next gen leaders, consumers and stakeholders into the fabric of the Tata groups’ journey. </p>
                <p>Blue Mint is going to be a completely virtual program, so that participants anywhere can have access to it. There will be one (optional) physical touchpoint during the program for participants to come together as the uncertainty around Covid settles in the future. The learning, social and networking elements have undergone an innovative design to ensure connectedness and high engagement through the program. </p>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-aim" id="aim">
    <img src="img/backgrounds/bluemint-objective-bg.svg" class="bm-objectives-element-1 duration-1s" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header text-center">Aim</header>
            </div>
            <div class="col-md-8 col-lg-7 bg-white pb-md-5 position-relative" style="z-index:2;">
                <ul class="hexagon-list">
                    <li>
                        <img src="img/icons/aim1.svg" alt="">
                        <p>Inspire early career leaders with the latest evidence-based actionable insights as well as build transferable skills and agility across a wide spectrum of future fit capabilities.</p>
                    </li>
                    
                    <li>
                        <img src="img/icons/aim2.svg" alt="">
                        <p>Provide Group wide immersions and experiences tailored to instilling diverse perspectives of business and leadership.</p>
                    </li>
                    
                    <li>
                        <img src="img/icons/aim3.svg" alt="">
                        <p>Give them the tools and the felicity to take ownership of their leadership journey.</p>
                    </li>
                    
                    <li>
                        <img src="img/icons/aim4.svg" alt="">
                        <p>Help them connect strategically with the Tata brand and purpose across the generations to jointly shape its future direction</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-structure" id="structure">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header text-light">Structure</header>
            </div>
            <div class="col-md-9 col-lg-8">
                <p>A mix of ‘learning’ and ‘doing’ pedagogies comprise the structure of Blue Mint. It is about putting the levers in your hands by increasing personal and professional growth by going through the life changing opportunities and creating maximum visibility across the group. It is a completely virtual program with an (optional) physical touchpoint to come together as the uncertainty around Covid settles in the future.</p>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-outcomes" id="key-outcomes">
    <div class="container">
        <div class="col-lg-4">
            <header class="section-header">Key Outcomes</header>
        </div>
        <div class="row">
            <div class="col-md-6 pr-md-5">
                <ul class="dash-list">
                    <li>
                        <p>Equip yourself with a range of leadership and strategic tools and frameworks to help you accelerate your work performance through actionable insights.</p>
                    </li>
                    
                    <li>
                        <p>Learn how your leadership behaviors and interactions with others can be improved by boosting your ability to be your best self in the workplace.</p>
                    </li>
                </ul>
            </div>
            
            <div class="col-md-6 pr-md-5">
                <ul class="dash-list">
                    <li>
                        <p>Enhance your sense of ownership of towards problems, challenges and opportunities.</p>
                    </li>
                    
                    <li>
                        <p>Develop an agile entrepreneurial mindset, driven by short experiments that push you out of your comfort zone.</p>
                    </li>
                    
                    <li>
                        <p>View the bigger picture encompassing the key issues and opportunities across the Tata group.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-highlight" id="highlights">
    <div class="container-fluid px-0">
        <!-- <header class="section-header">Hightlight</header> -->
        <img src="img/backgrounds/bluemint-highlights.png" class="img-fluid d-none d-md-block" alt="">
        <img src="img/backgrounds/bluemint-highlights-mobile.png" class="img-fluid d-md-none w-100" alt="">
    </div>
</section>

<section class="section-bluemint-for" id="leader-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header">Who is <br class="d-none d-md-block">BlueMint for?</header>
            </div>
            <div class="col-md-8">
                <div class="global-content-width-650">
                    <p>Blue Mint is for Tata learners with a recommended work experience of 4-10 years. You need to be highly motivated as is demonstrated by your work or involvement with other communities. </p>
                    <p>You should already have a framework of experience against which to apply your education, and understand workplace issues, in order to get more out of the rigorous program. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-looking-for" id="goals">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header">What are we <br class="d-none d-md-block">looking for?</header>
                <img src="img/backgrounds/bars-mint.svg" class="img-fluid pb-4" data-aos="fade-in">
            </div>
            <div class="col-md-8">
                
                <div class="global-content-width-600">
                    <ul class="hexagon-list">
                        <li>
                            <span class="hexagon-background">1</span>
                            
                            <p>Your preparation and potential to succeed.</p>
                            
                        </li>
                        <li>
                            <span class="hexagon-background">2</span>
                            <p>To see you challenge yourself and to do very well.</p>
                        </li>
                        <li>
                            <span class="hexagon-background">3</span>
                            <p>Your commitment, dedication, and passion in expanding your intellectual horizons.</p>
                        </li>
                        <li>
                            <span class="hexagon-background">4</span>
                            <p>The initiative with which you seek out opportunities and <br> expand your perspective.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-application-process" id="application">
    <div class="container">
        
        <div class="row">
            <div class="col-md-9 col-lg-7 mx-auto text-center mb-30">
                <header class="section-header mb-4">What is the Application Process?</header>
                <pv>‘Blue mint’ will be an open-application process. Tata employees with suggested 4-10 years of overall experience will be eligible to self-apply for the program and go through the process.</pv>
            </div>
        </div>
        
        <ul class="inline-icon-list">
            <li>
                <div class="inline-icon-list-image">
                    <img src="img/backgrounds/application-list.svg" height="75" width="75" alt="">
                </div>
                <div class="inline-icon-list-content">
                    <p class="mb-0">Online <br> application form</p>
                </div>
            </li>
            
            <li>
                <div class="inline-icon-list-image">
                    <img src="img/icons/application2.svg" height="75" width="75" alt="">
                </div>
                <div class="inline-icon-list-content">
                    <p class="mb-0">Leadership Simulation <br> on motivations and capabilities </p>
                </div>
            </li>
            
            <li>
                <div class="inline-icon-list-image">
                    <img src="img/icons/application3.svg" height="75" width="75" alt="">
                </div>
                <div class="inline-icon-list-content">
                    <p class="mb-0">Interview with Tata <br> leaders across the group</p>
                </div>
            </li>
            
            <li>
                <div class="inline-icon-list-image">
                    <img src="img/icons/application4.svg" height="75" width="75" alt="">
                </div>
                <div class="inline-icon-list-content">
                    <p class="mb-0">Offer made to join the programme</p>
                </div>
            </li>
        </ul>
    </div>
</section>

<section class="section-mint">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-4">
                <header class="section-header text-light mb-3">Who should <br> write my letter of <br>recommendation?</header>
            </div>
            <div class="col-md-7 col-lg-8">
                <div class="global-content-width-650">
                    <p>Recommenders should be people who know you best and can best speak to your leadership development. They should be people who are in supervisory positions to you. You may ask a client, a manager, or a mentor, for a recommendation. We want to understand your accomplishments relative to your peers.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-approach">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-4">
                <header class="section-header">How should <br class="d-none d-md-block">you approach <br>the application?</header>
            </div>
            <div class="col-md-7 col-lg-8">
                <div class="global-content-width-650">
                    <p>Paint a clear picture of yourself using a mix of personal and professional examples that best answer the questions. We highly encourage you to use recent examples in your cover letter. You should limit examples to those within the past three years, if possible. </p>
                    <p>Past performance is the best predicator of future performance and we look forward to learning more about your experiences and achievements! A variety of Tata folks will be a part of the selection process and are looking to get to know you better. We encourage you to be authentic.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-faculty" id="faculty">
    <div class="container">
        <header class="section-header text-center">Faculty</header>
        
        <div class="row justify-content-center">
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac1.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Herminia Ibarra</header>
                        <p class="mb-0 text-muted">Charles Handy Chair in Organisational Behaviour; Professor of Organisational Behaviour MA PhD (Yale)</p>
                    </div>
                </div>
            </div>
            
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac2.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Ionnis Ioanou</header>
                        <p class="mb-0 text-muted">Associate Professor of Strategy and Entrepreneurship BA (Yale) MA PhD (Harvard)</p>
                    </div>
                </div>
            </div>
            
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac3.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Kathleen O’Connor</header>
                        <p class="mb-0 text-muted">Professor of Organisational Behaviour, Director of Executive Education BC (Cornell) AM PhD (Illinois)</p>
                    </div>
                </div>
            </div>
            
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac4.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Costas Markides</header>
                        <p class="mb-0 text-muted">Robert P Bauman Chair of Strategic Leadership, Professor of Strategy and Entrepreneurship BA, MA (Boston University), MBA, DBA (Harvard Business School)</p>
                    </div>
                </div>
            </div>
            
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac5.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Julian Birkinshaw</header>
                        <p class="mb-0 text-muted">Professor of Strategy and Entrepreneurship BSc (Durham) MBA PhD (Western Ontario)</p>
                    </div>
                </div>
            </div>
            
            <div class="col-6 col-md-4 col-lg-2 px-md-2 mb-4">
                <div class="shadow-card text-center mb-0 h-100 disable-hover-effect">
                    <div class="shadow-card-content p-3 fs-14">
                        <div class="shadow-card-image">
                            <img src="img/backgrounds/bm-fac6.png" class="img-fluid" alt="">
                        </div>
                        <header class="shadow-card-header" style="margin:15px 0 10px 0;">Keyvan Vakili</header>
                        <p class="mb-0 text-muted">Associate Professor of Strategy and Entrepreneurship BA MBA (Sharif) PhD (Toronto)</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

<section class="section-bluemint-gallery">
    <div class="container-fluid">
        <header class="section-header text-center mb-4">Blue Mint Gallery</header>
        <nav class="nav gallery-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-bluemint-gallery-image-tab" data-toggle="tab" data-target="#nav-bluemint-gallery-image" type="button" role="tab" aria-controls="nav-bluemint-gallery-image" aria-selected="true">Images</button>
            <button class="nav-link" id="nav-bluemint-gallery-video-tab" data-toggle="tab" data-target="#nav-bluemint-gallery-video" type="button" role="tab" aria-controls="nav-bluemint-gallery-video" aria-selected="false">Videos</button>
        </nav>
        
        <div class="tab-content gallery-tabs-content" id="nav-tabContent">
            <!-- IMAGE TAB CONTENT -->
            <div class="tab-pane fade show active" id="nav-bluemint-gallery-image" role="tabpanel" aria-labelledby="nav-bluemint-gallery-image-tab">
                <div class="gallery-image-slider dots-dashed">
                    <div class="gallery-image-slide">
                        <img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="">
                    </div>
                    
                    <div class="gallery-image-slide">
                        <img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            
            <!-- VIDEO TAB CONTENT -->
            <div class="tab-pane fade" id="nav-bluemint-gallery-video" role="tabpanel" aria-labelledby="nav-bluemint-gallery-video-tab">
                
                <div class="gallery-image-slider dots-dashed">
                    
                    <div>
                        <!-- gallery-video-card -->
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/bluemint-banner-mobile.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    
                    <div>
                        <!-- gallery-video-card -->
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/bluemint-banner-mobile.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-bluemint-team">
    <div class="container">
        <div class="bg-white">
            <header class="section-header mb-2 text-center">Meet the Team</header>
            <p class="section-description mb-5 text-center">If you are a potential <a href="" class="text-mint">Blue Minter</a> or know someone who is, contact us.</p>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team1.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Aparna Maroo Jain</h5>
                        <strong>Programme Director</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team2.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Ankna Kaul</h5>
                        <strong>Programme Coordinator</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team3.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Prathmesh Deshmukh</h5>
                        <strong>Programme Coordinator</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>