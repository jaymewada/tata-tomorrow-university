<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	 <img src="img/banners/our-world-banner.png">
</section>

<section class="section-page-nav">
	<div class="container">
		<div class="about-page-navigation-list">
			<ul class="page-navigation-list m-0">
				<li>
					<a href="about">Director’s Note</a>
				</li>
				<li>
					<a href="our-journey">Our Journey</a>
				</li>
				<li class="is-active">
					<a href="our-world">Our World</a>
				</li>
				<li>
					<a href="our-footprints">Our Footprints</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<section class="section-campus">
	<img src="img/backgrounds/gold-d-left.svg" width="40" class="campus-element-1" />
	<img src="img/backgrounds/gold-rod.svg" class="campus-element-2" data-aos="fade-in" />
	<div class="container">
		<header class="section-header text-center">A Campus that Charms</header>
		<div class="row">
			<div class="col-md-6">
				<div class="global-content-width-500">
					<p>
						The renowned architect George Wittet, who designed the Gateway of India in Mumbai, also designed the heritage
						19th century villa which became the home for TMTC in 1966. An elegant structure surrounded by 15 acres of lush
						greenery, it offers an oasis of calm amidst the bustling city of Pune.
					</p>

					<p>
						The rich history and old-world charm of TMTC are palpable within its walls, while our upgraded and renovated
						facilities make for a comfortable stay.
					</p>
				</div>
			</div>

			<div class="col-md-6">
				<div class="global-content-width-500">
					<p>
						The graceful central building is framed by mature gardens, heritage trees and shaded pathways that promise a
						peaceful setting that inspire thought, reflection and learning endeavours.
					</p>

					<p>
						Our centrally located dining room ensures guests can enjoy memorable meals without leaving the campus. A
						gymnasium, table tennis area, and badminton and tennis courts make the campus a retreat for the body and the
						mind.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-campus-tabs">
	<div class="container-fluid">
		<nav class="nav gallery-tabs" id="nav-tab" role="tablist">
			<button role="tab" type="button" data-toggle="tab" aria-selected="true" class="nav-link active" id="gallery-classroom-tab" data-target="#gallery-classroom" aria-controls="gallery-classroom">
				Classroom
			</button>
			<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="gallery-recreational-tab" data-target="#gallery-recreational" aria-controls="gallery-recreational">
				Recreational
			</button>
			<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="gallery-campus-tab" data-target="#gallery-campus" aria-controls="gallery-campus">
				Campus
			</button>

			<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="gallery-cafeteria-tab" data-target="#gallery-cafeteria" aria-controls="gallery-cafeteria">
				Cafeteria
			</button>
			<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="gallery-accommodation-tab" data-target="#gallery-accommodation" aria-controls="gallery-accommodation">
				Accommodation
			</button>
		</nav>

		<div class="tab-content gallery-tabs-content" id="nav-tabContent">
			<!-- CLASSROOM TAB -->
			<div class="tab-pane fade show active" id="gallery-classroom" role="tabpanel">
				<div class="gallery-image-slider dots-dashed has-image-caption">
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-1.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-2.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas unde rerum quos nulla sunt libero
									accusantium esse magni quo? Veritatis sed quae placeat dicta et, incidunt ratione blanditiis nisi
									maxime!
								</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-3.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-4.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-5.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-6.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/classroom-7.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Classroom</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- RECREATIONAL TAB -->
			<div class="tab-pane fade" id="gallery-recreational" role="tabpanel">
				<div class="gallery-image-slider dots-dashed has-image-caption">
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/recreational-1.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Recreational</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/recreational-2.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Recreational</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- CAMPUS TAB -->
			<div class="tab-pane fade" id="gallery-campus" role="tabpanel">
				<div class="gallery-image-slider dots-dashed has-image-caption">
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-1.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-2.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-3.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-4.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-5.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-6.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-7.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-8.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-9.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-10.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-11.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/campus-12.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Campus</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- CAFETERIA TAB -->
			<div class="tab-pane fade" id="gallery-cafeteria" role="tabpanel">
				<div class="gallery-image-slider dots-dashed has-image-caption">
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-1.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-2.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-3.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-4.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-5.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-6.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-7.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-8.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-9.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-10.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/cafeteria-11.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Cafeteria</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- ACCOMMODATION TAB -->
			<div class="tab-pane fade" id="gallery-accommodation" role="tabpanel">
				<div class="gallery-image-slider dots-dashed has-image-caption">
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-1.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-2.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-3.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-4.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>

					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-5.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-6.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-7.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-8.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-9.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
					<div>
						<div class="gallery-image-slide">
							<img src="img/backgrounds/accommodation-10.png" class="img-fluid" />
							<div class="gallery-image-caption">
								<p>Accommodation</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-repo">
	<div class="container">
		<img src="img/backgrounds/trending-now-slider-element-2.svg" class="repo-element-1" data-aos="fade-up" />
		<div class="row">
			<div class="col-12 col-md-11 col-lg-7">
				<header class="section-header text-light m-0">A Repository of Knowledge</header>
				<p class="my-4 global-content-width-500 ml-md-0">
					The TMTC Library, Gyansarovar, is considered one of the finest management libraries in India, with a
					collection built over three decades. It has over 14,000 titles, and subscribes to 25 specialist Indian and
					international journals, as well as popular magazines and electronic databases. Visitors can peruse its
					treasures on campus, while employees around the country can requisition books from the library at their
					location, which arrive complete with a stamped envelope for seamless return.
				</p>
				<a href="" class="btn-cta-navy" style="width: 200px">Visit site</a>
			</div>
		</div>
	</div>
</section>

<!-- <section class="section-people">
	<img src="img/backgrounds/gold-d-left.svg" width="40" class="people-element-1" />

	<div class="container">
		<div class="text-center global-content-width-650 mb-md-5">
			<header class="section-header mb-4">Our People</header>
			<p class="mb-4">
				TMTC is today a hallowed centre of learning as much for the vision of the founders as the efforts of its people.
				With their diverse talents and backgrounds, people are at the very heart of TMTC. The campus is a labour of love
				and a testament to the dedication of many teams, from faculty to administration, kitchen staff to garden staff,
				programme directors to library staff. Team TMTC works together not only deliver value to the Tata group but also
				lovingly preserve the heritage campus and the values it stands for.
			</p>
		</div>
	</div>

	<nav class="nav gallery-tabs our-people-tabs" id="nav-tab" role="tablist">
		<button role="tab" type="button" data-toggle="tab" aria-selected="true" class="nav-link active" id="people-leadership-tab" data-target="#people-leadership" aria-controls="people-leadership">
			Leadership
		</button>

		<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="people-management-tab" data-target="#people-management" aria-controls="people-management">
			Management
		</button>

		<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="people-support-tab" data-target="#people-support" aria-controls="people-support">
			Support
		</button>

		<button role="tab" type="button" data-toggle="tab" class="nav-link" aria-selected="false" id="people-hospitality-tab" data-target="#people-hospitality" aria-controls="people-hospitality">
			Hospitality
		</button>
	</nav>

	<div class="tab-content gallery-tabs-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="people-leadership" role="tabpanel">
			<div class="people-slider dots-dashed">
				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/leadership-ccs.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Lt Col Cyril Satur (Retd.)</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Lt Col Cyril Satur (Retd) joined TMTC in 2021, post his premature retirement from the Indian Army. He has operational and HR experience from his years in the defence forces, and an Executive MBA from IIM Kolkata. He is responsible for all HR functions and administration at TMTC. Outside of work, Cyril is a travel buff, especially keen on angling trips and places of historical significance around the world.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/leadership-pg.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Priyadarshini Gupta</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									An alumnus of Tata Institute of Social Sciences, Priyadarshini has been working with the Tata group since 2019, leading the areas of inclusion, equity and belonging. She has been working in the space of organisational effectiveness and building inclusive work cultures for two decades. Besides collaborating with diverse people, Priyadarshini enjoys spending time with her cats and exploring new places in the world.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/leadership-rbs.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Rhea Bulsara Sidhwa</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Rhea has been with the Tata group since 2016. With a Masters in Human Resources, she leads the People Enablement vertical at Group HR. Rhea is responsible for creating a coaching culture across Tata group companies. When she is not immersed in curating custom talent interventions and coaching offerings, Rhea can be found trying out new restaurants or travelling with her family and friends.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade " id="people-management" role="tabpanel">
			<div class="people-slider dots-dashed">
				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-mb.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Mousumi Baruah</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Mousumi has been with the Tata group since January 2022. With a background in Economics and Development Studies, she looks after the research vertical at TMTC. When she’s not immersed in creating the research newsletter or case studies, Mousumi can be found trekking or hiking in the great outdoors, or binge-watching her favourite shows.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-sn.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Sanjay Nayak</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Sanjay joined the Tata group in 2017. He hails from a Library Science & Knowledge Management background and looks after Knowledge Management, Learner Experience and Enterprise Learning at TMTC. Besides work, Sanjay enjoys exploring new places, swimming, reading, and meeting people from different professions.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-bp.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Benaifer Parva</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Benaifer joined TMTC in 2021. She is a Master of Business Administration, with a specialisation in Finance, and works as a Programme Co-ordinator in the Programme Management team. An adventurous soul, Benaifer loves travelling and exploring new places, and escaping into the pages of a good novel. She also enjoys different foods and the company of friends and family.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-aj.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Abhishek Joshi</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Abhishek joined the Programme Management team at TMTC in 2022. He has studied Organisational Development and Compensation Management, and is well-versed in managing employee relations, training & development, and performance management. In his free time, Abhishek likes playing badminton, watching mythological and science fiction stories, and listening to classical fusion and rock music.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-rd.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Ruby Dutia</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Ruby has been with TMTC since 2022. She works as a Programme Co-ordinator in the Programme Management team. An avid sportsperson, Ruby treks, plays badminton, and has played hockey at the national level. Ruby is keen to learn new things and explore new places, whenever the opportunity arises.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-zi.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Zeeba Irani</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Zeeba joined TMTC as Executive Assistant to the Director in 2021. She has experience of more than 19 years in office assistance, customer relationship management, query handling and customer delight. She is a team player, known for her ability to collate and analyse data. A Master of Commerce and a Bachelor of Education, Zeeba likes to crochet and read in her leisure time.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-in.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Iravati Nair</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Iravati has been a part of the Tata group since 2015. With a background in industrial psychology and instructional design, she is working as a Programme Manager at TMTC. Her areas of interest include programme design and execution, audience engagement through instructional design methodologies, HR analytics, and talent development. When she is free, Iravati enjoys volunteering and being outdoors- trekking, gardening and spending time with animals.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-pk.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Prerna Koppiker</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Prerna joined TMTC as a Programme Manager in 2022, armed with a Master’s in Industrial Psychology, a PGDBA in Human Resources, and a certification in Belbin Team Roles Assessment. An experienced HR professional, Prerna is also a Reebok certified fitness trainer. She has a love of dance that has led her to learn 13 different dance forms, including Bharatnatyam. She has even studied Hindustani classical music, in her early years.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-dc.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Dipesh Chotaliya</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Dipesh joined TMTC in 2022, bringing a wealth of experience from his 12-year career in managerial work, office administration, planning and logistics. He has a background in management studies, and excels at creative thinking, in the boardroom and outside. Dipesh is passionate about nature, metaphysics and philosophy. In his spare time, he collects antiques and stones.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-sk.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Sarah Khan</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Sarah joined TMTC as Manager - Content Curation & Programmes in May 2021. She oversees the development and implementation of specialised training programmes and actively participates in the creation of self-paced learning resources. She also contributes to TMTC research interventions. Sarah has a Master’s in European Studies and Management, and certification in Belbin Team Roles Assessment. In her leisure time, she enjoys playing the piano and travelling.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-cd.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Cyrus Dorabjee</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Cyrus joined TMTC in 2018. With a degree in hotel management, he leads a team of 38 support staff members as Facility Team Lead at the Centre. A believer in customer centricity, Cyrus is committed to providing the best hospitality on campus. When he is not busy managing operations at TMTC, Cyrus enjoys playing different sports and travelling.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-vj.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Vijay Jadhav</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Vijay has been with the Tata group since 2017. Armed with a Master’s degree in Advanced Accounting and Taxation, he looks after vendor management, vendor payments, faculty payments, foreign payment & accounts, finance, and taxation at TMTC. In his leisure time, Vijay likes playing cricket, badminton, and table tennis, and going on treks.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/profile-empty.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Mitchelle Anthony</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Mitchelle joined the Tata group in 2012. She has an MBA in Banking and Finance and works on the design and development of leadership programmes, as Programme Manager. Her networking skills have led to strong client relationships and fruitful engagements with the Talent Development community in the Tata group. Mitchelle’s a creative soul, who loves baking, travelling and exploring.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/profile-empty.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Sunayana Kolharkar</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Sunayana has been with TMTC since 2010 and currently serves as Deputy Manager - Programme Coordination. A Master of Business Administration in Human Resources, she has worked in the area of Competency Framework Development. Sunayana obtained her Belbin Accreditation as well, while managing several programmes at TMTC. In her spare time, she enjoys music, mandala drawings, experimenting in other forms of art & craft, and playing badminton.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/profile-empty.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Hokesh Ovhal</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Hokesh has worked with TMTC since 2011 as Programme Coordinator. He graduated in English but his interest in the creative field goes beyond that to arts and crafts. In his spare time, Hokesh also enjoys listening to music.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/profile-empty.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Shweta Pradeep Singid</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Shweta has been with TMTC since 2010 and is currently the Senior Team Leader - Programme Coordination. She holds a Bachelor’s degree in Commerce from the University of Pune. She recently received her Belbin Accreditation certificate and is now a Belbin Ninja. When at leisure, Shweta can be found in the kitchen, making different types of biryanis, chocolates or tea cakes.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-rs.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Renuka Shetty</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Since she joined the Tata group in January 2017, Renuka has worked extensively in the diversity, equity & inclusion space. In her current role at Group HR, she also leads Vitality, the Group’s well-being initiative. She has been instrumental in building capability programmes and communication to enable inclusivity at work, while also designing some flagship women leadership programmes. Renuka is a foodie and an artist, when at leisure.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-pj.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Prakamya Joshi</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Prakamya has been with the Tata group since June 2021. With a post-graduate degree in Human Resource Management from XLRI Jamshedpur, he looks after People Enablement at Group HR. Apart from creating high-impact learning interventions for the group, his interests include reading up about electronics, business, finance and scuba diving.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-pd.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Prathmesh Deshmukh</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Prathmesh has been with the Tata group since 2022 in the Student Outreach & Youth Engagement team. His primary role is to engage and develop talent, both internal and external, via planned development interventions and global internships. Prathmesh comes from an engineering background, and has an MBA in Human Resource Management. In his leisure time, he enjoys travelling and exercising.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/management-ak.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Ankna Kaul</div>
								<strong>Faculty or Expert Designation</strong>
								<p>
									Ankna has been with Group HR since 2019. She works in the space of leadership and talent development, for projects like Blue Mint and Tata Global internships. Besides a background in Hospitality and Hotel Management and a post-graduate diploma in Business Management, Ankna comes with 14 years of experience in operations and service excellence.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="people-support" role="tabpanel">
			<div class="people-slider dots-dashed">
				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/support-vj.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Vijay Jadhav</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Vijay has been with the Tata group since 2017. Armed with a Master’s degree in Advanced Accounting and Taxation, he looks after vendor management, vendor payments, faculty payments, foreign payment & accounts, finance, and taxation at TMTC. In his leisure time, Vijay likes playing cricket, badminton, and table tennis, and going on treks.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/support-nk.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Nikita Kulkarni</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Nikita joined TMTC in 2022. With 10 years’ experience in hospitality, and a Diploma in Food Technology from SNDT University, Mumbai, she has several administrative responsibilities at the Centre. Nikita loves interacting with visitors, and spends her leisure time experimenting with cooking different cuisines.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/support-gp.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Girish Potdar</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Girish joined TMTC as IT Manager in 2009. Now the head of the IT department, Girish’s knowledge range includes designing IT and Audio/Video (AV) solutions for classrooms, data migration, implementing process automation, IT infrastructure development, and introducing new technology in the area of AV & IT. In his free time, he enjoys music, films, dancing and travelling.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/support-gp.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Girish Potdar</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Girish joined TMTC as IT Manager in 2009. Now the head of the IT department, Girish’s knowledge range includes designing IT and Audio/Video (AV) solutions for classrooms, data migration, implementing process automation, IT infrastructure development, and introducing new technology in the area of AV & IT. In his free time, he enjoys music, films, dancing and travelling.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="people-hospitality" role="tabpanel">
			<div class="people-slider dots-dashed">
				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/hospitality-ak.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Alok Keshri</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Alok has been with TMTC since 1996. With a background in hotel management, he is Head of Food & Beverages and Housekeeping services at TMTC. Alok manages the entire facilities staff, the kitchen and the stay arrangements of guests. His belief in “service with a smile” has him forging lasting connections with visitors. When at leisure, Alok enjoys badminton, reading, and spending time with family.
								</p>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div class="people-slider-card">
						<div class="vertical-card mx-auto" style="max-width: 340px">
							<img src="img/backgrounds/hospitality-cd.png" class="img-fluid" />
							<div class="vertical-card-content text-center">
								<div class="vertical-card-header">Cyrus Dorabjee</div>
								<strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</strong>
								<p>
									Cyrus joined TMTC in 2018. With a degree in hotel management, he leads a team of 38 support staff members as Facility Team Lead at the Centre. A believer in customer centricity, Cyrus is committed to providing the best hospitality on campus. When he is not busy managing operations at TMTC, Cyrus enjoys playing different sports and travelling.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="section-learners">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<header class="section-header m-0 text-light">Our Learners</header>
				<img src="img/backgrounds/bars-gold.svg" class="my-3" data-aos="fade-in" />
			</div>
			<div class="col-md-8">
				<div class="global-content-width-550 ml-0">
					<ul class="dash-list">
						<li>
							<p>
								TMTC partners leaders of today and tomorrow in their journey to become the best versions of themselves.
								Whether they are executives who show potential and ambition or leaders who need support in a turbulent
								business environment, at TMTC, they receive the tools, knowledge and support they need to lead
								effectively, and in alignment with the Tata ethos.
							</p>
						</li>
						<li>
							<p>
								Our learning initiatives, catering to leaders at various levels, cover all areas of leadership, right
								from strategic to functional to emerging areas. The programmes are designed to instil Tata values and
								business acumen in potential leaders, from the early phase of their leadership journey to the C-suite
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-about-partners">
	<img src="img/backgrounds/gold-d-right.svg" width="40" class="about-partners-element-1" />
	<div class="container">
		<div class="global-content-width-700 text-center">
			<header class="section-header mb-4">Our Partners</header>
			<p>
				For decades, Tata Management Training Centre has been synonymous with excellence in business learning. We have
				been supported in this journey by esteemed partners like Harvard Business School, London Business School,
				Michigan Ross, Carnegie Mellon University, the Indian School of Business, the Society for Human Resource
				Management and many more.
			</p>
		</div>

		<div class="partners-logo-grid">
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-1.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-2.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-3.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-4.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-5.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-6.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-7.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-8.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-9.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-10.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-11.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-12.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-13.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-14.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-15.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-16.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-17.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-18.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-19.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-20.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-21.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-22.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-23.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-24.svg" class="img-fluid" />
			</div>
			<div class="partners-logo-container">
				<img src="img/logo/digital-partner-25.svg" class="img-fluid" />
			</div>
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>