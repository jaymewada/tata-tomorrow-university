<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/assesment-development-center-banner.png" alt="">
</section>

<div class="banner-shadow-content text-center">
    TMTC brings you a multi-disciplinary webinar series designed in collaboration with thought leaders and domain experts. These are carefully curated sessions that offer powerful insights on themes that are critical to business success and personal development.
</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list bluemint">
            <li>
                <a class="hash-link" href="#about">About</a>
            </li>
            <li>
                <a class="hash-link" href="#aim">Aims</a>
            </li>
            <li>
                <a class="hash-link" href="#structure">Partnerships</a>
            </li>
            <li>
                <a class="hash-link" href="#our-approach">Our Approach</a>
            </li>
            <li>
                <a class="hash-link" href="#spotlight">Spotlight</a>
            </li>
            <li>
                <a class="hash-link" href="#leader-profile">Team</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-dc-about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header m-0">About</header>
                <img src="img/backgrounds/bars-sm-brown.svg" class="img-fluid my-4" data-aos="fade-in">
            </div>
            <div class="col-md-9 col-lg-8">
                <p>Continuous learning is the cornerstone of transformational growth of individuals and organisations, whether it is through the acquisition of new skills and capabilities or enhancement of competencies. Even the most accomplished leaders have areas in which they need support. The Assessment and Development Centre provides this support in the form of custom solutions for their growth and development, aligned with the company’s specific requirements and long-term agenda</p>
            </div>
        </div>
    </div>
</section>

<section class="section-dc-aims" id="aim">
    <img src="img/backgrounds/ac-dc-aim-element-1.svg" class="ac-dc-aim-element-1" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">Aim</header>
            </div>
            <div class="col-md-9 col-lg-8">
                <div class="global-content-width-675 ml-0">
                    
                    <ul class="dot-list brown-dots">
                        <li>Assist organisations in reaching their business goals by identifying the leadership resources worthy of investment and maximising the abilities of such leaders</li>
                        <li>Provide insights into how they compare to similar organisations through global benchmarking.</li>
                        <li>Identify the strengths and areas for improvement in their selected leaders.</li>
                        <li>Develop high-impact learning journeys for their specific capability or competency development needs that set them on the trajectory of growth.</li>
                        <li>Support succession planning by providing an accurate assessment of leadership potential.</li>
                        <li>Create a realistic career progression plan based on individual strengths and development areas</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dc-assessments">
    <img src="img/backgrounds/white-traingle-left.svg" width="40" height="47" class="dc-assessments-element">
    <img src="img/backgrounds/bars-white-sm.svg" class="dc-assessments-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header text-light text-center text-md-left">Assessments</header>
        <p>At the Tata Management Training Centre, we use several kinds of assessments:</p>
        <div class="row">
            <div class="col-md-6">
                <ul class="dash-list">
                    <li>
                        <strong>Behavioural Event Interview/ Leadership Interview</strong>
                        <p>Behavioural interviews focus on a candidate’s past experiences to assess how they have navigated specific situations and utilised skills relevant to the position.</p>
                    </li>
                    <li>
                        <strong>Role Play</strong>
                        <p>Role play is a form of experiential learning. Here participants take on different business roles and take part in diverse and complex learning settings.</p>
                    </li>
                    <li>
                        <strong>Group Discussion</strong>
                        <p>Group discussions typically involve 6 to 12 participants and 3 to 6 assessors. The assessors are positioned so that they can clearly see the candidates assigned to them for the entire session. The assessors document everything they hear and observe about each person’s behaviour.</p>
                    </li>
                </ul>
            </div>
            
            <div class="col-md-6">
                <ul class="dash-list">
                    <li>
                        <strong>Psychometric Assessment</strong>
                        <p>Psychometric Assessments are tests that are used to objectively measure an individual’s personality traits, intelligence, abilities, behavioural style, and aptitude.</p>
                    </li>
                    <li>
                        <strong>Business Simulation</strong>
                        <p>Simulations are situations designed to imitate the real-life environment of an organisation. Here participants are assessed on problem analysis and problem solving, interpersonal sensitivity, planning and organising.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-dc-methodology" id="our-approach">
    <div class="container">
        <header class="section-header text-center">Methodology</header>
        <div class="dc-methodology-card dc-methodology-card-1">
            <img src="img/backgrounds/dc-methodology-card-1-arrow.svg" class="dc-methodology-pointer-1" alt="">
            <span class="dc-methodology-card-1-arrow"></span>
            <strong style="color: #A3473B;">Assessment</strong>
            
            <ul class="dot-list brown-dots">
                <li>There are a plethora of world-class assessment tools and scientifically proven psychometric tests that are used to gauge the potential leaders, depending on the competencies and skills to be assessed.</li>
                <li>In-depth strategic analysis provides insights into individual strengths and weaknesses, and how they compare overall with personnel in the sector globally.</li>
                <li>Global benchmarking and industry analysis reveals the leadership requirements that must be fulfilled for the company to achieve consistent growth</li>
            </ul>
        </div>
        
        <div class="dc-methodology-card dc-methodology-card-2">
            <img src="img/backgrounds/dc-methodology-card-2-arrow.svg" class="dc-methodology-pointer-2" alt="">
            <span class="dc-methodology-card-2-arrow"></span>
            <strong>Development</strong>
            <ul class="dot-list white-dots">
                <li>Upon request, the results can be used to develop a personalised development plan designed to meet the individual’s and the company’s goals. This may include recommendations of custom or existing programmes, and skill development initiatives.</li>
                <li>Whenever necessary, custom programmes are developed to assist individuals in achieving greater success and driving organisational growth.</li>
                <li>Post completion of the learning journeys, another round of benchmarking is conducted to report on the effectiveness of the development programmes and initiatives</li>
            </ul>
        </div>
    </div>
</section>

<section class="section-dc-spotlight" id="spotlight">
    <header class="section-header text-light text-center">Spotlight</header>
    <div class="gallery-image-slider dots-dashed">
        <div>
            <div class="gallery-image-slide">
                <img src="img/backgrounds/hightlights.png" class="img-fluid" alt="">
            </div>
        </div>
        
        <div>
            <div class="gallery-image-slide">
                <img src="img/backgrounds/highlights2.png" class="img-fluid" alt="">
            </div>
        </div>
        
        <div>
            <div class="gallery-image-slide">
                <img src="img/backgrounds/highlights3.png" class="img-fluid" alt="">
            </div>
        </div>
        
        <div>
            <div class="gallery-image-slide">
                <img src="img/backgrounds/highlights4.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>

<section class="section-site-contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="pr-md-5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="55.76" height="66" viewBox="0 0 55.76 66">
                        <path id="Polygon_3" data-name="Polygon 3" d="M33,0,66,55.76H0Z" transform="translate(55.76) rotate(90)" fill="#914A2B" />
                    </svg>
                    <header class="section-header">Write to us to know more</header>
                    <p class="section-description">
                        If you have queries about the programmes or would like to know how to nominate/get nominated, send us a
                        message!
                    </p>
                    
                    <img src="img/backgrounds/bars-ac-dc.svg" class="img-fluid pb-5" data-aos="fade-in">
                </div>
            </div>
            <div class="col-lg-7">
                <form action="" class="site-contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Your Name" />
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Last Name" />
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Company" />
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="d-flex w-100">
                                <div class="digital-patterns-select-wrapper">
                                    <select name="" id="" class="digital-patterns-select" style="width: 80px">
                                        <option value="">+91</option>
                                        <option value="">+91</option>
                                        <option value="">+91</option>
                                    </select>
                                </div>
                                <div class="digital-patterns-input-wrapper ml-auto" style="width: calc(100% - 95px)">
                                    <input type="text" class="digital-patterns-input" placeholder="Phone Number" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Email Address" />
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="digital-patterns-textarea-wrapper">
                                <textarea class="digital-patterns-textarea" rows="6">Message</textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <button class="btn-cta-navy">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>