$(document).ready(function () {
	console.log("Developed with ❤ by Jay Mewada");
	console.log("https://jaymewada.me/");

	
	
	let siteMain = $(".site-main");

	
	setTimeout(() => {
		AOS.init();
	}, 100);
	
	// SCROLL TO SECTION
	$(".hash-link").each(function () {
		$(this).click(function (e) {
			e.preventDefault();
			$("html, body").animate({ scrollTop: $($.attr(this, "href")).offset().top - 85 }, 500);
		});
	});

	$('#btn-about-transcript').click(function(){
		$(this).toggleClass('pause');
	})
	
	// $(".dropdown-menu a").click(function () {
	// 	$("#selected").text($(this).text());
	// });
	
	// $('.btn-toggle-transcript').click(function(){
	// 	if($(this).hasClass('collapsed')){
	// 		$(this).html('VIEW')
	// 	}else{
	// 		$(this).html('SHOW')
	// 	}
	// })
	
	$("#triggerSignInModal").click(function () {
		$("#signUpModal").modal("hide");
		$("#signIn").modal("show");
	});
	
	$("#triggerSignUpModal").click(function () {
		$("#signIn").modal("hide");
		$("#signUpModal").modal("show");
	});
	
	$(".btn-show-password").click(function (e) {
		e.preventDefault();
		$(this).toggleClass("is-closed");
		
		if ($(this).hasClass("is-closed")) {
			$("#login-password").attr("type", "text");
		} else {
			$("#login-password").attr("type", "password");
		}
	});
	
	// $('.approach-card-left-arrow')
	// console.log($('.cs-approach-card-content').innerHeight());
	
	// $(".header-navigation-link").each(function () {
	// 	$(this).mouseover(function () {
	// 		console.log("Trigger Enter", $(this).attr("href"));
	
	// 		$($(this).attr("href")).collapse({
	// 			toggle: true,
	// 		});
	
	// 		$($(this).attr("aria-expanded='true'")).collapse({
	// 			toggle: false,
	// 		});
	// 	});
	// });
	
	// $(".header-navigation-link[aria-expanded='true']").each(function () {
	// 	$(this).mouseover(function () {
	// 		console.log("Trigger Enter", $(this).attr("href"));
	
	// 		$($(this).attr("href")).collapse({
	// 			toggle: false,
	// 		});
	// 	});
	// });
	
	// $($(".header-navigation-link").attr('aria-expanded="true"')).mouseleave(function () {
	
	// 		$($(this)).collapse({
	// 			toggle: false,
	// 		});
	
	// 	// console.log("Trigger Exit", $(this).attr("href"));
	// });
	
	// $(".header-navigation-link.has-child-menu").each(function () {
	
	// });
	
	// $(".header-navigation-link.has-child-menu").mouseover(function () {
	// 	console.log($(this).attr("href"));
	// 	let headerNavigaitonLink = $(this).attr("href");
	
	// 	$(headerNavigaitonLink).collapse({
	// 		toggle: true,
	// 	});
	// });
	
	// $(".header-navigation-link.has-child-menu").mouseleave(function () {
	// 	console.log($(this).attr("href"));
	// 	let headerNavigaitonLink = $(this).attr("href");
	
	// 	$(headerNavigaitonLink).collapse({
	// 		toggle: false,
	// 	});
	// });
	
	$(".horizontal-image-card").each(function () {
		$(this).children().css("height", $(this).outerHeight());
	});
	
	// ALL SLIDER
	
	$(".home-trending-slider").slick({
		dots: true,
		arrows: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					arrows: false,
					infinite: true,
					dots: true,
				},
			},
		],
	});
	
	$(".learn-now-slider").slick({
		dots: false,
		arrows: true,
		centerMode: false,
		infinite: true,
		variableWidth: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					dots: false,
					arrows: false,
				},
			},
		],
	});
	
	$(".cta-small-slider").slick({
		dots: false,
		speed: 30000,
		arrows: false,
		infinite: true,
		autoplay: true,
		centerMode: false,
		draggable:false,
		cssEase: "linear",
		pauseOnHover: false,
		variableWidth: true,
		focusOnSelect:false,
		pauseOnFocus:false,
	});
	
	$(".cta-center-slider").slick({
		pauseOnFocus:false,
		slidesToShow: 1,
    	slidesToScroll: 1,
		focusOnSelect:false,
		draggable:false,
		dots: false,
		arrows: false,
		infinite: true,
		initialSlide: 1,
		autoplay: true,
		speed: 40000,
		cssEase: "linear",
		variableWidth: true,
	});
	
	$(".landing-banner-slider").slick({
		dots: true,
		fade: true,
		autoplay: true,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		cssEase: "linear",
		autoplaySpeed: 5000,
	});
	
	$(".community-outreach-slider").slick({
		dots: true,
		autoplay: true,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
	});
	
	$(".partners-logo-slider").slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 5000,
		autoplay: true,
		autoplaySpeed: 0,
		loop: true,
		cssEase: "linear",
		slidesToShow: 1,
		initialSlide: 1,
		slidesToScroll: 1,
		variableWidth: true,
	});
	
	$(".testimonial-slider").slick({
		dots: true,
		arrows: false,
		infinite: true,
		autoplay: true,
		pauseOnHover: false,
		pauseOnFocus: false,
		autoplaySpeed: 10000,
	});
	
	$(".programme-faculty-slider").slick({
		dots: true,
		arrows: false,
		autoplay: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					// variableWidth: false,
				},
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: "30px",
					// centerMode: true,
					// variableWidth: false,
				},
			},
		],
	});
	
	$(".gallery-image-slider").slick({
		dots: true,
		arrows: true,
		infinite: false,
		variableWidth: true,
		pauseOnHover: false,
		// autoplay: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerPadding: "10%",
		centerMode: true,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					variableWidth: false,
					centerPadding: "0px",
				},
			},
			{
				breakpoint: 767,
				settings: {
					variableWidth: false,
					centerPadding: "25px",
					arrows: false,
				},
			},
		],
	});
	
	$('button[data-toggle="tab"]').on("shown.bs.tab", function (e) {
		console.log("tab click");
		$(".gallery-image-slider, .people-slider").slick("setPosition");
		
		let resetSliderCardHeight = $('.section-people .tab-pane.active .slick-track').innerHeight() - 30;
		$('.people-slider-card').css('height', resetSliderCardHeight)

		setTimeout(() => {

			let resetSliderCardHeight2 = $('.section-people .tab-pane.active .slick-track').innerHeight() - 30;
			$('.people-slider-card').css('height', resetSliderCardHeight2)
			
		}, 500);
		
	});
	
	
	
	$(".resources-slider").slick({
		dots: false,
		arrows: true,
		infinite: true,
		variableWidth: true,
		centerMode: true,
		initialSlide: 1,
		centerPadding: "10px",
		responsive: [
			{
				breakpoint: 767,
				settings: {
					// slidesToShow: 2,
					centerMode: true,
					variableWidth: false,
					centerPadding: "7.5%",
					// variableWidth: false,
				},
			},
		],
	});
	
	$(".related-podcast-slider").slick({
		dots: false,
		arrows: true,
		infinite: false,
		slidesToShow: 3,
		initialSlide: 1,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: true,
		centerPadding: "2.5%",
	});
	
	$(".people-slider").slick({
		dots: true,
		// autoplay: true,
		arrows: false,
		pauseOnHover: false,
		slidesToShow: 1,
		infinite:false,
		initialSlide: 1,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: true,
		centerPadding: "5%",
	});
	
	$(".our-history-slider").slick({
		dots: true,
		arrows: false,
		slidesToShow: 3,
		infinite:false,
		initialSlide: 1,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: true,
		centerPadding: "5%",
	});
	
	$(".tbeg-awards-slider").slick({
		dots: false,
		arrows: true,
		infinite: false,
		slidesToShow: 3,
		initialSlide: 0,
		slidesToScroll: 1,
		variableWidth: true,
		responsive: [
			{
				breakpoint: 576,
				settings: {
					centerMode: true,
					// variableWidth: false,
				},
			},
		],
	});
	
	$(".ow-banner-slider").slick({
		dots: true,
		arrows: true,
		autoplay: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 576,
				settings: {
					arrows: false,
				},
			},
		],
	});
	
	$(".ow-films-slider").slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		centerMode: true,
		centerPadding: 0,
		slidesToScroll: 1,
		asNavFor: ".ow-films-slider-nav",
	});
	
	$(".ow-films-slider-nav").slick({
		dots: false,
		arrows: true,
		loop: true,
		infinite: true,
		centerMode: true,
		focusOnSelect: true,
		variableWidth: true,
		asNavFor: ".ow-films-slider",
	});
	
	$(".cs-endorsements-slider").slick({
		dots: false,
		arrows: true,
		autoplay: true,
		variableWidth: true,
		pauseOnHover: false,
		autoplaySpeed: 4000,
		centerMode: true,
		initialSlide: 1,
		centerPadding: "0%",
		responsive: [
			{
				breakpoint: 576,
				settings: {
					centerMode: true,
					// variableWidth: false,
					centerPadding: "10px",
				},
			},
		],
	});
	
	function elementLoadHandler() {
		console.log("Element Handler Triggered");
		
		// HANDLE MAIN HEADER NAVIGAION
		$('.site-header-navigaton-list .has-child-menu').each(function() {
			
			
			if(window.innerWidth < 992){
				$(this).attr('data-toggle', 'collapse')
			}else{
				
				let menuName = $(this).attr('href');
				
				$(menuName).mouseover(function(){
					$('a[href="' + menuName + '"').addClass('is-active');
				})
				
				$(menuName).mouseleave(function(){
					$('a[href="' + menuName + '"').removeClass('is-active');
				})
				
			}
		});
		
		let methodologyCard1 = $(".dc-methodology-card-1").innerHeight() / 2;
		$(".dc-methodology-card-1-arrow").css({
			"border-top": methodologyCard1 + "px solid transparent",
			"border-bottom": methodologyCard1 + "px solid transparent",
		});
		
		let methodologyCard2 = $(".dc-methodology-card-2").innerHeight() / 2;
		$(".dc-methodology-card-2-arrow").css({
			"border-top": methodologyCard2 + "px solid transparent",
			"border-bottom": methodologyCard2 + "px solid transparent",
		});
		
		let cardHeight1 = $(".cs-approach-card-1").innerHeight() / 2;
		$(".approach-card-1-arrow").css({
			"border-top": cardHeight1 + "px solid transparent",
			"border-bottom": cardHeight1 + "px solid transparent",
		});
		
		let cardHeight2 = $(".cs-approach-card-2").innerHeight() / 2;
		$(".approach-card-2-arrow").css({
			"border-top": cardHeight2 + "px solid transparent",
			"border-bottom": cardHeight2 + "px solid transparent",
		});
		
		let cardHeight3 = $(".cs-approach-card-3").innerHeight() / 2;
		$(".approach-card-3-arrow").css({
			"border-top": cardHeight3 + "px solid transparent",
			"border-bottom": cardHeight3 + "px solid transparent",
		});
		
		let cardHeight4 = $(".cs-approach-card-4").innerHeight() / 2;
		$(".approach-card-4-arrow").css({
			"border-top": cardHeight4 + "px solid transparent",
			"border-bottom": cardHeight4 + "px solid transparent",
		});
		
		let ctaContentHeight = $(".cta-slider-center-content").innerHeight();
		$(".cta-center-img").css("height", ctaContentHeight);
		
		let testimonialCardHeight = $(".testimonial-slider .slick-track").innerHeight() - 55;
		$(".testimonial-slide").css("height", testimonialCardHeight);
		
		
		setTimeout(() => {
			let peopleCardHeight = $('.section-people .tab-pane.active .slick-track').innerHeight() - 30;
			$('.people-slider-card').css('height', peopleCardHeight);
		}, 1000);

		setTimeout(() => {
			let peopleCardHeight = $('.section-people .tab-pane.active .slick-track').innerHeight() - 30;
			$('.people-slider-card').css('height', peopleCardHeight);
		}, 2000);


		$(".podcast-card").css("height", $(".podcast-card-image"));
	}
	
	elementLoadHandler();
	
	$("#btn-nav-toggler").click(function () {
		$(".site-header-navigation").toggleClass("is-active");
		
		let togglerIcon = $(".btn-nav-toggler i");
		
		if ($(".site-header-navigation").hasClass("is-active")) {
			togglerIcon.html("close");
		} else {
			togglerIcon.html("menu");
		}
	});
	
	// UPDATE ON RESIZE
	$(window).resize(function () {
		console.log("Screen is Resized");
		
		$(".podcast-card").css("height", $(".podcast-card-image"));
		
		let testimonialCardHeight = $(".testimonial-slider .slick-track").innerHeight() - 55;
		$(".testimonial-slide").css("height", testimonialCardHeight);
		
		elementLoadHandler();
	});
});
