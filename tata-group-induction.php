<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/tata-group-induction-banner.png" alt="">
</section>

<section class="banner-shadow-content text-center">
    The Tata Group Induction Programme is designed for leaders who have recently joined the Tata group. It aims to communicate the vision and values of the group’s Founders to current and future Tata leaders to empower and guide you as you take forward the legacy of the group.
</section>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list">
            <li>
                <a class="hash-link" href="#about">About</a>
            </li>
            <li>
                <a class="hash-link" href="#amis">Aims</a>
            </li>
            <li>
                <a class="hash-link" href="#outcomes">Outcomes</a>
            </li>

            <li>
                <a class="hash-link" href="#learners-profile">Leaners Profile</a>
            </li>
            <li>
                <a class="hash-link" href="#speakers">Speakers</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-gi-about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">About</header>
                <img src="img/backgrounds/bars-military-green-sm.svg" data-aos="fade-in">
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p>The programme gives you a comprehensive understanding of the genesis of brand Tata, our key values, and rich history. Eminent leaders with years of experience working with the group take participants through the principles, code of conduct, and business ethics that distinguish the Tata group.</p>

                <p>The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>

<section class="section-gi-aim" id="amis">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">Aim</header>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p>The program is designed to meet the following objectives:</p>
                <ul class="dot-list military-green-dots">
                    <li>Know what makes us Tata and what defines Tata leadership.</li>
                    <li>Understand the Tata brand equity, legacy and values</li>
                    <li>Explore the leadership purpose and practices that will lead and shape our future.</li>
                    <li>Discover the impetus to quality, excellence and innovation at Tata.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-gi-outcomes" id="outcomes">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header text-light">Outcomes</header>
                <img src="img/backgrounds/bars-white-sm.svg" class="img-fluid mb-4" data-aos="fade-in">
            </div>
            <div class="col-md-9 col-md-8 col-lg-7 align-self-center">
                <ul class="dot-list white-dots">
                    <li>Build and reinforce the spirit and values of the Tata Group.</li>
                    <li>Offer a perspective on the group’s evolution, and focus on sustainability, society and community welfare.</li>
                    <li>Provide insight into the Tata Group’s structure and management of the Tata brand.</li>
                    <li>Share an overview of group level initiatives.</li>
                    <li>Facilitate engagement and collaboration between leaders of different group companies.</li>
                    <li>Help participants build lasting relationships with fellow senior leaders.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-gi-learners-profile" id="learners-profile">
    <div class="container">
        <header class="section-header mb-2">Learner profile</header>
        <p>N-1 and N-2 leaders who have joined the Tata group in the last 3 months.</p>
    </div>
</section>

<section class="section-gi-speakers" id="speakers">
    <div class="container">
        <header class="section-header text-center mb-3">Speakers</header>
        <p class="section-description">The Tata Group Induction Programme is delivered by several Tata leaders. Some of our past speakers include:</p>

        <img src="img/backgrounds/military-green-d-right.svg" class="section-bg-1">
        <img src="img/backgrounds/bars-military-green-lg.svg" class="section-bg-2" data-aos="fade-in"  >
        <img src="img/backgrounds/military-green-d.svg" class="section-bg-3">
        <img src="img/backgrounds/bars-military-green-lg.svg" class="section-bg-4" data-aos="fade-in">
        <img src="img/backgrounds/military-green-d-right.svg" class="section-bg-5">
        <img src="img/backgrounds/bars-military-green-lg.svg" class="section-bg-6" data-aos="fade-in">

        <div class="row">
            <div class="col-md-6 col-lg-12 mb-40">
                <div class="single-gi-speaker">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="gi-speaker-image">
                                <img src="img/backgrounds/gi-team.png" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 align-self-center">
                            <h5>Harish Bhat</h5>
                            <strong>Brand Custodian, Tata Sons</strong>
                            <p>Harish has been a driving force at Tata Sons for over three decades, holding key positions across different Tata companies, such as Managing Director of Tata Global Beverages and Chairman of Tata Coffee Limited. He has been instrumental in launching and nurturing several iconic brands, including Titan, Fastrack, Tanishq and Tata Tea. He has played a crucial role in the success of the jewellery business and the Tetley acquisition.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12 mb-40">
                <div class="single-gi-speaker">
                    <div class="row">
                        <div class="col-lg-8 order-2 order-md-1 align-self-center">
                            <h5>H N Shrinivas</h5>
                            <strong>Former Senior Vice President & Head – Human Resources, Taj Group of Hotels</strong>
                            <p>Fondly known as HNS, he has over three decades of experience in the field, including more than 25 years with the Tata group. Under his leadership, the Taj group of hotels earned several accolades, including the National Award for Best HR Practices and the international ‘Best Employer’ Award by Hewitt Associates. HNS was also vital in the Taj group receiving the Gallup Global ‘Great Places to Work’ Award for four consecutive years. Throughout his career, HNS has been dedicated to creating a positive and supportive work culture.</p>
                        </div>
                        <div class="col-lg-4 order-lg-1 order-lg-2">
                            <div class="gi-speaker-image">
                                <img src="img/backgrounds/gi-team.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12 mb-40">
                <div class="single-gi-speaker">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="gi-speaker-image">
                                <img src="img/backgrounds/gi-team.png" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 align-self-center">
                            <h5>Sanjeev Singh</h5>
                            <strong>Vice President, Tata Business Excellence Group</strong>
                            <p>Sanjeev has been with the Tata group for over 30 years. As the Head of Business Excellence Service Delivery at the Tata Business Excellence Group (TBExG), he partners with group companies to co-create solutions that help build a culture of performance excellence, including performance diagnostics, improvement projects, capability building, and best practice adaptation. Sanjeev’s expertise in organisation development has led to many process maturity assessments, capability-building programmes, and improvement projects in areas such as Operations Excellence and Customer Centricity.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12 mb-40">
                <div class="single-gi-speaker">
                    <div class="row">
                        <div class="col-lg-8 order-2 order-md-1 align-self-center">
                            <h5>Siddharth Sharma</h5>
                            <strong>CEO designate, Tata Trusts
                                Group Chief Sustainability Officer, Tata Sons</strong>
                            <p>Siddharth’s journey from a civil servant to Group Chief Sustainability Officer at Tata Sons to CEO (designate) for the Tata Trusts is a testament to his passion for sustainable development. Previously, his deep knowledge of the Indian financial system contributed immensely to the expansion of the National Pension System. He spearheaded the transitioning of the Indian government from cash to accruals, a landmark achievement that has paved the way for fiscal responsibility. His co-authorship with Dr Urjit Patel on the Government of India’s Pensionary Liabilities laid the foundation for India’s pension system.</p>
                        </div>
                        <div class="col-lg-4 order-lg-1 order-lg-2">
                            <div class="gi-speaker-image">
                                <img src="img/backgrounds/gi-team.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12 mb-40">
                <div class="single-gi-speaker">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="gi-speaker-image">
                                <img src="img/backgrounds/gi-team.png" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 align-self-center">
                            <h5>Dinanath (Dina) Kholkar</h5>
                            <strong>Vice President & Global Head, Analytics & Insights, Tata Consultancy Services (TCS)</strong>
                            <p>Dinanath guides some of the world’s best companies in their journeys to unlock the potential of their data through the power of analytics and artificial intelligence (AI), to uncover new business opportunities, drive business growth, and increase revenue streams. His thought leadership, in addition to his team’s expertise and collaboration with customer organisations, is empowering companies to realise the power of their data in real-time decision-making and ensuring success in their Business 4.0™ transformation journeys.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
</section>

<section class="section-site-contact" id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="pr-md-5">
					<svg xmlns="http://www.w3.org/2000/svg" width="55.76" height="66" viewBox="0 0 55.76 66">
						<path id="Polygon_3" data-name="Polygon 3" d="M33,0,66,55.76H0Z" transform="translate(55.76) rotate(90)" fill="#365A58" />
					</svg>
					<header class="section-header">Write to us to know more</header>
					<p class="section-description">
						If you have queries about the programmes or would like to know how to nominate/get nominated, send us a
						message!
					</p>

					<img src="img/backgrounds/bars-military-green-lg-x2.svg" data-aos="fade-in" class="img-fluid pb-5" alt="">
				</div>
			</div>
			<div class="col-lg-7">
				<form action="" class="site-contact-form">
					<div class="row">
						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Your Name" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Last Name" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Company" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="d-flex w-100">
								<div class="digital-patterns-select-wrapper">
									<select name="" id="" class="digital-patterns-select" style="width: 80px">
										<option value="">+91</option>
										<option value="">+91</option>
										<option value="">+91</option>
									</select>
								</div>



								<div class="digital-patterns-input-wrapper ml-auto" style="width: calc(100% - 95px)">
									<input type="text" class="digital-patterns-input" placeholder="Phone Number" />
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Email Address" />
							</div>
						</div>

						<div class="col-md-12">
							<div class="digital-patterns-textarea-wrapper">
								<textarea class="digital-patterns-textarea" rows="6">Message</textarea>
							</div>
						</div>

						<div class="col-md-12">
							<button class="btn-cta-navy">Send Message</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>