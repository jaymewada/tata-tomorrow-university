<!-- Developed with ❤ by Jay Mewada -->
<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/onederful-world-banner.png" alt="" />
</section>

<section class="banner-shadow-content text-center">
	Diversity is what defines us at the Tata group, and it is reflected in our core values. Inclusion isn’t just a cause
	for us but a deliberate well-thought business strategy that enables workspaces built on mutual respect, acceptance and
	empathy for employees, stakeholders, vendors, suppliers and the communities we work in. To embed this further amongst
	all our employees, in January 2020, we launched a movement to create a ONEderful World.
</section>

<section class="section-page-nav">
	<div class="container">
		<ul class="page-navigation-list">
			<li>
				<a class="hash-link" href="#about">About</a>
			</li>
			<li>
				<a class="hash-link" href="#we-at-tata">We@Tata</a>
			</li>
			<li>
				<a class="hash-link" href="#films">Films</a>
			</li>
			<li>
				<a class="hash-link" href="#programmes">programmes</a>
			</li>
			<li>
				<a class="hash-link" href="#onederful-collectives">ONEderful Collective</a>
			</li>
		</ul>
	</div>
</section>

<section class="section-ow-about" id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<header class="section-header text-center">About</header>
			</div>
			<div class="col-md-8">
				<p>
					ONEderful World is a powerful collection of 12 simple, impactful insights that highlight nuances of exclusion
					and inclusion in worlds that we live in. While we have often talked about insights like “Break the stereotype”
					and “Nurturing is Universal”, we have also brought into conversation lesser-known aspects like “Every voice
					counts” and “Thoughts transcend language”.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-gallery">
	<div class="container">
		<header class="section-header text-center">
			ONEderful World Essentials <br class="d-none d-md-block" />
			Gallery
		</header>

		<img src="img/backgrounds/ow-gallery-top-arrow.svg" class="ow-gallery-top-arrow duration-1s" data-aos="fade-down" />
		<img src="img/backgrounds/ow-gallery-bottom-arrow.svg" class="ow-gallery-bottom-arrow duration-1s" data-aos="fade-up" />
		<img src="img/backgrounds/ow-galler-logo.png" width="150" height="75" class="ow-gallery-logo" />

		<div class="row">
			<div class="col-md-6 align-self-center">
				<img src="img/backgrounds/ow-galler.png" class="ow-galler-preview" alt="" />
			</div>

			<div class="col-md-6 align-self-center">
				<p>21 Exhibits <strong style="color: #af416a" class="px-1">|</strong> 9 aspects of Inclusion</p>

				<p>Diverse Perspectives <strong style="color: #efaf35" class="px-1">|</strong> Reasons to Reflect</p>

				<p>Cause for Smiles</p>
				<p>And countless ways to create a ONEderful World.</p>
				<a href="" style="width: 215px; margin-top: 20px" class="btn-cta-yellow fw-500">Go to Gallery</a>
			</div>
		</div>
	</div>
</section>

<header class="section-header mb-3 text-center">We@Tata</header>
<section class="section-ow-tata" id="we-at-tata">
	<div class="container">
		<div class="row">
			<div class="col-md-6 text-center">
				<img src="img/backgrounds/we-at-tata.png" class="img-fluid mb-3" alt="" />
				<div class="d-none d-md-block">
					<img src="img/backgrounds/we-at-tata-arrow.png" class="img-fluid " alt="" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="ow-tata-content">
					
					<p>
						At the Tata group, our corporate mission, purpose and values are of vital significance for our business
						success. Equally important for us is the celebration, respect, inclusion and acknowledgment of each
						individual. The underlying fusion of these two thoughts laid the foundations of We@Tata.
					</p>
					<p>
						We@Tata is an exclusive Tata network that builds bonds among employees across all group companies to create
						safe spaces, communities and employee resource groups composed of women networks, LGBTQ + community,
						differently abled, and other affinity groups, which will foster conversations for productive brainstorming,
						cultural engagement, cross-functional learnings and seek support for all.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-network">
	<header class="section-header">Networks</header>

	<div class="ow-network-card-wrapper">
		<img src="img/backgrounds/onederful-world-networks-bg.svg" class="section-bg-arrow duration-1s" data-aos="fade-right" />
		<div class="container">
			<div class="row">
				<div class="col-md-6 mb-30 mb-md-0">
					<div class="ow-network-card">
						<h5>Women’s Network</h5>
						<p>
							A community designed to create a safe space for women to address their challenges in realising their
							personal and professional aspirations and also to reiterate our core value that every voice counts!
						</p>

						<strong>Member Benefits:</strong>
						<ul class="dot-list yellow-dots">
							<li>
								Platform to share, connect and network with women across the Tata companies in your region and beyond
							</li>
							<li>Access to learning resources for your personal and professional development</li>
							<li>Opportunity to connect with women role models and receive and offer mentorship</li>
							<li>Create awareness, discuss ideas and e-socialise basis common interest group areas and expertise</li>
						</ul>

						<a href="" class="btn-cta-navy">Request Membership</a>
					</div>
				</div>

				<div class="col-md-6">
					<div class="ow-network-card">
						<h5>Learning Cohorts</h5>
						<p>
							Learning Cohorts is a platform that hosts many communities for the various programmes under the ‘Shift a
							Little, Grow a Lot’ section. These communities have been designed for the participants attending the
							different batches to help them strengthen their learnings through active engagement and networking amongst
							the co-learners.
						</p>

						<strong>Member Benefits:</strong>
						<ul class="dot-list yellow-dots">
							<li>
								Platform to share, engage, connect and network with participants across the Tata companies in your batch
								and beyond.
							</li>
							<li>Access to learning resources for your personal and professional development</li>
							<li>Create awareness, discuss ideas and e-socialise basis common interest group areas and expertise.</li>
						</ul>

						<small>*Accessible only to registered participants</small>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-films" id="films">
	<div class="">
		<header class="section-header">Films</header>
		<p class="section-description">
			Short films showcasing powerful stories of ONEderful little-big things that give us hope and purpose.
		</p>

		<div class="ow-films-slider">
			<?php for ($x = 0; $x <= 3; $x++) { ?>
			<div>
				<a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="ow-films-slide video-card" data-fancybox>
					<img src="img/backgrounds/ow-films-slide-1.png" alt="" />
				</a>
			</div>

			<div>
				<a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="ow-films-slide video-card" data-fancybox>
					<img src="img/backgrounds/ow-films-slide-2.png" alt="" />
				</a>
			</div>

			<div>
				<a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="ow-films-slide video-card" data-fancybox>
					<img src="img/backgrounds/ow-films-slide-3.png" alt="" />
				</a>
			</div>

			<?php } ?>
		</div>

		<div class="ow-films-slider-nav">
			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-3.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-4.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-5.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-5.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-6.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-7.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-8.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-9.png" width="150" height="150" alt="" />
				</div>
			</div>

			<div>
				<div class="ow-slide-nav">
					<img src="img/backgrounds/ow-films-10.png" width="150" height="150" alt="" />
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-explore" id="programmes">
	<div class="container">
		<header class="section-header section-header-sm">
			Explore the programmes under Diversity, Equity, & Inclusion
		</header>

		<div class="row justify-content-center">
			<div class="col-md-4 mb-30 mb-md-0">
				<a href="" class="webinar-card">
					<div href="" class="webinar-card-image">
						<img src="img/cards/dei-wave-1.jpg" />
					</div>
				</a>
			</div>
			<div class="col-md-4 mb-30 mb-md-0">
				<a href="" class="webinar-card">
					<div href="" class="webinar-card-image">
						<img src="img/cards/dei-wave-2.jpg" />
					</div>
				</a>
			</div>

			<div class="col-12 mt-3 mt-md-5">
				<a href="" class="btn-cta-navy">Explore all programmes</a>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-podcast" id="onederful-collectives">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="horizontal-podcast-card">
					<a href="">
						<img src="img/backgrounds/radio-circular-logo.svg" height="125" width="125" class="circular-logo" />
					</a>

					<img src="img/backgrounds/radio-card1.png" class="inner-card-logo" alt="" />
					<p>A podcast series that explores Tata stories through the lens of inclusion</p>

					<a href="" class="inner-card-link">
                        <span>Listen now</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="19" viewBox="0 0 23 19">
                            <g id="Group_3141" data-name="Group 3141" transform="translate(-309 -1256)">
                              <rect width="23" height="15" rx="2" transform="translate(309 1260)" fill="#fff"/>
                              <rect width="21" height="2" rx="1"  transform="translate(311 1256)" fill="#fff"/>
                              <path d="M0,0H4A0,0,0,0,1,4,0V2A0,0,0,0,1,4,2H1A1,1,0,0,1,0,1V0A0,0,0,0,1,0,0Z" transform="translate(313 1256) rotate(90)" fill="#fff"/>
                              <rect width="6" height="1.5" class="h-yellow" rx="0.75" transform="translate(323.5 1264.5) rotate(90)" fill="#249093"/>
                              <rect width="6" height="1.5" class="h-yellow" rx="0.75" transform="translate(326.5 1264.5) rotate(90)" fill="#249093"/>
                              <rect width="6" height="1.5" class="h-yellow" rx="0.75" transform="translate(329.5 1264.5) rotate(90)" fill="#249093"/>
                              <circle cx="3.5" cy="3.5" r="3.5"  transform="translate(312 1264)" fill="#fff" stroke="#249093" stroke-width="2"/>
                            </g>
                          </svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-ow-banners">
	<div class="ow-banner-slider dots-dashed">
		<div>
			<img src="img/banners/onederful-world-slide-0.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-1.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-2.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-4.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-5.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-6.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-7.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-8.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-9.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-10.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-11.png" />
		</div>

		<div>
			<img src="img/banners/onederful-world-slide-12.png" />
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>
