<?php include "components/header.php" ?>

<section class="section-landing-banner has-content-card global-header-margin">
    <img src="img/banners/tbeg-banner.png" alt="">
</section>

<div class="banner-shadow-content" style="max-width:900px;">
    <div class="row">
        <div class="col-lg-6">
            <p class="mb-0">The foundation of Business Excellence movement in the Tata Group was enabled by adopting the JRD Quality Value for Performance Excellence and promote a unified common Tata brand name. Tata Business Excellence Group (TBExG) is entrusted to build and nurture an institutionalized approach to drive the business excellence movement at the Tata group.</p>
        </div>
        <div class="col-lg-6">
            <p>It set standards of excellence and partner with group companies in their journey to achieve world class performance. This journey has been institutionalised through a virtuous cycle of performance excellence which has evolved over the years. This starts with Assessing the progress of the companies using excellence frameworks, accelerating progress through Improvement Interventions and nurturing an ecosystem of learning and sharing from each other.</p>
        </div>
    </div>


</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list">
            <li>
                <a class="hash-link" href="#purpose">Purpose</a>
            </li>
            <li>
                <a class="hash-link" href="#awards">Awards</a>
            </li>
            <li>
                <a class="hash-link" href="#services">Services</a>
            </li>
            <li>
                <a class="hash-link" href="#capability-building">Capability Building</a>
            </li>
            <li>
                <a class="hash-link" href="#contact-us">Contact Us</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-tbeg-purpose" id="purpose">
    <div class="container text-center">
        <header class="section-header mb-2">Purpose</header>
        <p class="section-description">To Partner with Tata Companies in their Journey of Excellence to Achieve World-Class Performance</p>
        <img src="img/backgrounds/tbeg-purpose.png" class="img-fluid" alt="">
    </div>
</section>

<section class="section-tbeg-quote">
    <div class="container">
        <div class="row">
            <div class="col-0 col-md-2 col-lg-3 d-none d-md-block">
                <img src="img/backgrounds/tbeg-quote-arrow-left.svg" class="h-100 img-fluid" alt="">
            </div>

            <div class="col-12 col-md-8 col-lg-6">
                <div class="tbeg-quote-content">
                    <blockquote>
                        <img src="img/backgrounds/tbeg-quote-before.svg" alt="">
                        <p>One must forever strive for excellence, or even perfection, in any task, however small, and never be satisfied with the second best.</p>
                        <img src="img/backgrounds/tbeg-quote-after.svg" alt="">
                    </blockquote>
                    <span class="tbeg-quote-author">- JRD Tata</span>
                </div>
            </div>

            <div class="col-0 col-md-2 col-lg-3 d-none d-md-block">
                <img src="img/backgrounds/tbeg-quote-arrow-right.svg" class="h-100 img-fluid" alt="">
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-awards" id="awards">
    <div class="container-fl">

        <div class="col-md-10 col-lg-9 mx-auto text-center">
            <header class="section-header mb-3">The Coveted Business Excellence Awards</header>
            <p style="max-width:800px;margin:0 auto;">The JRDQV Award is presented to companies that cross the 650-mark for the first time and demonstrate role model excellence in the Business Excellence Assessment using TBEM criteria. For companies that cross the 750-mark, the Benchmark Leader recognition is endowed.</p>
        </div>

        <div class="col-12">
            <div class="tbeg-awards-slider">

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-1.png" alt="" />
                            <a href="">Download</a>
                        </div>

                        <div class="winners-card-content">
                            <span>2021</span>
                            <p>JRDQV Winner – Tata Steel</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-2.png" alt="" />
                            <a href="">Download</a>
                        </div>

                        <div class="winners-card-content">
                            <span>2021</span>
                            <p>JRDQV Winner – Tata Elxsi</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-3.png" alt="" />
                            <a href="">Download</a>
                        </div>

                        <div class="winners-card-content">
                            <span>2021</span>
                            <p>JRDQV Winner – Tata Chemicals</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-4.png" alt="" />
                            <a href="">Download</a>
                        </div>
                        <div class="winners-card-content">
                            <span>2020</span>
                            <p>JRDQV Winner – Tata Power</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-5.png" alt="" />
                            <a href="">Download</a>
                        </div>
                        <div class="winners-card-content">
                            <span>2021</span>
                            <p>Benchmark Leader – Tata Steel</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-6.png" alt="" />
                            <a href="">Download</a>
                        </div>
                        <div class="winners-card-content">
                            <span>2019</span>
                            <p>Benchmark Leader – Tata Consultancy Services</p>
                        </div>
                    </div>
                </div>

                <div class="px-2 px-md-3">
                    <div class="winners-card">
                        <div class="winners-card-img">
                            <img src="img/cards/winner-7.png" alt="" />
                            <a href="">Download</a>
                        </div>
                        <div class="winners-card-content">
                            <span>2018</span>
                            <p>JRDQV Winner - Tata Sponge</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 text-center">
            <a href="jrdqv-winners" class="btn-cta-navy">JRDQV Event Photographs</a>
        </div>
    </div>
</section>

<section class="section-tbeg-services" id="services">
    <header class="section-header text-center mb-3">Services</header>
    <ul class="nav tbeg-tabs-nav page-navigation-list">
        <li class="nav-item text-center">
            <a class="active" data-toggle="tab" data-target="#tbeg">
                <span>Assessing progress using <br> excellence frameworks</span>
            </a>
        </li>
        <li class="nav-item">
            <a id="profile-tab" data-toggle="tab" data-target="#tbeg2">
                <span>Accelerating progress through <br> improvement interventions</span>
            </a>
        </li>
        <li class="nav-item">
            <a id="contact-tab" data-toggle="tab" data-target="#tbeg3">
                <span>Enabling learning from each other</span>
            </a>
        </li>
    </ul>

    <div class="tab-content tbeg-tab-content-wrapper">

        <img src="img/backgrounds/bars-white.svg" class="tbeg-tab-element-2" data-aos="fade-in">


        <div class="tab-pane fade show active" id="tbeg">
            <div class="container">
                <div class="tbeg-single-tab-content">
                    <div class="row">
                        <div class="col-12">
                            <h4>Assessing progress using excellence frameworks</h4>
                        </div>

                        <div class="col-lg-6 order-2 order-lg-1">
                            <p>The process has evolved as repeatable, measurable and scalable in a systemic and inclusive manner by pursuing fulfilling excellence journeys.</p>

                            <ul class="green-dot-list">
                                <li>
                                    Business Excellence, using TBEM framework Safety Excellence, using Tata Safety Standards
                                </li>
                                <li>
                                    Data Excellence, using TCS proprietary DATOM™
                                </li>
                                <li>Social Excellence, using Tata Affirmative Action Programme framework</li>
                                <li>Cyber Excellence, using TCS Bluetick Framework</li>
                            </ul>

                            <p>Tata companies find an opportunity to collectively introspect and reflect on these journeys through a customised and collaborative approach, engaging with subject matter experts as assessors, guided by senior Tata leaders as Mentors and Team Leaders.</p>
                            <br>
                            <a href="" class="btn-cta-navy" style="width:200px;">Know More</a>
                        </div>

                        <div class="col-lg-6 order-1 order-lg-2 mb-3">
                            <img src="img/backgrounds/tbeg-tab.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade " id="tbeg2">
            <div class="container">
                <div class="tbeg-single-tab-content">
                    <div class="row">
                        <div class="col-12">
                            <h4>Accelerating progress through <br> improvement interventions</h4>
                        </div>

                        <div class="col-lg-6 order-2 order-lg-1">
                            <p>Based on the strategic imperatives, as highlighted in the assessment feedback across excellence frameworks, organisations develop/update action plans in their journey towards world class performance. These action plans are both short-term and long-term interventions which enable the companies to achieve their business objectives through performance improvement (KPI impact). These interventions are broadly classified as process Improvements through Deep Dive diagnostics, Benchmarking studies and Excellence workshops. These Improvement Interventions are guided by Subject Matter Experts from Tata and outside Tata ecosystem and collaboratively executed by the process owners from the organisations.</p>

                            <p class="fw-500" style="color:#007D7B">Deep Dives are undertaken in the areas of</p>
                            <div class="row">
                                <div class="col-6">
                                    <ul class="green-dot-list">
                                        <li>Strategy Deployment</li>
                                        <li>Customer Centricity</li>
                                        <li>Human Resources</li>
                                        <li>Operational Excellence</li>
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <ul class="green-dot-list">
                                        <li>Knowledge Management</li>
                                        <li>Organisational Culture</li>
                                        <li>Safety</li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <a href="" class="btn-cta-navy" style="width:200px;">Know More</a>
                        </div>

                        <div class="col-lg-6 order-1 order-lg-2 mb-3">
                            <img src="img/backgrounds/tbeg-tab-2.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="tbeg3">
            <div class="container">
                <div class="tbeg-single-tab-content">
                    <div class="row">
                        <div class="col-12">
                            <h4>Enabling learning from each other</h4>
                        </div>
                        <div class="col-lg-6 order-2 order-lg-1">
                            <p>One of the important enablers of our purpose is to nurture learning and sharing amongst the group companies. The avenues like EDGE portal, EDGE Webinars, Best Practices Sharing Forums and Tata Network Forums, enable group companies to continuously learn from each other. The EDGE portal has become the knowledge repository of the Tata Group. Tata Network Forums around the world aim to foster the one Tata culture and collaboration. EDGE webinars create a platform for the Tata colleagues to learn from the subject matter experts. Customised Best Practices sharing & implementation helps improve process & results maturity by leveraging the Subject Matter Expertise in the Tata ecosystem.</p>
                            <ul class="green-dot-list">
                                <li>EDGE Portal – A repository of good practices across various topics</li>
                                <li>EDGE Webinars</li>
                                <li>Best Practices sharing sessions & forums</li>
                                <li>Tata Network Forums</li>
                                <li>Business Excellence Heads Forum</li>
                            </ul>
                            <br>
                            <a href="" class="btn-cta-navy px-5">Know More</a>
                        </div>
                        <div class="col-lg-6 order-1 order-lg-2 mb-3">
                            <img src="img/backgrounds/tbeg-tab-3.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-building" id="capability-building">
    <img src="img/backgrounds/white-rod.svg" class="tbeg-capability-building-element-1" data-aos="fade-in" style="transform:rotate(180deg);">
    <div class="container">
        <header class="section-header text-white text-center">Capability Building</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-40">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header font-primary fw-500">Business Excellence</header>
                        <p>The Business Excellence capability building programmes are geared towards preparing business leaders to achieve benchmark performance excellence. The programmes are classified into areas, to build Assessors capabilities and Champions capabilities.</p>
                        <span>Explore</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header font-primary fw-500">Data Excellence</header>
                        <p>Data Excellence is geared towards continuously driving the ecosystem to support data-driven decision-making and helping Tata companies to become more agile and future-ready. The capability building programmes are aimed at preparing business users and potential Assessors to understand the core elements of Data Excellence and how effective data management principles and practices can drive business impact.</p>
                        <span>Explore</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header font-primary fw-500">Social Excellence</header>
                        <p>These capability building programmes, enable Executives and Managers to appreciate the fundamentals of the Tata Affirmative Action programme. The programmes are classified into Assessor and Champions programme.</p>
                        <span>Explore</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header font-primary fw-500">Safety Excellence</header>
                        <p>The Safety Excellence capability building programmes have been designed for various levels to enhance the organisation’s Safety performance and culture through the adoption of Tata Safety Standards.</p>
                        <span>Explore</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header font-primary fw-500">Cyber Excellence</header>
                        <p>The Cyber Excellence capability building programmes are built towards preparing the organisation to achieve benchmark performance to achieve Cyber Excellence and protect the organisation’s networks and devices from external threats to protect confidential information, maintain employee productivity, and enhance customer confidence. The programmes are classified into areas to build Assessors’ and Champions’ capabilities.</p>
                        <span>Explore</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-contact" id="contact-us">
    <img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <p class="mb-1">Tata Business Excellence Group (TBExG)</p>
                        <p>Mumbai Office: Upper Ground Floor, Fort <br> House, 221 DN Road, Fort, Mumbai - 400001</p>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>