<?php include "components/header.php" ?>

<section class="section-episode-banner" style="background:url('img/backgrounds/brand-builder-bg.png');">
    <div class="episode-details-wrapper">
        <div class="row">
            <div class="col-md-9 col-lg-5 align-self-center pr-md-4 mx-auto">
                <div class="episode-author">
                    <img src="img/backgrounds/brand-builder-user.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-11 col-lg-7 align-self-center mx-auto">
                <div class="episode-banner-logo">
                    <img src="img/backgrounds/bb-logo.svg" alt="">
                </div>

                <div class="current-episode-banner-season">
                    <span>Season 4</span>
                    <span class="mx-2">|</span>
                    <span>Episode 1</span>
                </div>

                <header class="mb-3">
                    <h4 class="current-episode-banner-name">Challenger Brand Secrets: The myth of the underdog</h4>
                    <strong class="current-episode-banner-author">By Harish Bhat, CFO Tata Motors</strong>
                </header>

                <p>The myth of the underdog can teach us all a lot. Harish Bhat, Brand Custodian, Tata Sons in conversation with Adrian Terron, shares what goes on behind the scenes as a challenger brand takes on incumbent leaders.</p>
            </div>
        </div>

        <div class="podcast-controller">
            <button class="btn-podcast-player">
                <svg xmlns="http://www.w3.org/2000/svg" width="32.853" height="39.423" viewBox="0 0 32.853 39.423">
                    <path d="M161.658,1192.3v39.423l32.853-19.711Z" transform="translate(-161.658 -1192.297)" fill="#fff" />
                </svg>
            </button>

            <div class="podcast-player-timer">
                <span class="current-player-time">2:00</span>
                <span class="mx-1">/</span>
                <span class="total-player-time">35:00</span>
            </div>

            <div class="podcast-player-timeline">
                <div class="current-timeline-progress current-timeline-progress-purple"></div>
            </div>

            <div class="dropdown">
                <button class="btn-podcast-player-speed btn-podcast-purple" type="button" data-toggle="dropdown" aria-expanded="false">
                    <span>x1</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right podcast-speed-dropdown">
                    <h6 class="dropdown-header">Playback Speed</h6>
                    <a class="dropdown-item" href="#"> 0.5</a>
                    <a class="dropdown-item" href="#">0.75</a>
                    <a class="dropdown-item active" href="#">1</a>
                    <a class="dropdown-item" href="#">1.25</a>
                    <a class="dropdown-item" href="#">1.75</a>
                    <a class="dropdown-item" href="#">2</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-transcript">
    <div class="transcript-wrapper">
        <div class="collapse" id="transcript-content">
            <header class="section-header section-header-sm transcript-content-header">Episode Transcript</header>
            <div class="transcript-content-wrapper transcript-content-purple">
                <ul>
                    <li>
                        <span>0s</span>
                        <div>(Intro Music)</div>
                    </li>

                    <li class="is-active">
                        <span>0.5s</span>
                        <p>An all-audio exclusive in-house channel carrying fresh insights from the world of work to all our learners. Listen to The Radio Channel currently live with the following segments of audio-learning streams, plunge into our bite-sized playlists now.</p>
                    </li>

                    <li>
                        <span>1.6s</span>
                        <div>Host:</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem</p>
                    </li>

                    <li>
                        <span>5.0s</span>
                        <div>PB Balaji:</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem</p>
                    </li>

                    <li>
                        <span>1.6s</span>
                        <div>Host:</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem</p>
                    </li>

                </ul>
            </div>
        </div>


        <button class="btn-toggle-transcript" style="background:#3D0080;" data-toggle="collapse" href="#transcript-content">
            <span>View Transcript</span>
        </button>
    </div>
</section>
 
<section class="section-related-episode">
    <div class="">
        <header class="section-header section-header-sm text-center">Listen to more episodes</header>
        <div class="related-podcast-slider">
            <?php for ($x = 1; $x <= 6; $x++) { ?>
                <div>
                    <div class="rps-wrapper">
                        <a href="" class="podcast-card podcast-card-purple">
                            <div class="podcast-card-image">
                                <img src="img/backgrounds/user.png" class="img-fluid" alt="">
                            </div>
                            <div class="podcast-card-content-container">
                                <div class="podcast-card-header">
                                    <img src="img/backgrounds/brandbuilder-podcast-icon.svg" width="30" height="28" alt="">
                                    <span class="se">S1: EP2</span>
                                </div>

                                <div class="podcast-card-content ">
                                    <div>
                                        <h6 class="podcast-card-title">Anubhuti Banerjee</h6>
                                        <p class="podcast-card-description">Learning the ABC of LGBTQIA+ </p>
                                    </div>
                                </div>

                                <div class="podcast-card-footer">
                                    <span>Listen Now</span>&nbsp;&nbsp;
                                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                        <g transform="translate(-424 -2476)">
                                            <g transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                <circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
                                                <circle cx="9.5" cy="9.5" r="9" fill="none" />
                                            </g>
                                            <path d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>