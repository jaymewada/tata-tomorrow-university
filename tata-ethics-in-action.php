<?php include "components/header.php" ?>
<br class="d-md-none">
<header class="section-header global-header-margin text-center mb-4">Tata Ethics in Action</header>

<section class="section-ethics-conclave tata-ethics-in-action">
    <img src="img/backgrounds/blue-d.svg"class="tec-d-1">
    <img src="img/backgrounds/blue-d-right.svg"class="teia-2">
    <img src="img/backgrounds/bars-white.svg" class="tec-bars"  data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center mb-4">
                <img src="img/backgrounds/tata-eia.png" class="w-75 mx-auto" alt="">
            </div>
            <div class="col-md-11 col-lg-6 align-self-center mx-auto">
                <!-- SECOND EDITION -->
                <div class="mb-5">
                    <h5 class="section-header section-header-sm text-light">2nd Edition - 2022</h5>
                    <p>The 2nd edition of the competition witnessed registration from 133 teams consisting of 543 participants across 40 Tata companies. The competition was held over three rounds through virtual platform, 50 teams were shortlisted for presentation in the first round based on their written analysis of a common case study. The case study for first round was focused on the underlying ethical issue faced by a multinational fashion brand, due to inappropriate practices at one of its strategic value chain partners, impacting its brand image. Post an exhaustive presentation session by the 50 teams to 2-member jury panels, 20 teams progressed to the second round.</p>
                    <p>In second round the teams had to present their analysis of another common case study, to the 2-member jury panels. This case study was based on a multinational telecommunications service provider facing a serious concern of data privacy breach for its large number of subscribers. Out of the 20 teams, 4 teams made it to the final round. Post intense presentations and discussions by the 4 finalist teams, Team Transparency Defenders emerged as the winning team.</p>

                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="btn-cta-navy" style="width:200px;">Play Video</a>
                </div>

                <!-- FIRST EDITION -->
                <div class="mb-5">
                    <header class="section-header section-header-sm text-light">1st Edition - 2021</header>
                    <p>The competition was launched on 29th July 2021 coinciding JRD Tata’s birth anniversary. The competition was conducted in two phases - qualifying round and final round. The event received overwhelming response, 78 teams comprising 267 participants from 39 Tata companies from across geographies participated in the competition.</p>
                    <p>Team Sunitee from Tata Motors emerged as winner. Team Ethicist from Tata Steel BSL, Team MTHL from Tata Projects and Wonder Women from Hitachi from Tata Hitachi were other 3 team qualified for final round.</p>

                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="btn-cta-navy" style="width:200px;">Play Video</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>