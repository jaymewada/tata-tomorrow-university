<!-- Developed with ❤ by Jay Mewada -->
<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/custom-programmes-banner.png">
</section>

<section class="banner-shadow-content text-center">
    We understand that different organisations have different needs – and at the Tata group, we have businesses that are as different as coffee is from cars, salt from software and apparel from airlines! Even within an organisation, different teams straddling different roles and functions have very different learning needs and development journeys.
</section>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list">
            <li>
                <a class="hash-link" href="#about">About</a>
            </li>
            <li>
                <a class="hash-link" href="#our-approach">Our Approach</a>
            </li>
            <li>
                <a class="hash-link" href="#our-promise">Our Promise</a>
            </li>
            <li>
                <a class="hash-link" href="#custom-solutions">Custom Solutions</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-cs-about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header mb-4">About</header>
                <img src="img/backgrounds/bars-skyblue-sm.svg" class="cs-about-element-1 mt-0" data-aos="fade-in">
            </div>
            <div class="col-md-8 pt-2 pt-md-0">
                <p>Tata Management Training Centre works with individual companies to create high-impact learning experiences that are tailo#4489C7 to their specific capability development needs. These customised interventions are designed to enhance both business-critical functional skills as well as leadership capabilities. Participants take back new perspectives, knowledge and skill sets that enable them to contribute effectively towards achieving desi#4489C7 business results and building thriving workplaces.</p>
                <p>We offer learning solutions in the areas of Commercial Acumen, Digital Transformation, Communication, Strategy & Innovation, Customer Centricity, Finance, Ethics, Diversity, Equity & Inclusion, Analytics, and Managing Self & Others.</p>
                <p>With more than five decades of experience in executive education, TMTC can partner your organisation to provide varied teams with world-class learning solutions and leadership development journeys. </p>
            </div>
            <div class="col-12 text-right">
                <img src="img/backgrounds/skyblue-rod.svg" class="cs-about-element-2" data-aos="fade-in">
            </div>
        </div>
    </div>
</section>

<section class="section-cs-stats">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <div class="cs-stats">
                            <div class="d-flex align-items-center">
                                <span>2000+</span>
                                <p>Learners reached <br> Through our custom <br class="d-none d-md-block"> Programmes Each Year</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="cs-stats">
                            <div class="d-flex align-items-center">
                                <span>75+</span>
                                <p>Custom <br class="d-none d-md-block"> programmes <br> Each Year</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-cs-endorsements">
    <header class="section-header">Our Endorsements</header>

    <div class="cs-endorsements-slider">
        <?php for ($x = 0; $x <= 4; $x++) { ?>
            <div>
                <div class="endorsements-slide">
                    <h5>Karan Bhatia</h5>
                    <strong>Seminar Director</strong>
                    <span>Organisation Name</span>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo</p>
                </div>
            </div>
        <?php } ?>
    </div>
    </div>
</section>

<section class="section-cs-approach" id="our-approach">
    <div class="container">
        <header class="section-header text-center">Our Approach</header>

        <div class="cs-approach-card">
            <span style="color:#66AAE6;">1</span>
            <div class="cs-approach-card-1" style="background-color:#66AAE6;left:0;">
                <div class="approach-card-1-arrow"></div>
                <strong>Diagnostic Study</strong>
                <ul class="dot-list white-dots">
                    <li>Observe, audit and diagnose the learning needs of your organisation. </li>
                    <li>A deep understanding of your operating environment and the challenges and needs of the participants. </li>
                    <li>Align our learning inputs to your specific context.</li>
                </ul>
            </div>
        </div>

        <div class="cs-approach-card">
            <span style="color:#4489C7;right:0;">2</span>
            <div class="cs-approach-card-2" style="background-color:#4489C7;">
            <div class="approach-card-2-arrow"></div>
                <strong>Programme Design</strong>
                <ul class="dot-list white-dots">
                    <li>Most suitable development inputs and learning methods are determined as per client’s specific needs</li>
                    <li>Our programmes include non-linear immersive elements such as action learning project, capstone, simulation, assessment, Virtual instructor-led training (VILT), in-person contact sessions and classroom sessions</li>
                </ul>
            </div>
        </div>

        <div class="cs-approach-card">
            <span style="color:#1468B3;left:0;">3</span>
            <div class="cs-approach-card-3" style="background-color:#1468B3;">
            <div class="approach-card-3-arrow"></div>
                <strong>Programme Delivery</strong>
                <ul class="dot-list white-dots">
                    <li>Our custom programmes are built on a multi-modal delivery mechanism, to ensure a holistic learning journey for our diverse participants.</li>
                    <li>Our programme delivery methods include wholly virtual, hybrid, in-person on the TMTC campus and at client’s prefer#4489C7 location.</li>
                </ul>
            </div>
        </div>

        <div class="cs-approach-card">
            <span style="color:#0B5291;right:0;">4</span>
            <div class="cs-approach-card-4" style="background: #0B5291;right:0">
                <div class="approach-card-4-arrow"></div>
                <strong>Post-Programme Engagement</strong>
                <ul class="dot-list white-dots">
                    <li>Our post-programme engagement includes review of action learning projects, coaching follow-ups, continuous learning on our Learning Experience Platform (LXP) through webinars etc.</li>
                    <li>We also collect post-programme feedback from participants to improve the design for future interventions and offer the best learning experience.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-cs-promise" id="our-promise">
    <div class="container">
        <header class="section-header text-center">Our Promise</header>
        <div class="row">
            <div class="col-md-8 col-lg-4 mx-auto">
                <div class="cs-promise-card">
                    <img src="img/backgrounds/cs-promise-1.svg">
                    <p>Learning that is contextual (relevant case studies and examples, sometimes from the client organization)</p>
                </div>
            </div>

            <div class="col-md-8 col-lg-4 mx-auto">
                <div class="cs-promise-card">
                    <img src="img/backgrounds/cs-promise-2.svg">
                    <p>Engaging, contemporary, transformational experiences – interactive faculty-led sessions, simulations, role plays, outbound activities, learning immersions, coaching, e-learning</p>
                </div>
            </div>

            <div class="col-md-8 col-lg-4 mx-auto">
                <div class="cs-promise-card">
                    <img src="img/backgrounds/cs-promise-3.svg">
                    <p>Perspectives on the “Tata” Way of Leadership – experience sharing by senior group leaders on relevant topics</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-cs-companies">
    <div class="container">
        <header class="section-header text-center">Companies we <br class="d-md-none"> have worked with</header>
        <div class="row">
            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Consultancy Services Ltd.</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Motors Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7">
                    <span>Tata Steel Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata AutoComp Systems Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Consumer Products Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata AIA Life Insurance Co Ltd.</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata AIA Life Insurance Co Ltd.</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Capital Financial Services Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7">
                    <span>Tata Projects Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Teleservices Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Elxsi Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>The Tata Power Company Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Digital Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Chemicals Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7">
                    <span>Tata Consulting Engineers Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7">
                    <span>Air Asia India (Private) Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata AIG General Insurance Co Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Coffee Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Unistore Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Infiniti Retail Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Realty and Infrastructure Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Metaliks Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata International Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Communications Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Mjunction Services Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Advanced Systems Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Steel Long Products Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Jaguar Land Rover India Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Titan Company Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Sons Private Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Business Excellence Group</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Business Hub Limited (TBH)</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Technologies Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>JAMIPOL</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Voltas Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Indian Hotels Company Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata NYK Shipping Pte Liimited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Jaguar Land Rover Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Trent Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Services Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Capital Housing Finance Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Play Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata ClassEdge</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Industries Limited-DHP Division</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>TATA 1MG Technologies Private Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Insights & Quants</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Electronics Private Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Rallis India Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Strategic Management Group</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata NYK Shipping (India) Pvt Ltd</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Advanced Systems Limited (DTA)</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Housing Development Co. Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Community Initiatives Trust</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>TATA 1MG Healthcare Solutions Pvt Ltd</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>TATA Boeing Aerospace Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Trent Hypermarket Pvt. Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Asset Management Ltd.</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Steel Utilities & Infrastructure Services Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>TATA SmartFoodz Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>The Tinplate Company of India Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>TRL Krosaki Refractories Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Steel Downstream Products Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata BlueScope Steel Private Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Lockheed Martin Aerostructures Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Medical and Diagnostics Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Industries Limited</span>
                </div>
            </div>


            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Consumer Products Canada Inc</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card">
                    <span>Tata Motors Finance Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>TM Automotive Seating Systems Pvt Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#4489C7;">
                    <span>Tata Sky Broadband Pvt. Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Steel BSL Limited</span>
                </div>
            </div>

            <div class="col-6 col-md-3 col-lg-2 mb-30">
                <div class="cs-companies-card" style="border-color:#0B5291;">
                    <span>Tata Industries Ltd-Tata Insights & Quants Division</span>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="section-recent-cs" id="custom-solutions">
    <div class="container" style="max-width:992px !important;">
        <header class="section-header text-center">Some recent custom solutions</header>
        <div class="row">
            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-1" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Helping First-time Managers See the Bigger Picture</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-2" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Leadership Wings</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-3" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Pivot to Grow</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-4" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>The Trusted Advisor Mindset</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-5" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Achieve Your Leadership Potential</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-6" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Resonant Leadership: <br>Leading with EQ</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-7" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Leadership Voyages</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-8" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Design Thinking</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>

            <div class="col-6 col-md-4 mb-40">
                <a href="#cs-data-9" class="recent-cs-card" data-fancybox="gallery">
                    <div class="recent-cs-card-stripe"></div>
                    <div class="recent-cs-card-content">
                        <h6>Digital Transformation & Innovation Management</h6>
                    </div>
                    <span class="recent-cs-card-link">Read More</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- CS MODAL DATA -->
<div class="recent-cs-modal" id="cs-data-1">
    <!-- <img src="img/backgrounds/rcs-1.svg" class="recent-cs-modal-logo"> -->
    <header>Helping First-time Managers See the Bigger Picture | Rallis</header>
    <p>The main ask was to develop Strategic Thinking capabilities among first-time managers to help them understand the implications of business decisions on a company’s financial performance. Secondly, there was a need to equip the cohort to be able to take critical business decisions that contribute to organisational growth.</p>
    <p>To address this need, a customised two-day programme was developed that focused on specific customer segments and customer-centric priorities. The aim was to help them make the mental shift from “doing to enabling”, focusing on the team’s performance, learning how to help others develop, grow and succeed.</p>
    <p class="mb-2"><strong>The two-day Management Development Programme focused on:</strong></p>
    <p><strong>Customer Centricity:</strong> Fostering trust for enduring relationships with target customers</p>
    <p><strong>Strategic Clarity and Agility:</strong> The overall purpose, strategy and vision</p>
    <p><strong>Culture and Collaboration towards Social Responsibility:</strong> Sha#4489C7 mindset, identity and values with strong teamwork and appreciation of cross-functional interdependencies.</p>
    <p><strong>Cultivating Efficiency</strong> by leveraging innovation and technology.</p>
</div>

<div class="recent-cs-modal" id="cs-data-2">
    <!-- <img src="img/backgrounds/rcs-2.svg" class="recent-cs-modal-logo"> -->
    <header>Leadership Wings | Tata Advanced Systems (Aerospace and Aerostructure) </header>
    <p>The client organisation requested a learning intervention gea#4489C7 towards the needs of mid-level managers in the Aerospace division who are poised to take on larger, strategic roles within the organisation. </p>

    <p>
        <strong>We designed a month-long programme in collaboration with Deakin University to unlock four capabilities: </strong>
    </p>

    <p>
        <strong>Strategic Thinking</strong>
    </p>

    <p>
        <strong>Managing Uncertainty </strong>
    </p>

    <p>
        <strong>Innovation </strong>
    </p>

    <p>
        <strong>People </strong>
    </p>

    <p>The custom programme is deployed through classroom lecture sessions and a Challenge Workshop for each module, which can be worked in groups. The programme culminates with a ‘Think like a CEO’ presentation. </p>
    <p>Three batches of the Aerospace and Aerostructure division have already experienced this programme</p>

</div>

<div class="recent-cs-modal" id="cs-data-3">
    <!-- <img src="img/backgrounds/rcs-3.svg" class="recent-cs-modal-logo"> -->
    <header>Pivot to Grow | Tata Asset Management Company </header>
    <p>The client organisation was looking for a programme that would equip their mid-level managers, primarily in Sales, Fund Management, Investment and Support functions, with skills necessary to drive organisational
        growth in a hypercompetitive environment. </p>

    <p>We designed a one-month leadership programme to help the organisation’s talent deliver the strategic priorities for the future by building requisite knowledge, skills, and mindsets. <br>The modules focused on :</p>

    <p><strong>Strategic Thinking </strong></p>
    <p><strong>Design Thinking</strong></p>
    <p><strong>Customer Centricity</strong></p>
    <p><strong>People management</strong></p>
    <p>Deployed online to two batches of 28 each, the programme also contained an Action Learning Project and the learning outcomes included change readiness, agility, taking ownership, building a personal brand, building, and managing high-performance teams, problem solving, financial acumen, data-driven decision-making, among others.</p>
</div>

<div class="recent-cs-modal" id="cs-data-4">
    <!-- <img src="img/backgrounds/rcs-4.svg" class="recent-cs-modal-logo"> -->
    <header>The Trusted Advisor Mindset | Tata Elxsi </header>
    <p>The ask was to enable client partners, programme managers and delivery managers to lead the transformation agenda for their clients and become their trusted advisors. Tata Elxsi wanted its teams to pick up future-forward skills that would help them understand, create, and communicate value for client organisations effectively. This would help them future proof their clients’ businesses, in the process creating and capturing value for Tata Elxsi. </p>
    <p>Understanding the specific needs, we created a two-day programme titled ‘The Trusted Advisor Mindset’ that helps build executive presence by developing capabilities in Strategic Thinking, Consultative Selling, Personal Branding, Commercial Acumen, Influencing and Impactful CXO conversations. </p>
</div>

<div class="recent-cs-modal" id="cs-data-5">
    <!-- <img src="img/backgrounds/rcs-5.svg" class="recent-cs-modal-logo"> -->
    <header>Achieve Your Leadership Potential | Tata Power</header>
    <p>Tata Power is the country’s largest integrated power utility with many pioneering initiatives to its credit. In line with the company’s views on sustainable and clean energy development, Tata Power is steering the transformation of utilities to integrated solutions. As the company undergoes this transformational journey to become a utility of the future, there emerged a need to help the mid-level leadership acquire capabilities necessary for delivering value and contributing to the strategic priorities of the organisation. </p>
    <p>To address this critical requirement, we developed a six-month learning journey that helped participants develop, first and foremost, a Strategic Mindset. The design ensu#4489C7 they understood and imbibed Innovation Culture and Capability and attempted to deepen Self-awareness, Interpersonal Effectiveness and People Leadership. </p>
    <p>Deployed through a mix of classroom sessions, webinars, coaching and Action Learning Projects, the aim was to steer the teams towards a growth and value creation orientation, become more sales and customer-centric, enhance project management capabilities and sharpen business and digital acumen, especially how to leverage data for insights and decision-making.</p>
</div>

<div class="recent-cs-modal" id="cs-data-6">
    <!-- <img src="img/backgrounds/rcs-6.svg" class="recent-cs-modal-logo"> -->
    <header>Resonant Leadership: Leading with EQ | Tata Steel </header>
    <p>The ask was for a programme customised to the needs of managers in IL3, who have recently transitioned from IL4, that is, from an IC to a people’s manager. The main focus is on developing Emotional Intelligence while dealing with teams and stakeholders, to enable leaders to take people along to deliver performance. </p>
    <p>After assessing the needs of the new managers in the client organisation, we designed a learning intervention that helps participants understand the role of emotions and emotional Intelligence in leadership, explore nuances of purpose-driven leadership, recognise, and comprehend the traits of personality through Big 10 psychometric instrument, develop leadership capability for effective task management and enhance effectiveness for self and the team. The programme also taught them the art of giving feedback, work delegation and building trust with the team.</p>
    <p>This four-day programme has already cove#4489C7 200-plus participants from Tata Steel.</p>
</div>

<div class="recent-cs-modal" id="cs-data-7">
    <!-- <img src="img/backgrounds/rcs-7.svg" class="recent-cs-modal-logo"> -->
    <header>Leadership Voyages | Tata Consumer Products </header>
    <p>Leadership Voyages is a General Management and Leadership Development programme designed by the SDA Bocconi School of Management, Italy, in partnership with the Tata Management Training Centre. </p>
    <p>This high-impact development expedition was contextualized to the client organisation’s growth aspirations and customised to the FMCG domain. Both academically rigorous and professionally relevant for the handpicked talent of Tata Consumer Products, the 19-day learning intervention was deployed through three modules — People, Strategy and Change. </p>
    <p>What is notable about Leadership Voyages was how the modules incorporated the 6 strategic pillars of the client organisation to build capabilities in Strategic Thinking, Learning Agility, Digital Acumen and Innovation, Cooperation and Conflict Resolution, Entrepreneurial Mindset, Purpose-led Alignment, Future-Ready Mindset and Sustainable Business Practices. </p>
    <p>Apart from the classroom sessions, the cohort attended two open-mics by senior leaders from other Tata companies and got the opportunity to understand how the programme learnings have been/will be implemented in current roles with metrics to measure. </p>
</div>

<div class="recent-cs-modal" id="cs-data-8">
    <!-- <img src="img/backgrounds/rcs-8.svg" class="recent-cs-modal-logo"> -->
    <header>Design Thinking | Tata Consultancy Services</header>
    <p>The client organisation wanted a learning intervention to help team members become adept at understanding their customer contexts and develop solutions that are relevant and beneficial for their customers. In short, transform high-potential employees from being ‘order takers’ (executing what has been decided) to ‘order makers’ (being a partner with the customer in decision making) – and thus moving up the value chain. </p>

    <p>After analysis of the client’s needs, we decided to create an application-oriented workshop on Design Thinking. The core objective of the programme, which has completed 5 batches already, is for the participants to gain new insights into the evolving business world and understand the need for new types of thinking. The programme helps learners understand customer psychology better and seek opportunities in the changing context. Over the course of three months, they learn how to tap into breakthrough solutions from similar problems in other domains and how to develop and prioritise a portfolio of opportunities. Most importantly for today’s fast-changing business environment, they learn to drive innovation across the organisations through small experiments. </p>
</div>

<div class="recent-cs-modal" id="cs-data-9">
    <!-- <img src="img/backgrounds/rcs-9.svg" class="recent-cs-modal-logo"> -->
    <header>Digital Transformation & Innovation Management | Tata Metaliks</header>
    <p>The client organisation wanted to build capability around digital transformation and innovation strategy for their senior leadership team. The aim was to help them understand how digital transformation was changing key aspects of doing business globally, cutting across facets like processes, customer connect, decision-making and innovation. In short, they needed a program to enable participants to design the Tata Metaliks 2.0 strategic roadmap.</p>
    <p>A customised programme was designed and deployed in hybrid mode to help the Tata Metaliks senior leadership understand what “digital” means in the context of their organisation. The programme aimed at providing a comprehensive understanding of how new digital technologies were disrupting industries and business operations, on building a customer-focused ecosystem, and innovative products and services. </p>
    <p>In addition, focused modules were created on building leadership capabilities and mindsets to help them pivot in the changing environment. Another significant facet was learning through live example of innovative application of technologies in various sectors, along with their strategic rationales and implementation challenges. Industry immersions and deep-tech startups interactions enriched the programme content further. </p>
    <p>As part of the programme, an off-site intervention was planned to build future vision for the client organisation, to help build strategy and implementation roadmaps for the near-term. Goals were also set for the mid-term and long-term goals. </p>
</div>


<section class="section-site-contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="pr-md-5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="55.76" height="66" viewBox="0 0 55.76 66">
                        <path id="Polygon_3" data-name="Polygon 3" d="M33,0,66,55.76H0Z" transform="translate(55.76) rotate(90)" fill="#1468B3" />
                    </svg>
                    <header class="section-header">Write to us to know more</header>
                    <p class="section-description">
                        If you have queries about the programmes or would like to know how to nominate/get nominated, send us a
                        message!
                    </p>

                    <img src="img/backgrounds/bars-sky-blue-x2.svg" class="img-fluid pb-5" data-aos="fade-in">
                </div>
            </div>
            <div class="col-lg-7">
                <form action="" class="site-contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Your Name" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Last Name" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Company" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex w-100">
                                <div class="digital-patterns-select-wrapper">
                                    <select name="" id="" class="digital-patterns-select" style="width: 80px">
                                        <option value="">+91</option>
                                        <option value="">+91</option>
                                        <option value="">+91</option>
                                    </select>
                                </div>
                                <div class="digital-patterns-input-wrapper ml-auto" style="width: calc(100% - 95px)">
                                    <input type="text" class="digital-patterns-input" placeholder="Phone Number" />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="digital-patterns-input-wrapper">
                                <input type="text" class="digital-patterns-input" placeholder="Email Address" />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="digital-patterns-textarea-wrapper">
                                <textarea class="digital-patterns-textarea" rows="6">Message</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="btn-cta-navy">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>