<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/pi-banneer-3.jpg" />
</section>

<section class="section-gi-about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">#727272</header>
                <strong class="text-dark">Current Color on all Grey Sections</strong>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p class="mb-0">The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>


<br>

<section class="section-gi-about" style="color:#696969;" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">#696969</header>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p class="mb-0">The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>

<br>

<section class="section-gi-about" style="color:#484848;" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">#484848</header>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p class="mb-0">The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>

<br>

<section class="section-gi-about" style="color:#303030;" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">#303030</header>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p class="mb-0">The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>

<br>

<section class="section-gi-about" style="color:#202020;" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">#202020</header>
            </div>
            <div class="col-md-9 col-lg-8 align-self-center">
                <p class="mb-0">The programme is intended to provide you with insights into the foundational and transformational pillars of the Tata group that will support you in maximising you impact in your new leadership role. Understanding the Tata organisational context, its purpose, how the organisation work, the culture and ecosystem of Tata, and the peer and leadership networks that you can build are some of the aspects that will help you at the start of your leadership journey at Tata.</p>
            </div>
        </div>
    </div>
</section>

<br>

<section class="section-learning-pedagogy msc">
    <div class="container">
        <header class="section-header text-center">Text on White Background</header>
        
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#7d7d7d;">#7d7d7d</div>
                    <strong class="text-dark">Current Colour on all white backgrounds</strong>
                    <p>Blended learning over a period of 10 weeks. This course offers 9 weeks of virtual learning and 3 days in-person learning @ TMTC</p>
                </div>
            </div>
            
            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#888888;">#888888</div>
                    <p style="color:#888888;">On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>
            
            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#585858;">#585858</div>
                    <p style="color:#585858;">On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>
            
            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#484848;">#484848</div>
                    <p style="color:#484848;">On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>


            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#282828;">#282828</div>
                    <p style="color:#282828;">On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header" style="color:#101010;">#101010</div>
                    <p style="color:#101010;">On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>
        </div>
    </div>
</div>
<img src="img/backgrounds/orange-rod-msc.svg" class="programme-inner-rod" alt="">
</section>





<?php include "components/footer.php" ?>