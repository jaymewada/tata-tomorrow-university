<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-padding">
    <div class="landing-banner-slider dots-dashed">
        <div>
            <img src="img/banners/ltc-banner-1.png" class="img-fluid d-none d-md-block" alt="">
            <img src="img/banners/ltc-banner-1-mobile.png" class="img-fluid d-md-none" alt="">
        </div>

        <div>
            <img src="img/banners/ltc-banner-2.png" class="img-fluid d-none d-md-block" alt="">
            <img src="img/banners/ltc-banner-2-mobile.png" class="img-fluid d-md-none" alt="">
        </div>

        <div>
            <img src="img/banners/ltc-banner-3.png" class="img-fluid d-none d-md-block" alt="">
            <img src="img/banners/ltc-banner-3-mobile.png" class="img-fluid d-md-none" alt="">
        </div>

    </div>

</section>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list">
            <li>
                <a class="hash-link" href="#values">Values</a>
            </li>
            <li>
                <a class="hash-link" href="#purpose">Purpose</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-ltc-values" id="values">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-5">
                <header class="section-header text-light mb-3">Who we are | Values</header>
                <p class="text-light mb-40">Tata has always been a values-driven organisation. These values continue to direct the growth and business of Tata companies.</p>
                <p>Play the video to know more about the values</p>

                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="d-table video-card mx-0 my-3">
                    <img src="img/backgrounds/ltc-video.png" class="img-fluid" alt="">
                </a>

            </div>

            <div class="col-md-6 col-lg-5 offset-lg-1">
                <p>The five core Tata values underpinning the way we do business are:</p>


                <div class="accordion" id="coreValuesAccordian">
                    <div class="single-accordian-wrapper">
                        <button type="button" class="btn-accordian" data-toggle="collapse" data-target="#cv_integrity" aria-expanded="true">
                            Integrity
                        </button>

                        <div id="cv_integrity" class="collapse show" data-parent="#coreValuesAccordian">
                            <div class="single-accordian-body">
                                <p>We will be fair, honest, transparent and ethical in our conduct; everything we do must stand the test of public scrutiny.</p>
                            </div>
                        </div>
                    </div>

                    <div class="single-accordian-wrapper">
                        <button class="collapsed btn-accordian" type="button" data-toggle="collapse" data-target="#cv_responsibility" aria-expanded="false">
                            Responsibility
                        </button>
                        <div id="cv_responsibility" class="collapse" data-parent="#coreValuesAccordian">
                            <div class="single-accordian-body">
                                <p>We will integrate environmental and social principles in our businesses, ensuring that what comes from the people goes back to the people many times over.</p>
                            </div>
                        </div>
                    </div>

                    <div class="single-accordian-wrapper">
                        <button class="collapsed btn-accordian" type="button" data-toggle="collapse" data-target="#cv_excellence" aria-expanded="false">
                            Excellence
                        </button>
                        <div id="cv_excellence" class="collapse" data-parent="#coreValuesAccordian">
                            <div class="single-accordian-body">
                                <p>We will be passionate about achieving the highest standards of quality, always promoting meritocracy.</p>
                            </div>
                        </div>
                    </div>

                    <div class="single-accordian-wrapper">
                        <button class="collapsed btn-accordian" type="button" data-toggle="collapse" data-target="#cv_pioneering" aria-expanded="false">
                            Pioneering
                        </button>
                        <div id="cv_pioneering" class="collapse" data-parent="#coreValuesAccordian">
                            <div class="single-accordian-body">
                                <p>We will be bold and agile, courageously taking on challenges, using deep customer insight to develop innovative solutions.</p>
                            </div>
                        </div>
                    </div>

                    <div class="single-accordian-wrapper">
                        <button class="collapsed btn-accordian" type="button" data-toggle="collapse" data-target="#cv_unity" aria-expanded="false">
                            Unity
                        </button>
                        <div id="cv_unity" class="collapse" data-parent="#coreValuesAccordian">
                            <div class="single-accordian-body">
                                <p>We will invest in our people and partners, enable continuous learning, and build caring and collaborative relationships based on trust and mutual respect.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-ltc-purpose" id="purpose">
    <div class="container" style="max-width:1024px">

        <div class="row">
            <div class="col-md-8 col-lg-7 text-center mx-auto">
                <header class="section-header mb-3">What we do | Purpose</header>
                <p class="section-description md-4 mb-md-5">To improve the quality of life of the communities we serve globally, through long-term stakeholder value creation based on ‘Leadership with Trust’. ​</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 mb-30">
                <a href="best-practices-sharing.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="56.53" height="56.529" viewBox="0 0 56.53 56.529">
                            <g id="Group_4098" transform="translate(-8085 9335)">
                                <rect width="56" height="56" transform="translate(8085 -9335)" fill="#fff" opacity="0" />
                                <g  transform="translate(-150.13 -0.236)">
                                    <path class="fill-white stroke-white" id="Path_1210" d="M280.646,236.93a6.467,6.467,0,1,0,6.467,6.467A6.474,6.474,0,0,0,280.646,236.93Zm0,11.736a5.27,5.27,0,1,1,5.269-5.269A5.275,5.275,0,0,1,280.646,248.666Z" transform="translate(7982.395 -9549.896)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" d="M174.761,122.762V116.2h-4.729a13.51,13.51,0,0,0-1.577-3.8l3.345-3.345-4.637-4.636-3.345,3.345a13.539,13.539,0,0,0-3.8-1.577v-4.729h-6.557v4.729a13.517,13.517,0,0,0-3.8,1.576l-3.345-3.345-4.637,4.636,3.345,3.345a13.559,13.559,0,0,0-1.576,3.8h-4.728v6.557h4.728a13.541,13.541,0,0,0,1.577,3.8l-3.345,3.344,4.637,4.637,3.345-3.344a13.506,13.506,0,0,0,3.8,1.576V137.5h6.557v-4.728a13.536,13.536,0,0,0,3.8-1.576l3.345,3.344L171.8,129.9l-3.345-3.344a13.544,13.544,0,0,0,1.577-3.8h4.728Zm-5.793-.721a12.368,12.368,0,0,1-1.775,4.277l-.267.407,3.181,3.18-2.944,2.945-3.18-3.18-.407.266a12.373,12.373,0,0,1-4.276,1.775l-.477.1v4.5h-4.163v-4.5l-.477-.1a12.356,12.356,0,0,1-4.277-1.775l-.407-.266-3.18,3.18-2.944-2.945,3.181-3.18-.267-.407a12.349,12.349,0,0,1-1.775-4.277l-.1-.477h-4.5V117.4h4.5l.1-.477a12.371,12.371,0,0,1,1.774-4.276l.267-.407-3.18-3.18,2.944-2.944,3.18,3.181.407-.267a12.359,12.359,0,0,1,4.277-1.775s.358,1.05.477-.1,0-4.5,0-4.5h4.163v4.5l.477.1a12.387,12.387,0,0,1,4.276,1.775l.407.267,3.18-3.181,2.944,2.944-3.181,3.18.267.407a12.365,12.365,0,0,1,1.775,4.276l.1.477h4.5v4.163h-4.5Z" transform="translate(8106.302 -9425.982)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white" id="Path_1212" d="M5.958,4.432A21.8,21.8,0,0,1,31.321,8.3l1.411-1.41A23.8,23.8,0,0,0,4.713,2.839L4.406.234,0,7.593l8.3-2.17Z" transform="translate(8268.515 -9334.764) rotate(45)" fill="#4f7eba" />
                                    <path class="fill-white" id="Path_1430" d="M26.774,3.868A21.8,21.8,0,0,1,1.411,0L0,1.41A23.8,23.8,0,0,0,28.019,5.46l.308,2.6L32.732.706l-8.3,2.17Z" transform="translate(8240.998 -9307.248) rotate(45)" fill="#4e7eb9" />
                                </g>
                            </g>
                        </svg>
                        <h5 class="shadow-card-header">Best Practices Sharing</h5>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="model-policies-procedures.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="56" height="56" viewBox="0 0 56 56">
                            <g id="Group_4082" transform="translate(-8166 9335)">
                                <rect id="Rectangle_1973" width="56" height="56" transform="translate(8166 -9335)" fill="#fff" opacity="0" />
                                <g id="Group_4075" transform="translate(-72.745 -174.398)">
                                    <path class="fill-white stroke-white" id="Path_1254" d="M283.572,328.9a12.016,12.016,0,0,1-6.642-3.073l-.017-.014-.023-.018h0l-.024-.018-.027-.02-.022-.013-.031-.018-.022-.01-.033-.014-.023-.007-.034-.011-.023-.006-.034-.007h-.2l-.031.005-.028.006-.029.008-.033.006-.028.01-.028.013-.027.013-.027.016-.026.016-.025.018-.026.02-.021.018-.018.016a12.006,12.006,0,0,1-6.638,3.071.621.621,0,0,0-.563.536c-.011.091-1.073,9.159,7.439,11.8h.011l.043.009h0a.382.382,0,0,0,.045.01.575.575,0,0,0,.094.008.561.561,0,0,0,.093-.008.475.475,0,0,0,.051-.011l.038-.008h.013c8.51-2.646,7.451-11.714,7.438-11.8h0A.621.621,0,0,0,283.572,328.9Zm-7.07,11.111c-6.38-2.09-6.542-8.187-6.449-9.947a13.287,13.287,0,0,0,6.449-2.96,13.246,13.246,0,0,0,6.449,2.96C283.044,331.825,282.881,337.922,276.5,340.012Z" transform="translate(7990.511 -9454.031)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1255" d="M317.835,373.4a.614.614,0,0,0-.867.072l-4.139,4.892-1.248-1.048h0a.616.616,0,0,0-.792.943l1.718,1.442a.613.613,0,0,0,.865-.074l4.536-5.36h0a.615.615,0,0,0-.074-.867Z" transform="translate(7952.702 -9497.114)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1256" d="M179.5,16.316H168.955V15.4a.621.621,0,0,0-.616-.615H164.4a4.069,4.069,0,0,0-4.064-4.074h-.069A4.069,4.069,0,0,0,156.2,14.77h-3.939a.621.621,0,0,0-.616.615v.931H141.1a.621.621,0,0,0-.621.621V61.046a.621.621,0,0,0,.621.616h38.4a.621.621,0,0,0,.616-.616V16.927a.621.621,0,0,0-.616-.611Zm-27.24,6.875h16.076a.62.62,0,0,0,.616-.621v-1.59h6.182v36.01H145.465V20.981h6.182v1.595h0a.621.621,0,0,0,.616.616Zm.621-7.186h3.939a.621.621,0,0,0,.616-.621v-.621a2.837,2.837,0,0,1,2.833-2.832h.069a2.835,2.835,0,0,1,2.832,2.832v.621a.621.621,0,0,0,.621.621h3.94V21.96H152.879Zm26,44.425H141.714V17.542h9.934v2.207h-6.8a.621.621,0,0,0-.621.616V57.6a.621.621,0,0,0,.621.621h30.9a.621.621,0,0,0,.616-.621V20.365a.621.621,0,0,0-.616-.616h-6.8V17.542h9.934Z" transform="translate(8106.712 -9168.867)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1257" d="M242.945,178.413a.621.621,0,0,0,.621.616h19.019a.616.616,0,1,0,0-1.23H243.56a.615.615,0,0,0-.616.615Z" transform="translate(8013.94 -9320.15)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1258" d="M262.352,236.325H243.327a.616.616,0,1,0,0,1.23h19.024a.616.616,0,1,0,0-1.23Z" transform="translate(8014.174 -9373.14)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1259" d="M250.566,295.468a.615.615,0,0,0-.615-.616h-6.386a.616.616,0,0,0,0,1.231h6.386A.621.621,0,0,0,250.566,295.468Z" transform="translate(8013.935 -9426.13)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                </g>
                            </g>
                        </svg>

                        <h5 class="shadow-card-header">Model Policies and Procedures</h5>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="annual-compliance-reporting.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="56" height="56" viewBox="0 0 56 56">
                            <g id="Group_4087" transform="translate(-8085 9265)">
                                <rect id="Rectangle_1977" width="56" height="56" transform="translate(8085 -9265)" fill="#fff" opacity="0" />
                                <g id="Group_4076" transform="translate(0.91 -2)">
                                    <path class="fill-white stroke-white" id="Path_1302" d="M120.917,92a3.262,3.262,0,0,1-2.381-.845,2.7,2.7,0,0,1-.262-3.512c.054-.089.125-.208.173-.3h-7.739V71.713a2.614,2.614,0,0,1,2.607-2.607h19.568v5.012c0,1.417.238,1.613.244,1.625h0a2.083,2.083,0,0,0,.506-.179,3.351,3.351,0,0,1,.875-.274,2.638,2.638,0,0,1,2.1.6,3.411,3.411,0,0,1,1.036,2.709,3.253,3.253,0,0,1-.851,2.381,2.853,2.853,0,0,1-2.072.78,2.787,2.787,0,0,1-1.417-.524c-.1-.059-.232-.137-.333-.184a3.554,3.554,0,0,0-.071.875v5.358h-8.733c-.316.071-.381.149-.381.149h0a2.173,2.173,0,0,0,.173.5,3.316,3.316,0,0,1,.28.941,2.649,2.649,0,0,1-.6,2.1A3.423,3.423,0,0,1,120.917,92Zm-9.275-5.626h7.144a.912.912,0,0,1,.649.53,1.417,1.417,0,0,1-.321,1.191,2.18,2.18,0,0,0-.387.923,1.954,1.954,0,0,0,.506,1.4,2.346,2.346,0,0,0,1.715.6,2.588,2.588,0,0,0,1.953-.684,1.643,1.643,0,0,0,.381-1.345,2.587,2.587,0,0,0-.208-.6,1.542,1.542,0,0,1-.137-1.339,1.507,1.507,0,0,1,1.09-.643h7.894V81.958c0-.887.089-1.53.548-1.786s.911,0,1.316.268h0a2.081,2.081,0,0,0,.929.387,1.909,1.909,0,0,0,1.435-.5,2.345,2.345,0,0,0,.566-1.715,2.588,2.588,0,0,0-.738-1.97,1.65,1.65,0,0,0-1.345-.381,2.692,2.692,0,0,0-.6.2,1.453,1.453,0,0,1-1.411.1c-.47-.309-.667-1.012-.679-2.423V70.093H113.285a1.649,1.649,0,0,0-1.643,1.643Z" transform="translate(7976.941 -9324.506)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.5" />
                                    <path class="fill-white stroke-white" id="Path_1303" d="M207.662,124.54h-19.58v5c0,1.411.208,2.113.679,2.423a1.464,1.464,0,0,0,1.411-.089,2.458,2.458,0,0,1,.6-.208,1.65,1.65,0,0,1,1.345.381,2.59,2.59,0,0,1,.733,1.982,2.346,2.346,0,0,1-.6,1.715,1.935,1.935,0,0,1-1.393.5,2.144,2.144,0,0,1-.929-.381c-.4-.25-.869-.524-1.31-.274s-.554.9-.554,1.786V141.8h-7.894a1.506,1.506,0,0,0-1.089.643,1.543,1.543,0,0,0,.137,1.34,2.564,2.564,0,0,1,.208.6,1.643,1.643,0,0,1-.381,1.345,2.59,2.59,0,0,1-1.982.708,2.346,2.346,0,0,1-1.715-.566,1.939,1.939,0,0,1-.506-1.4,2.181,2.181,0,0,1,.387-.923,1.417,1.417,0,0,0,.322-1.191.911.911,0,0,0-.649-.53h-8.078v15.615a2.607,2.607,0,0,0,2.607,2.607h38.231a2.613,2.613,0,0,0,2.607-2.607V127.154a2.614,2.614,0,0,0-2.607-2.614Zm-19.05,27.432h0a.916.916,0,0,0-.53.631v6.477H169.431a1.649,1.649,0,0,1-1.643-1.643V142.775h6.787c-.048.1-.119.214-.173.3h0a2.7,2.7,0,0,0,.262,3.512,3.269,3.269,0,0,0,2.381.851,3.439,3.439,0,0,0,2.7-1.018,2.666,2.666,0,0,0,.6-2.1,3.568,3.568,0,0,0-.232-.875,1.963,1.963,0,0,1-.179-.5s.066-.077.381-.149h7.769v4.6a1.488,1.488,0,0,0,.643,1.072,1.542,1.542,0,0,0,1.34-.143,2.537,2.537,0,0,1,.6-.2,1.648,1.648,0,0,1,1.345.381,2.589,2.589,0,0,1,.708,1.982,2.344,2.344,0,0,1-.566,1.714,2.018,2.018,0,0,1-1.4.5,2.091,2.091,0,0,1-.923-.381,1.409,1.409,0,0,0-1.208-.345Zm20.694,5.465a1.649,1.649,0,0,1-1.643,1.643H189.029v-6.108l.3.167a2.756,2.756,0,0,0,1.423.524,2.864,2.864,0,0,0,2.1-.8,3.253,3.253,0,0,0,.851-2.381,3.43,3.43,0,0,0-1.036-2.709,2.579,2.579,0,0,0-.452-.315,2.51,2.51,0,0,0-1.066-.31,2.745,2.745,0,0,0-.6,0,3.311,3.311,0,0,0-.881.274,2.636,2.636,0,0,1-.5.179.776.776,0,0,1-.143-.369v-4.453h7.62c-.048.1-.125.232-.184.333a2.784,2.784,0,0,0-.524,1.417,2.873,2.873,0,0,0,.78,2.09,3.256,3.256,0,0,0,2.381.851,3.405,3.405,0,0,0,2.733-1.042,2.613,2.613,0,0,0,.6-2.1,3.071,3.071,0,0,0-.256-.875,1.932,1.932,0,0,1-.173-.506s.1-.119.637-.191h6.674Zm-6.7-15.627h-.059c-.744.1-1.191.3-1.387.667a1.475,1.475,0,0,0,.125,1.363,2.41,2.41,0,0,1,.2.6,1.679,1.679,0,0,1-.375,1.393,2.6,2.6,0,0,1-1.982.708,2.382,2.382,0,0,1-1.72-.566,1.975,1.975,0,0,1-.5-1.4,2.029,2.029,0,0,1,.387-.929c.244-.4.494-.821.3-1.256h0a1.149,1.149,0,0,0-.869-.548h-7.692v-4.453a3.569,3.569,0,0,1,.071-.881c.1.048.232.125.333.184h0a2.788,2.788,0,0,0,1.417.524,2.845,2.845,0,0,0,2.072-.78,3.25,3.25,0,0,0,.851-2.381,3.5,3.5,0,0,0-1.03-2.709,2.663,2.663,0,0,0-2.107-.6,3.571,3.571,0,0,0-.875.274,2.379,2.379,0,0,1-.5.179s-.238-.208-.25-1.619v-4.072h18.633a1.649,1.649,0,0,1,1.643,1.649V141.81Z" transform="translate(7926.79 -9374.048)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.5" />
                                    <path class="fill-white stroke-white" id="Path_1434" d="M317.835,373.4a.614.614,0,0,0-.867.072l-4.139,4.892-1.248-1.048h0a.616.616,0,0,0-.792.943l1.718,1.442a.613.613,0,0,0,.865-.074l4.536-5.36h0a.615.615,0,0,0-.074-.867Z" transform="translate(7784.957 -9622.513)" fill="#4f7eba" />
                                </g>
                            </g>
                        </svg>

                        <h5 class="shadow-card-header">Annual Compliance Reporting and Maturity Assessment</h5>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="business-ethics-framework.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content">
                        <svg  xmlns="http://www.w3.org/2000/svg" width="56" height="56" viewBox="0 0 56 56">
                            <g id="Group_4088" transform="translate(-8166 9265)">
                                <rect width="56" height="56" transform="translate(8166 -9265)" fill="#fff" opacity="0" />
                                <g transform="translate(5.889 -373.082)">
                                    <path class="fill-white stroke-white" id="Path_1435" d="M145.761,389.476v-8.658a2.793,2.793,0,0,0-2.79-2.79h-9.509a2.793,2.793,0,0,0-2.79,2.79v8.658a.628.628,0,0,0,.629.628h13.832a.627.627,0,0,0,.628-.628Zm-1.257-.629h-1.991v-5.58a.629.629,0,1,0-1.257,0v5.58h-6.077v-5.58a.629.629,0,1,0-1.257,0v5.58H131.93v-8.029a1.534,1.534,0,0,1,1.533-1.532h9.509a1.534,1.534,0,0,1,1.532,1.532Z" transform="translate(8033.08 -9230.94)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1436" d="M165.67,309.785a3.619,3.619,0,1,0,1.06-2.555,3.615,3.615,0,0,0-1.06,2.555Zm5.973,0a2.361,2.361,0,1,1-.691-1.666,2.359,2.359,0,0,1,.691,1.666Z" transform="translate(8002.011 -9167.15)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1437" d="M297.891,389.476v-8.658a2.793,2.793,0,0,0-2.79-2.79h-9.51a2.793,2.793,0,0,0-2.79,2.79v8.658a.628.628,0,0,0,.628.628h13.832a.628.628,0,0,0,.628-.628Zm-1.257-.629h-1.991v-5.58a.629.629,0,0,0-1.258,0v5.58h-6.078v-5.58h0a.629.629,0,0,0-1.258,0v5.58H284.06v-8.029a1.534,1.534,0,0,1,1.532-1.532h9.51a1.534,1.534,0,0,1,1.532,1.532Z" transform="translate(7898.029 -9230.94)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1438" d="M325.031,309.785a3.619,3.619,0,1,0-1.06,2.555,3.615,3.615,0,0,0,1.06-2.555Zm-5.973,0a2.36,2.36,0,1,1,.691,1.667,2.356,2.356,0,0,1-.691-1.667Z" transform="translate(7866.96 -9167.15)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1439" d="M447.232,378.03h-9.509a2.793,2.793,0,0,0-2.79,2.79v8.658a.627.627,0,0,0,.628.628h13.832a.628.628,0,0,0,.629-.628V380.82a2.793,2.793,0,0,0-2.79-2.79Zm1.532,10.819h-1.991v-5.58a.629.629,0,1,0-1.257,0v5.58h-6.077v-5.58a.629.629,0,1,0-1.257,0v5.58h-1.991V380.82a1.534,1.534,0,0,1,1.532-1.532h9.509a1.534,1.534,0,0,1,1.533,1.532Z" transform="translate(7762.978 -9230.942)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" id="Path_1440" d="M473.545,313.4a3.619,3.619,0,1,0-2.555-1.06,3.615,3.615,0,0,0,2.555,1.06Zm0-5.973a2.361,2.361,0,1,1-1.667.692,2.358,2.358,0,0,1,1.667-.692Z" transform="translate(7731.91 -9167.15)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" d="M228.192,93.656V78.767a2.2,2.2,0,0,0-.354-4.365H197.5a2.2,2.2,0,0,0-.354,4.365V93.656a.629.629,0,0,0,.629.628h14.264v2.392a.629.629,0,0,0,1.258,0V94.284h14.264a.629.629,0,0,0,.629-.628ZM197.5,77.545a.943.943,0,0,1,0-1.886h30.335a.943.943,0,0,1,0,1.886Zm29.431,15.482H198.407V78.8h28.528Z" transform="translate(7975.706 -8961.402)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                    <path class="fill-white stroke-white" d="M261.088,147.667a.624.624,0,0,0,.387-.133l4.591-3.587,2.436,2.436h0a.629.629,0,0,0,.854.032l4.273-3.662,1.977,2.477a.629.629,0,0,0,.437.234.621.621,0,0,0,.471-.155l4.532-4.008a.629.629,0,0,0-.833-.942l-4.036,3.57-1.971-2.469a.627.627,0,0,0-.432-.233.617.617,0,0,0-.468.149l-4.325,3.707-2.417-2.417h0a.628.628,0,0,0-.831-.051l-5.03,3.929a.629.629,0,0,0,.387,1.124Z" transform="translate(7917.864 -9019.814)" fill="#4f7eba" stroke="#4f7eba" stroke-width="0.25" />
                                </g>
                            </g>
                        </svg>

                        <h5 class="shadow-card-header">Leadership of Business Ethics Framework</h5>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="ethics-survey.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="56" height="56" viewBox="0 0 56 56">
                            <g id="Group_4102" transform="translate(-8363 9208)">
                                <rect width="56" height="56" transform="translate(8363 -9208)" fill="#fff" opacity="0.002" />
                                <g>
                                    <g class="fill-blue" transform="translate(8374 -9199)" fill="#fff" stroke="#4f7eba" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25">
                                        <rect width="33" height="40" stroke="none" />
                                        <rect x="0.625" y="0.625" width="31.75" height="38.75" fill="none" />
                                    </g>
                                    <g transform="translate(8377 -9197)" fill="#fff" stroke="#4f7eba" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25">
                                        <rect x="0.625" y="0.625" width="25.75" height="32.75" fill="none" />
                                    </g>
                                    <g transform="translate(7104 -9096)" fill="#fff">
                                        <path class="fill-blue" d="M 1294.000244140625 -99.62500762939453 L 1278 -99.62500762939453 C 1277.793334960938 -99.62500762939453 1277.625122070312 -99.79318237304688 1277.625122070312 -99.99990081787109 L 1277.625122070312 -105.0003051757812 C 1277.625122070312 -105.2070236206055 1277.793334960938 -105.3752059936523 1278 -105.3752059936523 L 1281.157348632812 -105.3752059936523 L 1281.61279296875 -105.3752059936523 L 1281.752319335938 -105.808723449707 C 1281.978759765625 -106.5122146606445 1282.509155273438 -107.1546630859375 1283.245971679688 -107.6177444458008 C 1284.034057617188 -108.1130828857422 1284.986450195312 -108.3749008178711 1286.000122070312 -108.3749008178711 C 1287.013793945312 -108.3749008178711 1287.966064453125 -108.1130905151367 1288.75390625 -107.6177749633789 C 1289.490478515625 -107.1547470092773 1290.020751953125 -106.5122909545898 1290.2470703125 -105.8087921142578 L 1290.386596679688 -105.3752059936523 L 1290.842041015625 -105.3752059936523 L 1294.000244140625 -105.3752059936523 C 1294.206909179688 -105.3752059936523 1294.375122070312 -105.2070236206055 1294.375122070312 -105.0003051757812 L 1294.375122070312 -99.99990081787109 C 1294.375122070312 -99.79318237304688 1294.206909179688 -99.62500762939453 1294.000244140625 -99.62500762939453 Z" stroke="none" />
                                        <path class="fill-white"  d="M 1293.750122070312 -100.2500076293945 L 1293.750122070312 -104.7502059936523 L 1289.93115234375 -104.7502059936523 L 1289.652099609375 -105.6173858642578 C 1289.254516601562 -106.8530578613281 1287.71875 -107.7499008178711 1286.000122070312 -107.7499008178711 C 1284.28125 -107.7499008178711 1282.744995117188 -106.8529815673828 1282.347290039062 -105.6172332763672 L 1282.068237304688 -104.7502059936523 L 1278.250122070312 -104.7502059936523 L 1278.250122070312 -100.2500076293945 L 1293.750122070312 -100.2500076293945 M 1294.000244140625 -99.00000762939453 L 1278 -99.00000762939453 C 1277.447387695312 -99.00000762939453 1277.000122070312 -99.44730377197266 1277.000122070312 -99.99990081787109 L 1277.000122070312 -105.0003051757812 C 1277.000122070312 -105.552001953125 1277.447387695312 -106.0002059936523 1278 -106.0002059936523 L 1281.157348632812 -106.0002059936523 C 1281.712524414062 -107.7251205444336 1283.670532226562 -108.9999008178711 1286.000122070312 -108.9999008178711 C 1288.329711914062 -108.9999008178711 1290.287109375 -107.7251205444336 1290.842041015625 -106.0002059936523 L 1294.000244140625 -106.0002059936523 C 1294.551879882812 -106.0002059936523 1295.000122070312 -105.552001953125 1295.000122070312 -105.0003051757812 L 1295.000122070312 -99.99990081787109 C 1295.000122070312 -99.44730377197266 1294.551879882812 -99.00000762939453 1294.000244140625 -99.00000762939453 Z" stroke="none" fill="#4f7eba" />
                                    </g>
                                    <g transform="translate(104.084 -207.375)">
                                        <g transform="translate(8282.555 -8972.626)"   fill="none" stroke="#4f7eba" stroke-width="1.25">
                                            <circle class="stroke-white" cx="5.298" cy="5.298" r="5.298" stroke="none" />
                                            <circle class="stroke-white" cx="5.298" cy="5.298" r="4.673" fill="none" />
                                        </g>
                                        <line class="stroke-white" x2="3.532" y2="3.532" transform="translate(8291.385 -8963.796)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.25" />
                                    </g>
                                    <line class="stroke-white" x2="7" transform="translate(8381.5 -9190.5)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.25" />
                                    <line class="stroke-white" x2="16" transform="translate(8381.5 -9186.5)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.25" />
                                    <line class="stroke-white" x2="16" transform="translate(8381.5 -9182.5)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.25" />
                                </g>
                            </g>
                        </svg>

                        <h5 class="shadow-card-header">Ethics Survey</h5>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="training-and-capability-building.php" class="shadow-card has-forward-icon ltc-purpose-card border-0">
                    <div class="shadow-card-content">
                        <svg id="itc-training" xmlns="http://www.w3.org/2000/svg" width="57.062" height="56" viewBox="0 0 57.062 56">
                            <g transform="translate(-12206.887 1299.729) rotate(42)" style="isolation: isolate">
                                <path class="stroke-white" d="M6.1,28.5H4A3.5,3.5,0,0,1,.5,25V4A3.5,3.5,0,0,1,4,.5H37A3.5,3.5,0,0,1,40.5,4V25A3.5,3.5,0,0,1,37,28.5H15.984L6.1,34.919Z" transform="translate(8213.77 -9124.104) rotate(-42)" fill="none" stroke="#4f7eba" stroke-width="1.5" />
                                <line class="stroke-white" x2="27" transform="translate(8233.729 -9113.146) rotate(-42)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.5" />
                                <line class="stroke-white" x2="16" transform="translate(8229.046 -9118.348) rotate(-42)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.5" />
                                <line class="stroke-white" x2="19" transform="translate(8224.361 -9123.55) rotate(-42)" fill="none" stroke="#4f7eba" stroke-linecap="round" stroke-width="1.5" />
                                <path class="stroke-white fill-blue" d="M8227.022-9136.261V-9159.3h-6.5v23.041l3.224,5.778Z" transform="translate(23.168)" fill="#fff" stroke="#4f7eba" stroke-width="1.5" />
                                <path class="fill-blue" d="M8238.212-9159.3c0-.008-6.445,0-6.445,0s-.641-4.349,3.2-4.5C8235.094-9163.773,8238.682-9164.027,8238.212-9159.3Z" transform="translate(11.962)" fill="#fff" stroke="#4f7eba" stroke-width="1.5" />
                                <path class="fill-blue" d="M8243.688-9136.3h6.264" fill="#fff" stroke="#4f7eba" stroke-width="1.5" />
                            </g>
                        </svg>

                        <h5 class="shadow-card-header">Training and Capability Building</h5>
                    </div>
                </a>
            </div>


        </div>
    </div>
</section>

<?php include "components/footer.php" ?>