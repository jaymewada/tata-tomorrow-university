<?php include "components/header.php" ?>
<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php" class="active">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content white-element">
    <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element">
    <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-5 mx-auto">
                    <header class="section-header section-header-sm text-light">Ethics Survey</header>
                    <p>The Ethics Survey termed as Leadership of Business Ethics (LBE) Assurance Survey started more than a decade back to support Tata companies to objectively measure their ‘Ethics-environment’ thereby providing useful insights and benchmark for continuous improvement of their business ethics practices.</p>
                    <p>This biennial independent perception-based survey undertaken by the Tata companies covers employees, associates and value chain partners across geographies and business functions. The measurement of the ethical culture and efficacy of initiatives undertaken within the LBE framework are done based on voluntary and anonymous online survey covering following five perspectives.</p>
                    <br>
                </div>

                <div class="col-md-10 col-lg-7 mx-auto">
                    <div class="ethics-survey-wrapper">
                        <ul class="blue-dot-list has-connecting-line">
                            <li>
                                <h5 class="dot-list-header">Leadership Engagement</h5>
                                <p>‘Tone at the Top’ and action plan set by the leadership to promote ethical business environment for all stakeholders.</p>
                            </li>

                            <li>
                                <h5 class="dot-list-header">Environment for Ethics</h5>
                                <p>Enabling environment that promotes the Tata values as integral part of business conduct.</p>
                            </li>
                            <li>
                                <h5 class="dot-list-header">Awareness and Training</h5>
                                <p>Communication and training initiatives providing clear guidelines to the stakeholders for taking the right decisions</p>
                            </li>
                            <li>
                                <h5 class="dot-list-header">Ethics Counselor’s Role</h5>
                                <p>Mechanism available for stakeholders to seek guidance for matters related to ethics.</p>
                            </li>
                            <li>
                                <h5 class="dot-list-header">Systems and Processes</h5>
                                <p>Systems and processes put in place to promote and enable the policies and controls related to ethics.</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>