<?php include "components/header.php" ?>
<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php" class="active">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content">
        <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element">
        <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">
        <div class="container">
            <div class="row position-relative" style="z-index:2;">
                <div class="col-md-8 col-lg-7 text-center mx-auto mb-3 mb-md-5">
                    <header class="section-header section-header-sm text-light mb-2">Training & Capability Building</header>
                    <p class="fw-300 mb-0">Tata has always been a values-driven organisation. These values continue to direct the growth and business of Tata companies.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Tata Code of Conduct e-learn</header>
                            <p>Common e-learning module for the Tata Code of Conduct is available to Tata companies. This enables consistent understanding across the Tata employees.</p>
                            <span>Click to Confirm</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Anti Bribery and Anti Corruption e-learn</header>
                            <p>One of the core principles set out in the Tata Code of Conduct states: ‘We are committed to operating our businesses conforming to the highest moral and ethical standards. We do not tolerate bribery or corruption in any form. This commitment underpins everything we do.’</p>
                            <p>The e-Learn reinforces the principle and equips individuals to prevent and mitigate bribery or corruption risks at their organisation.</p>
                            <span>Click to Confirm</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-community-training">
    <div class="container">
        <div class="row">
            <div class="col-md-6 align-self-center mb-4 mb-md-0">
                <header class="section-header mb-3">Ethics Community <br> Training Program</header>
                <img src="img/backgrounds/ltc-ect-element.svg" class="community-training-element" alt="">
            </div>
            <div class="col-md-6 mb-30">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header">Ethics in Action</header>
                        <p>The ethics case study competition organized by Group Ethics Office, Tata Sons Private Limited with the objective to stimulate discussion and application of Tata Values and Principles in context of practical business environment. The event provides an opportunity to Ethics Community members of the Tata Group to showcase their capability of resolving real life ethical issues in business environment and to network with peer group.</p>
                        <span>Know More</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header">Ethics Masterclass</header>
                        <p>Enabling ‘Ethics Community’ members to confidently uphold, disseminate, and guide other stakeholders in the “Right Way to conduct a Tata business” is very important for Tata Brand Equity. Ethics Practitioners are called upon to build this differentiating capability across our group’s businesses.</p>
                        <span>Know More</span>
                    </div>
                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header">Workplace Investigation Masterclass</header>
                        <p>This Masterclass provides understanding of the process for conducting workplace investigation, basic tenets, tools and techniques of the investigation process, and to build the required acumen.</p>
                        <span>Know More</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section-awareness-kit">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-5">
                <div class="awareness-kit-element-wrapper">
                    <header class="section-header text-light">Communication and Awareness Kit</header>
                    <img src="img/backgrounds/ltc-cak-element.svg" class="awareness-kit-element" data-aos="fade-in">
                </div>
            </div>
            <div class="col-md-6">
                <a href="" class="shadow-link-card h-100">
                    <div class="shadow-link-content">
                        <header class="shadow-link-card-header">Communication Templates</header>
                        <p>Communication Kit developed to drive awareness of the Tata Code of Conduct though online and offline communication channels. The messages in this kit ‘shine a light’ on broad aspects of the Code and provide an impetus to the objective of having Tata employees ‘Live the Code’!</p>
                        <span>Know More</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>