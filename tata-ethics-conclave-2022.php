<?php include "components/header.php" ?>
<br class="d-md-none">
<header class="section-header global-header-padding text-center m-0">Tata Ethics Conclave</header>


<ul class="page-navigation-list my-3">
    <li>
        <a href="tata-ethics-conclave-2018">2018</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2021">2021</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2022">
            <strong>2022</strong>
        </a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2023">2023</a>
    </li>
</ul>

<section class="section-ethics-conclave">
<img src="img/backgrounds/blue-d.svg" class="tec-d-1">
<img src="img/backgrounds/bars-white.svg" class="tec-bars" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center align-self-center mb-4">
                <img src="img/backgrounds/ethics-conclave.png" class="w-75 mx-auto" alt="">
            </div>
            <div class="col-lg-6 align-self-center">
                <p>The Tata Ethics Conclave 2023 was held on 14th February 2023 at Taj President, Mumbai and the theme was ‘Ethics in the era of emerging technologies and digitalization’. The conclave was addressed by Tata Group Chairman, Tata Group Chief Ethics Officer and notable speakers from Tata Group, Industry Leaders, and Thought Leaders.
                    Recognitions were bestowed to 12 Ethics Community members for their contributions towards driving Leadership of Business Ethics framework along with the winning and 3 finalist teams of the 2nd edition of Ethics in Action Case Study Competition. The conclave was attended in-person by senior Tata leaders and 160+ Tata Group Ethics Community members, additionally 250+ participants joined virtually.</p>
            </div>
        </div>
    </div>
</section>


<section class="section-graphical-scribe">
<img src="img/backgrounds/ethics-conclave-bars.svg" class="gs-bars" data-aos="fade-in">
    <div class="container-sm">
        <header class="section-header text-center">Graphical scribe and <br> recording of the sessions</header>

        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session1.png" class="img-fluid" alt="">
                    </a>
                    <strong>Group Chairman’s Address - N. Chandrasekaran, Chairman, Tata Sons Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session2.png" class="img-fluid" alt="">
                    </a>
                    <strong>S. Padmanabhan, Group Chief Ethics Officer, Tata Sons Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session3.png" class="img-fluid" alt="">
                    </a>
                    <strong>Digital trust driving customer choices - Aarthi Subramanian, Group Chief Digital Officer, Tata Sons Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session4.png" class="img-fluid" alt="">
                    </a>
                    <strong>Responsible partnership key for value chain trust - Thomas Flack, President & Chief Purchasing Officer, Tata Motors Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session5.png" class="img-fluid" alt="">
                    </a>
                    <strong>Responsible partnership key for value chain trust - Thomas Flack, President & Chief Purchasing Officer, Tata Motors Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-30">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/session6.png" class="img-fluid" alt="">
                    </a>

                    <strong>Panel Discussion Session Opportunities and challenges to bring in positive impact on external stakeholder trust
                        - Arvind Goel, Managing Director & Chief Executive Officer, Tata AutoComp Systems Limited
                        - Deepika Rao, Managing Director & Chief Executive Officer, Roots Corporation Limited
                        - Koushik Chatterjee, Executive Director & Chief Financial Officer, Tata Steel Limited</strong>

                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>