<?php include "components/header.php" ?>

<div class="section-tbeg-tabs">
    <header class="section-header text-center">Tata Business Excellence Group</header>

    <nav class="tbeg-pages-navigation" style="grid-template-columns:auto auto auto auto auto;">
        <div>
            <a href="business-excellence.php" class="active">Business Excellence</a>
        </div>
        <div>
            <a href="cyber-excellence.php">Cyber Excellence</a>
        </div>

        <div>
            <a href="data-excellence.php">Data Excellence</a>
        </div>
        <div>
            <a href="safety-excellence.php">Safety Excellence</a>
        </div>
        <div>
            <a href="social-excellence.php">Social Excellence</a>
        </div>
    </nav>

    <div class="tbeg-tabs-content-wrapper">
        <img src="img/backgrounds/bars-white-sm.svg" class="tbeg-self-paced-element" data-aos="fade-in">
        <div class="container">
            <header class="section-header section-header-sm">Self-Paced Modules</header>
            <div class="row mx-auto justify-content-center" style="max-width:675px;">
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Overview & History</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>The Tata Group’s Journey of Excellence</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>


                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Foundation of World-Class Organization</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>TBEM Reference Manual - The Bluebook</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-tbeg-classroom">
    <img src="img/backgrounds/tbeg-classroom-element-1.svg" class="tbeg-classroom-element-1" data-aos="fade-in">
    <img src="img/backgrounds/tbeg-classroom-element-2.svg" class="tbeg-classroom-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header section-header-sm">Classroom Programmes</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Business Excellence Assessor Programme</h6>
                    <p>Enable high-performing Tata executives become Business Excellence Assessors, and thereby sharpen their leadership competencies. Open to all executives and managers with at least 8 years of work experience (5 years for TAS managers).</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Subject Matter Expert Assessor Programme</h6>
                    <p>Enable senior functional leaders, with domain expertise in areas such as Sales & Marketing, HR, Finance, Legal & Secretarial, Project Management, Operations, Strategy, Data & Analytics and so on to become Business Excellence Assessors. Subject Matter Experts with at least 15 years of functional experience are eligible for this programme.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Advanced Programme for Leaders</h6>
                    <p>To review and sharpen assessment process by incorporating learnings and new ideas, thereby co-create an agenda for excellence in assessment. Participation by team Leaders and potential leaders, by invitation.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Experienced Assessor Programme</h6>
                    <p>Enable current Assessors with Business Excellence external assessment experience, get acquainted with the changes in assessment process and new focus areas. Invitation to this programme is based on Assessors’ past performance and experience.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Business Excellence Champion Programme</h6>
                    <p>Designed for senior executives and managers, the programme serves as a stepping stone to generate significant effort and thrust for improvement in an Organisation and live the TBEM way. Each programme is customised to suit the company and the participants’ needs.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Embracing Business Excellence </h6>
                    <p>Enable senior executives and managers to appreciate the fundamentals of Business Excellence, celebrate ‘Tataness’ & Excellence as a value.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-programme">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-assesor-programme.svg" height="70" alt="">
                    <div>
                        <strong>Assesor Programme</strong>
                        <p>Enables participants to become a Certified Assessor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-champion-programme.svg" height="70" alt="">
                    <div>
                        <strong>Champion Programme</strong>
                        <p>Enables understanding and application of the model in own organisation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-contact" id="contact-us">
    <img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <strong class="text-navy-2 fw-500">Madhu Chaithanya</strong>
                        <div class="text-muted my-10">mchaithanya@tata.com</div>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>