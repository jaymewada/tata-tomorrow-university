<?php include "components/header.php" ?>

<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php"  class="active">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content">
        <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element">
        <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">
        <div class="container">
            <div class="row position-relative" style="z-index:2;">
                <div class="col-md-8 col-lg-8 text-center mx-auto mb-3 mb-md-5">
                    <header class="section-header section-header-sm text-light mb-2">Best Practices Sharing</header>
                    <p class="fw-300 mb-0">Tata has always been a values-driven organisation. These values continue to direct the growth and business of Tata companies.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Promising Practices</header>
                            <p>Sharing and learning of promising practices amongst the group companies and by subject matter experts is key to enable journey of continuous improvements. The EDGE portal has become the knowledge repository of the Tata Group for varied subjects including business ethics.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Tata Ethics Conclave</header>
                            <p>The Tata Ethics Conclave is a flagship event organised by the Group Ethics Office, Tata Sons Private Limited, to bring together Ethics Practitioners from across group companies to share challenges, learnings and evolving trends in the area of business ethics.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section>

<?php include "components/footer.php" ?>