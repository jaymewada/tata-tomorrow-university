<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	 <img src="img/banners/our-journey-banner.png">
</section>

<section class="section-page-nav">
    <div class="container">
        <div class="about-page-navigation-list">
            <ul class="page-navigation-list m-0">
                <li>
                    <a href="about">Director’s Note</a>
                </li>
                <li class="is-active">
                    <a href="our-journey">Our Journey</a>
                </li>
                <li>
                    <a href="our-world">Our World</a>
                </li>
                <li>
                    <a href="our-footprints">Our Footprints</a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="section-about-intro">
    <img src="img/backgrounds/gold-d-left.svg" width="40" class="about-intro-element-1 d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header m-0">Introduction</header>
                <img src="img/backgrounds/bars-gold.svg" class="my-4" data-aos="fade-in">
            </div>
            <div class="col-md-8">
                <div class="global-content-width-600 ml-0">
                    <p>The Tata Management Training Centre (TMTC) was established in 1959 to nurture business skills and instil Tata values in executives across levels. Today, it is a highly regarded leadership development and executive education institution that enables cross-sector, cross-organisation, cross-industry learning. We offer programmes covering a range of learning disciplines and partner our companies in their learning journeys, in collaboration with some of the most renowned global learning institutions and subject matter experts.</p>

                    <p>At TMTC, we believe that opportunities for learning and growth should be available to all. With the aim to democratise learning and reach Tata employees around the world, in 2020 we established our digital arm and e-learning platform, Tata Tomorrow University. Along with information on the flagship, open and custom programmes offered by TMTC, this web portal houses self-paced courses, workshops, podcasts, webinars and many other learning and development modules accessible to all Tata employees, across all levels</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-purpose">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header m-0 text-light">Our Purpose</header>
                <img src="img/backgrounds/bars-white-sm.svg" class="my-4" data-aos="fade-in">
            </div>
            <div class="col-md-8">
            <div class="global-content-width-550 ml-0">
                <ul class="dot-list white-dots text-light">
                    <li>
                        <p>JRD Tata established TMTC to foster learning among high-performing executives in the group, and to create a sense of fellowship among employees across our companies.</p>
                    </li>
                    <li>
                        <p>Over the years, TMTC has helped many Tata leaders achieve their business goals and fulfil their potential with its programmes in leadership, management, and business acumen.</p>
                    </li>
                    <li>
                        <p>It has held true to its purpose and served as forum for them to meet leaders from other group companies, exchange ideas and learn from one another.</p>
                    </li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="section-about-values">
    <img src="img/backgrounds/gold-d-left.svg" width="40" class="av-element-1 d-none d-md-block">
    <img src="img/backgrounds/trending-now-slider-element-2.svg" class="av-element-2" data-aos="fade-up">

    <div class="container">
        <header class="section-header">Our Values</header>
        <div class="row">
            <div class="col-md-6">
                <ul class="dash-list global-content-width-500">
                    <li>
                        <p>The Tata group has always been a values-driven organisation. We invest in our people, partners and the communities we operate in. The values of the group foster continuous learning and help build caring and collaborative relationships based on trust and mutual respect.</p>
                    </li>

                    <li>
                        <p>TMTC works towards upholding this legacy by helping professionals in the group become future-ready.</p>
                    </li>
                </ul>
            </div>

            <div class="col-md-6">
                <ul class="dash-list global-content-width-500">
                    <li>
                        <p>We are dedicated to holistic capability-building and believe that continuous learning is essential for achieving success in dynamic business environments. We extend this approach while designing our programmes as well. They evolve basis global benchmarking, participant feedback and findings of effectiveness surveys.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section> -->

<section class="section-tmtc-edge">
    <img src="img/backgrounds/tmtc-edge-element-2.svg" class="edge-element-1" data-aos="fade-up">
    <img src="img/backgrounds/gold-rod.svg" class="edge-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header">The TMTC Edge</header>
        <div class="row">
            <div class="col-md-6">
                <ul class="dash-list global-content-width-500">
                    <li>
                        <p>Learning is a journey, and there are no shortcuts to change. At TMTC, we take the time to understand the context of our learners before designing any capability-building solution</p>
                    </li>
                    <li>
                        <p>We believe in nurturing and encouraging thought leadership. This drives our focus on emergent business themes and future-ready competencies that are poised to become vital to business sustainability.</p>
                    </li>
                    <li>
                        <p>We engage deeply with the senior leadership of our companies to ensure our programmes remain contemporary, relevant and aligned with the group’s priorities and focus.</p>
                    </li>
                    <li>
                        <p>We work exclusively with global, best-in-class delivery partners, collaborating with renowned faculty, institutions and subject matter experts from around the world. As a result, we offer cutting-edge, innovative, and outcome-driven programmes.</p>
                    </li>
                </ul>
            </div>

            <div class="col-md-6">
                <ul class="dash-list global-content-width-500">
                    <li>
                        <p>We use a range of learning methodologies — classroom sessions, action learning projects, experiential learning and immersive experiences — to make our programmes engaging and effective. To make our programmes accessible to the widest audience possible, we deploy multi-modal learning, including in-person, hybrid and online.</p>
                    </li>
                    <li>
                        <p>We enable “outside-in” learning and cross-pollination through peer interactions across a wide spectrum of industries and businesses.</p>
                    </li>
                    <li>
                        <p>Through all our offerings, we nurture the “Tata Way” of leadership to ensure that the actions of our leaders and the way we do business build on the Tata legacy and vision.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="section-history">
    <div class="our-history-banner">
        <!-- <img src="img/backgrounds/our-history-bg.png"> -->
        <div class="container">
            <header class="section-header mb-4 text-light">Our History</header>
            <img src="img/backgrounds/bars-white-sm.svg" data-aos="fade-in">
        </div>
    </div>
    
    <div class="our-history-slider dots-dashed">
        <div>
            <div class="our-history-slide">
                <h6>1959</h6>
                <span>&bull;</span>
                <p>JRD Tata established TMTC to nurture the leaders of tomorrow and instil the values of the Tata group.</p>
            </div>
        </div>
        <div>
            <div class="our-history-slide">
                <h6>1966</h6>
                <span>&bull;</span>
                <p>The TMTC campus at Pune was inaugurated, centred around an elegant heritage bungalow, designed by renowned architect George Wittet.</p>
            </div>
        </div>
        <div>
            <div class="our-history-slide">
                <h6>1992</h6>
                <span>&bull;</span>
                <p>On the Silver Jubilee of TMTC, JRD Tata announced a Chair in ‘International Marketing and Indo EEC Trade’, and unveiled the first issue of the ‘TMTC Journal of Management’.</p>
            </div>
        </div>
        <div>
            <div class="our-history-slide">
                <h6>2015</h6>
                <span>&bull;</span>
                <p>The Centre became an exclusive resource for Tata group companies.</p>
            </div>
        </div>
        <div>
            <div class="our-history-slide">
                <h6>2020</h6>
                <span>&bull;</span>
                <p>Tata Tomorrow University was launched to make learning solutions accessible to all Tata group employees.</p>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>