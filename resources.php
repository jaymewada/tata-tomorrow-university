<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/resources-banner.png" alt="" />
</section>

<section class="section-page-nav">
	<div class="container">
		<ul class="page-navigation-list bluemint">
			<li>
				<a class="hash-link" href="#research">Research</a>
			</li>
			<li>
				<a class="hash-link" href="#newsletter">Newsletter</a>
			</li>
			<li>
				<a class="hash-link" href="#blog">Blog</a>
			</li>
		</ul>
	</div>
</section>

<section class="section-research" id="research">
	<img src="img/backgrounds/gold-d-right.svg" class="research-element-1" alt="">
	<div class="container ">
		<header class="section-header mb-4 text-center">Research</header>
		<div class="global-content-width-650 text-center">
			<p>
				At TMTC, continuous learning is a way of life. We partner with subject matter experts, thought leaders and
				academicians from around the world to research and develop ideas and cases that can benefit our leaders and
				colleagues.
			</p>

			<p>
				<i class="fw-500">MTC Research Insights - Applied Research @ TMTC</i> is our in-depth bulletin that
				shares insights and updates about the research being conducted by Tata group members, in partnership with global
				experts, in fields such as management, best practices, leader-follower dynamics, sustainability, and the like.
			</p>
		</div>

		<div class="research-card-wrapper">
			<div class="row">
				<div class="col-6 col-md-3 mb-30">
					<div class="research-card">
						<div class="research-card-image">
							<img src="img/cards/research-card-1.png" class="img-fluid" alt="">
						</div>
						<h6>Research Insights</h6>
						<span class="research-card-time">Issue 02: November 2022</span>
						<a href="" class="research-card-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
								<g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(20.133 -8.098) rotate(90)">
									<path id="Path_27" data-name="Path 27" d="M7.5,18H18.35" transform="translate(1.598 -4.89)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_37" data-name="Path 37" d="M7.5,18H18.35" transform="translate(42.523 0.184) rotate(90)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_28" data-name="Path 28" d="M18,7.5l5.61,5.61L18,18.72" transform="translate(-2.085 0)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
								</g>
							</svg> &nbsp;Download
						</a>
					</div>
				</div>

				<div class="col-6 col-md-3 mb-30">
					<div class="research-card">
						<div class="research-card-image">
							<img src="img/cards/research-card-1.png" class="img-fluid" alt="">
						</div>
						<h6>Research Insights</h6>
						<span class="research-card-time">Issue 02: November 2022</span>
						<a href="" class="research-card-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
								<g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(20.133 -8.098) rotate(90)">
									<path id="Path_27" data-name="Path 27" d="M7.5,18H18.35" transform="translate(1.598 -4.89)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_37" data-name="Path 37" d="M7.5,18H18.35" transform="translate(42.523 0.184) rotate(90)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_28" data-name="Path 28" d="M18,7.5l5.61,5.61L18,18.72" transform="translate(-2.085 0)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
								</g>
							</svg> &nbsp;Download
						</a>
					</div>
				</div>

				<div class="col-6 col-md-3 mb-30">
					<div class="research-card">
						<div class="research-card-image">
							<img src="img/cards/research-card-2.png" class="img-fluid" alt="">
						</div>
						<h6>Research Insights</h6>
						<span class="research-card-time">Issue 02: May 2022</span>
						<a href="" class="research-card-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
								<g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(20.133 -8.098) rotate(90)">
									<path id="Path_27" data-name="Path 27" d="M7.5,18H18.35" transform="translate(1.598 -4.89)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_37" data-name="Path 37" d="M7.5,18H18.35" transform="translate(42.523 0.184) rotate(90)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_28" data-name="Path 28" d="M18,7.5l5.61,5.61L18,18.72" transform="translate(-2.085 0)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
								</g>
							</svg> &nbsp;Download
						</a>
					</div>
				</div>

				<div class="col-6 col-md-3 mb-30">
					<div class="research-card">
						<div class="research-card-image">
							<img src="img/cards/research-card-2.png" class="img-fluid" alt="">
						</div>
						<h6>Research Insights</h6>
						<span class="research-card-time">Issue 02: May 2022</span>
						<a href="" class="research-card-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
								<g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(20.133 -8.098) rotate(90)">
									<path id="Path_27" data-name="Path 27" d="M7.5,18H18.35" transform="translate(1.598 -4.89)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_37" data-name="Path 37" d="M7.5,18H18.35" transform="translate(42.523 0.184) rotate(90)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									<path id="Path_28" data-name="Path 28" d="M18,7.5l5.61,5.61L18,18.72" transform="translate(-2.085 0)" fill="none" stroke="#00064d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
								</g>
							</svg> &nbsp;Download
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<section class="section-newsletter" id="newsletter">
	<img src="img/backgrounds/gold-d-left.svg" class="newsletter-element-1 d-none d-md-block">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-lg-4">
				<header class="section-header m-0">Newsletter</header>
				<img src="img/backgrounds/bars-gold.svg" class="my-4" data-aos="fade-in" />
			</div>
			<div class="col-md-9 col-lg-8">
				<p class="global-content-width-600 ml-0 mb-4">
					<i class="fw-500">The Learning Post</i> is our quarterly publication designed to spark thought and a sense of belonging. The
					newsletter keeps all group members updated about recent and upcoming activities at TMTC, while sharing
					relevant global developments and ideas, and current conversations.
				</p>

				<div class="row">
					<div class="col-md-6 mb-30">
						<a href="#" class="recent-cs-card mw-100">
							<div class="recent-cs-card-img">
								<img src="img/backgrounds/newsletter-card.png" class="img-fluid" />
							</div>
							<div class="recent-cs-card-content p-3">
								<h6>The Learning Post</h6>
								<small class="text-dark">April - July 2022</small>
							</div>
							<span class="recent-cs-card-link">
								<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
									<g transform="translate(12.634 1) rotate(90)">
										<path d="M0,0H10.85" transform="translate(0 5.61)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
										<path d="M0,0H10.85" transform="translate(15.425 0.185) rotate(90)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
										<path d="M0,0,5.61,5.61,0,11.22" transform="translate(6.817)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									</g>
								</svg>
								&nbsp;Download</span>
						</a>
					</div>
					<div class="col-md-6 mb-30">
						<a href="#" class="recent-cs-card mw-100">
							<div class="recent-cs-card-img">
								<img src="img/backgrounds/newsletter-card-2.png" class="img-fluid" />
							</div>
							<div class="recent-cs-card-content p-3">
								<h6>The Learning Post</h6>
								<small class="text-dark">April - July 2022</small>
							</div>
							<span class="recent-cs-card-link">
								<svg xmlns="http://www.w3.org/2000/svg" width="14.048" height="17.425" viewBox="0 0 14.048 17.425">
									<g transform="translate(12.634 1) rotate(90)">
										<path d="M0,0H10.85" transform="translate(0 5.61)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
										<path d="M0,0H10.85" transform="translate(15.425 0.185) rotate(90)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
										<path d="M0,0,5.61,5.61,0,11.22" transform="translate(6.817)" fill="none" stroke="#bfaa65" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
									</g>
								</svg>
								&nbsp;Download</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-blog" id="blog">
	<img src="img/backgrounds/white-d.svg" class="blog-element-1">
	<img src="img/backgrounds/white-rod.svg" class="blog-element-2" data-aos="fade-in">
	<div class="container">
		<header class="section-header text-center text-light">Blog</header>
		<div class="row justify-content-center">
			<div class="col-md-6 col-lg-4 mb-30">
				<a href="#modal-card-1" class="recent-cs-card mw-100" data-fancybox="gallery">
					<div class="recent-cs-card-img">
						<img src="img/backgrounds/blog-card-banner.png" class="img-fluid" />
					</div>
					<div class="recent-blogs-card-content">
						<span class="blog-card-time">26 Dec 2022</span>
						<h6>Driving Innovation and Social Impact</h6>
						<p class="text-dark">The Tata Group Corporation, founded in 1868 by Jamsetji Nusserwanji Tata…</p>
					</div>
					<span class="recent-cs-card-link">Read More</span>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 mb-30">
				<a href="#modal-card-1" class="recent-cs-card mw-100" data-fancybox="gallery">
					<div class="recent-cs-card-img">
						<img src="img/backgrounds/blog-card-banner.png" class="img-fluid" />
					</div>
					<div class="recent-blogs-card-content">
						<span class="blog-card-time">26 Dec 2022</span>
						<h6>Driving Innovation and Social Impact</h6>
						<p class="text-dark">The Tata Group Corporation, founded in 1868 by Jamsetji Nusserwanji Tata…</p>
					</div>
					<span class="recent-cs-card-link">Read More</span>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 mb-30">
				<a href="#modal-card-1" class="recent-cs-card mw-100" data-fancybox="gallery">
					<div class="recent-cs-card-img">
						<img src="img/backgrounds/blog-card-banner.png" class="img-fluid" />
					</div>
					<div class="recent-blogs-card-content">
						<span class="blog-card-time">26 Dec 2022</span>
						<h6>Driving Innovation and Social Impact</h6>
						<p class="text-dark">The Tata Group Corporation, founded in 1868 by Jamsetji Nusserwanji Tata…</p>
					</div>
					<span class="recent-cs-card-link">Read More</span>
				</a>
			</div>
		</div>
	</div>
</section>

<div class="blog-modal" id="modal-card-1">
	<div class="blog-modal-img-wrapper">
		<img src="img/backgrounds/blog-card-banner.png" class="img-fluid" alt="">
	</div>

	<div class="blog-modal-content">
		<div class="blog-modal-info-wrapper">
			<span>26 Dec 2022</span>
			<header>Driving Innovation and Social Impact: The Journey of Tata Group Corporation</header>
			<span style="color:#00064D;">by Jane Doe</span> | <span>3 Min read</span>
		</div>
		<p>The Tata Group Corporation, founded in 1868 by Jamsetji Nusserwanji Tata, is one of the oldest and most respected business conglomerates in India. With its headquarters in Mumbai, Tata Group has a rich legacy of driving innovation and creating a positive social impact in various sectors such as automotive, steel, chemicals, hospitality, telecommunications, and more. In this blog article, we will delve into the journey of Tata Group Corporation, India, and explore how it has become a trailblazer in fostering innovation and social responsibility.</p>
		<p>At the heart of Tata Group’s success lies its commitment to innovation. The group has consistently fostered a culture of innovation across its diverse businesses, which has resulted in numerous breakthroughs and pioneering initiatives. Tata Group has a strong focus on research and development (R&D) and invests significantly in cutting-edge technologies and innovation-driven projects.</p>
		<p>Known for its pioneering work in the Indian automobile industry, Tata Motors has developed innovative and affordable vehicles that cater to the unique needs of the Indian market. The Tata Nano, dubbed as the “People’s Car,” was one such innovation, which aimed to make car ownership accessible to millions of Indians. Tata Motors has also been at the forefront of developing electric vehicles (EVs) and is committed to sustainability and environmental conservation.</p>
	</div>
</div>

<?php include "components/footer.php" ?>