<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/tata-executive-banner.png" class="d-none d-lg-block" alt="">
    <img src="img/banners/tata-executive-mobile-banner.png" class="d-block d-lg-none">
</section>

<div class="banner-shadow-content text-center">
    The Tata Group Executive Leadership Seminar is a transformative learning experience for business and functional leaders who have the ambition and desire to broaden their horizons and make a wider impact in their organisations. Developed and deployed in collaboration with Michigan Ross faculty and an executive coach, the seminar adopts a blended learning approach, with modules delivered online and at the Tata Management Training Centre (TMTC) campus.
</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list els">
            <li>
                <a class="hash-link" href="#objectives">Objectives</a>
            </li>
            <li>
                <a class="hash-link" href="#aim">Aim</a>
            </li>
            <li>
                <a class="hash-link" href="#learing-goal">Learning Goals</a>
            </li>
            <li>
                <a class="hash-link" href="#learner-profile">Learner Profile</a>
            </li>
            <li>
                <a class="hash-link" href="#structure">Structure</a>
            </li>
            <li>
                <a class="hash-link" href="#faculty">Faculty</a>
            </li>
            <li>
                <a class="hash-link" href="#highlights">Highlights</a>
            </li>
            <li>
                <a class="hash-link" href="#gallery">Gallery</a>
            </li>
            <li>
                <a class="hash-link" href="#testimonials">Testimonials</a>
            </li>
            <li>
                <a class="hash-link" href="#team">Team</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-exl-objective" id="objectives">

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header m-0">Objectives</header>
                <img src="img/backgrounds/bars-navy.svg" class="img-fluid my-4" data-aos="fade-in" width="175">

            </div>
            <div class="col-md-8">
                <p>With a focus on strengthening business acumen in core areas such as Finance, Strategic HR, Marketing, Digital Transformation, Sustainability and Strategy, the seminar is designed to accelerate the growth journey for senior leadership and help them expand their horizons to be able to fulfil a larger role within the organisation and the Tata group.</p>
                
                <p>The immersive classroom experience, including stimulating case studies and engagement with live business challenges, is crafted with the objective of giving participants a holistic understanding of the larger business context.</p>
                
                <p>Delving deep into the role participants can play in expanding, enhancing and energising their organisations and teams, the seminar is geared towards honing their business acumen through interactions with highly accomplished faculty and networking with peers also go a long way to</p>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-aims" id="aim">
<img src="img/backgrounds/exl-objective-bg.svg" width="350" class="exl-objective-element-1" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header mb-30">Aim</header>
            </div>
            <div class="col-md-9 col-lg-6">
                <ul class="hexagon-list">
                    <li>
                        <img src="img/icons/exl-aim1.svg" alt="">
                        <p>The seminar aims to augment the leadership experience of senior business leaders and enable them to make an impact on the growth of their organisations. </p>
                    </li>
                    <li>
                        <img src="img/icons/exl-aim2.svg" alt="">
                        <p>Participants will acquire an enhanced understanding to adapt, and leverage different business functions, while developing an integrated action plan for powerful business and corporate strategies. </p>
                    </li>
                    <li>
                        <img src="img/icons/exl-aim3.svg" alt="">
                        <p>The seminar design focuses on strengthening business acumen in core areas such as Finance, Strategic HR, Marketing, Digital Transformation, Sustainability and Strategy. </p>
                    </li>
                    <li>
                        <img src="img/icons/exl-aim4.svg" alt="">
                        <p>The seminar also aims to provide participants with strategic self-awareness that will enable them to become better leaders. </p>
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</section>

<section class="section-exl-outcomes" id="learing-goal">
    <div class="container">
        <header class="section-header text-light text-center text-md-left">Key Outcomes</header>
        <div class="row">
            <div class="col-md-6">
                <ul class="dash-list dash-list-white">
                    <li>
                        <p>Participants are encouraged to develop integrated action plans by formulating a personal development plan as well as a professional development plan for action post-completion.</p>
                    </li>
                    <li>
                        <p>Participants learn to adapt and leverage global and local best practices for organisational growth. </p>
                    </li>
                    <li>
                        <p>Participants build a strong network of peers from across Tata group companies that continues beyond the classroom. </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="dash-list dash-list-white">
                    <li>
                        <p>Participants experience real-world scenarios by taking up an Action Learning Project that deals with a live business issue relevant to the Tata group. They learn to apply learnings from the seminar to address the issue, and to demonstrate the business impact of their project to the group. </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-attend" id="learner-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <header class="section-header">Who should <br class="d-none d-lg-block"> attend?</header>
                <img src="img/backgrounds/bars-navy.svg" class="img-fluid mb-4" data-aos="fade-in">
            </div>
            <div class="col-md-8">
                <div class="global-content-width-650">
                    <p>This immersive multi-week seminar is designed for senior-level executives who are within two reporting levels of the C-suite and have been identified as key talent within their organisation.</p>
                    <p>The seminar will benefit leaders who report to functional heads, or to Business Unit Heads (or those with potential for such roles).</p>
                    <p>This seminar would help senior functional or technical managers make the transition to a general management role. </p>
                    <div>Note:</div>
                    <p>Participation in this seminar is by invitation only. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-pedagogy" id="structure">
    <div class="container">
        <header class="section-header text-center">Structure</header>
        <div class="row">
            <div class="col-md-6 mb-40">
                <div class="shadow-card full-height mb-0 disable-hover-effect">
                    <div class="shadow-card-content">
                        <div class="shadow-card-header">Methodology</div>
                        <p>Interactive faculty-led sessions, case analyses, psychometric-based coaching, and project work on live business issues. Participants will also have the opportunity to collaborate on group projects with team members from various Tata group entities. </p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 mb-40">
                <div class="shadow-card full-height mb-0 disable-hover-effect">
                    <div class="shadow-card-content">
                        <div class="shadow-card-header">Duration</div>
                        <p>A blended learning approach with 1 week of online and 2 weeks of in-person classes at the TMTC campus. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right pr-3">
            <img src="img/backgrounds/executive-leadership-structure-element.svg" class="img-fluid" data-aos="fade-in" width="175">
        </div>
    </div>
</section>

<section class="section-exl-experts" id="faculty">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header text-center text-md-left">Faculty & <br class="d-none d-md-block"> Experts</header>
            </div>
            <div class="col-md-9 col-lg-8">

            <div class="global-content-width-650">
                <img src="img/backgrounds/exls-expert.svg" class="img-fluid pb-4" alt="">
                <p>The seminar is delivered by faculty from the Tata Management Training Centre and the Ross School of Business (University of Michigan). Pioneers of innovation and future-forward ideas, the faculty members are well-versed with the realities of the Indian and other developing markets. Each of them has extensive experience in research and consulting with companies within the Tata group, as well as other leading multinational corporations. This helps them share insights on how to translate concepts and tools to the group-specific context. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-highlights" id="highlights">
    <img src="img/backgrounds/exl-highlights.png" class="img-fluid d-none d-md-block" alt="">
    <img src="img/backgrounds/exl-highlights-mobile.png" class="img-fluid d-md-none w-100" alt="">
</section>

<section class="section-exl-gallery" id="gallery">
    <div class="container-fluid">
        
        <header class="section-header text-center mb-3">Gallery</header>
        <nav class="nav gallery-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-els-gallery-image-tab" data-toggle="tab" data-target="#nav-els-gallery-image" type="button" role="tab" aria-controls="nav-els-gallery-image" aria-selected="true">Images</button>
            <button class="nav-link" id="nav-els-gallery-video-tab" data-toggle="tab" data-target="#nav-els-gallery-video" type="button" role="tab" aria-controls="nav-els-gallery-video" aria-selected="false">Videos</button>
        </nav>
        
        <div class="tab-content gallery-tabs-content" id="nav-tabContent">
            <!-- IMAGE TAB CONTENT -->
            <div class="tab-pane fade show active" id="nav-els-gallery-image" role="tabpanel">
                <div class="gallery-image-slider dots-dashed">
                    <div class="gallery-image-slide">
                        <img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="">
                    </div>
                    
                    <div class="gallery-image-slide">
                        <img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <!-- VIDEO TAB CONTENT -->
            <div class="tab-pane fade" id="nav-els-gallery-video" role="tabpanel">
                <div class="gallery-image-slider dots-dashed">
                    <div>
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/tata-executive-mobile-banner.png" class="img-fluid" alt="">
                        </a>
                    </div>
                    
                    <div>
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/tata-executive-mobile-banner.png" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-tesimonial" id="testimonials">
    <div class="container">
        <header class="section-header text-center mb-3">Testimonials</header>
        
        
        <div class="testimonial-slider-wrapper">
            <img src="img/icons/quote-dark-blue.svg" class="testimonial-slider-fixed-quote" alt="">
            <div class="testimonial-slider dots-dashed">
                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">The energy, passion and patience of all faculty members was commendable, and all subject themes were relevant, aligned and futuristic.</p>
                        </div>
                    </div>
                </div>
                
                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">Crisp and to the point in identifying important concepts of senior general managers.</p>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">We learnt many new concepts. The concept of HR being completely aligned to customer needs and the external market situation was powerful.</p>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">One of the best takeaways for me from this programme was to understand the self, and I could correlate the learnings to myself to a large extent. And the one-on-one session was so helpful in understanding the assessment outcomes in detail.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-exl-team" id="team">
    <div class="container">
        <header class="section-header text-center bg-white">Meet the Team</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team1.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Karn Bhatia</h5>
                        <strong>Seminar Director</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team2.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Pearl Mascarenhas</h5>
                        <strong>Seminar Manager</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>