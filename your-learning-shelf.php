<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/your-learing-shelf-banner.png" alt="">
</section>

<section class="banner-shadow-content text-center">
A platform that offers Tata employees the opportunity to upgrade their skills and accelerate their learning journey, anytime, anywhere. Here you’ll find short online courses completely free of cost, on a range of subjects, from supply chain management to presentation skills. You can learn at your own pace, and showcase your achievement with a certificate of completion.
</section>

<section class="section-learning-shelf-cards">
    <div class="container">

    <img src="img/backgrounds/yellow-rod-2.svg" class="yls-element-1" data-aos="fade-in">
    <img src="img/backgrounds/yls-element-2.svg" class="yls-element-2" data-aos="fade-in">

        <div class="row">
            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Learn The Skill Of Communicating Effectively</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>What is communication</li>
                            <li>Verbal and non-verbal communication</li>
                            <li>Barriers in communication</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>

                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Supply Chain Planning And Scheduling</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>Basics of Supply Chain</li>
                            <li>Overview of Planning and Forecasting as part of Planning</li>
                            <li>Demand Management as a part of Planning</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Introduction To Soft Skills</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>What are Soft Skills</li>
                            <li> The importance of Soft Skills for success in our day-to-day life and for professional growth</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Strategic Sourcing & Category Management In Supply Chain Management</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>Overview of Strategic Sourcing</li>
                            <li>Strategic Sourcing process and activities such as Spend Analysis, RFx Management and Contract Management</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Presentation Skills</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>Steps to prepare effective slides in a Presentation</li>
                            <li>Dos and Don’ts of creating a Presentation</li>
                            <li>Points to remember while making a Presentation</li>  
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Master Data Management For Beginners</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>Concepts of Data Management System and its importance</li>
                            <li>Concepts of Master Data Management System and its importance</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <a class="learning-shelf-card">
                    <div class="learning-shelf-card-content">
                        <p>Business Etiquette</p>

                        <strong>What will you learn?</strong>
                        <ul>
                            <li>Meaning of Business Etiquette</li>
                            <li>How to dress for business engagements</li>
                            <li>Different types of courtesies extended to business associates</li>
                            <li>Points remember while making a presentation</li>
                        </ul>
                    </div>

                    <div class="learning-shelf-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-timer-light.svg" class="icon" height="20" width="20" alt="">
                                <span>1 Week</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-language-light.svg" class="icon" height="20" width="20" alt="">
                                <span>English</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-online-light.svg" class="icon" height="20" width="20" alt="">
                                <span>Online Self-paced</span>
                            </li>
                        </ul>
                        <div class="learning-shelf-card-link">Start Learning</div>
                    </div>
                </a>
            </div>

            <div class="col-12 text-center py-4">
                <img src="img/backgrounds/powered-by.svg" width="200" height="25" alt="">
            </div>

        </div>
    </div>
</section>

<?php include "components/footer.php" ?>