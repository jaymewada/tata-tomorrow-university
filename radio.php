<?php include "components/header.php" ?>
<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/radio-banner.png" alt="">
</section>

<div class="banner-shadow-content text-center">
    An all-audio exclusive in-house channel carrying fresh insights from the world of work to all our learners. Listen to The Radio Channel currently live with the following segments of audio-learning streams, plunge into our bite-sized playlists now.
</div>

<section class="section-radio-listing">
    <img src="img/backgrounds/radio-listing-bg.svg" class="radio-listing-element-1 duration-1s" width="90" data-aos="fade-right">
    <img src="img/backgrounds/bars-blue.svg" class="radio-listing-element-2" width="200" data-aos="fade-in">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-60">
                <a href="wonderful-collective.php" class="radio-card radio-card-green">
                    <div class="radio-card-content">
                        <span class="radio-card-number">1</span>
                        <div class="radio-card-image">
                            <img src="img/backgrounds/radio-card1.png" class="img-fluid" alt="">
                        </div>
                        <p class="mb-0">A podcast series that explores Tata stories through the lens of inclusion</p>
                        
                        <div class="radio-card-link link-hover-green">
                            <span class="pr-3">View Episodes</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="19" viewBox="0 0 23 19">
                                <g data-name="Group 3141" transform="translate(0.01)">
                                    <rect width="23" height="15" rx="2" transform="translate(-0.01 4)" fill="#22888c" />
                                    <rect width="21" height="2" rx="1" transform="translate(1.99)" fill="#23888c" />
                                    <path d="M0,0H4A0,0,0,0,1,4,0V2A0,0,0,0,1,4,2H1A1,1,0,0,1,0,1V0A0,0,0,0,1,0,0Z" transform="translate(3.99) rotate(90)" fill="#23888c" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(14.637 8.5) rotate(90)" fill="#fff" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(17.637 8.5) rotate(90)" fill="#fff" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(20.637 8.5) rotate(90)" fill="#fff" />
                                    <circle cx="3.5" cy="3.5" r="3.5" transform="translate(2.99 8)" fill="#22888c" stroke="#fff" stroke-width="1" />
                                </g>
                            </svg>
                        </div>
                    </div>
                </a>
            </div>
            
            <div class="col-md-6 col-lg-4 mb-60">
                <a href="my-hacks.php" class="radio-card radio-card-blue">
                    <div class="radio-card-content">
                        <span class="radio-card-number">2</span>
                        <div class="radio-card-image">
                            <img src="img/backgrounds/radio-card2.png" class="img-fluid" alt="">
                        </div>
                        <p class="mb-0">This podcast features unplugged conversations with subject matter experts from the Tata. </p>
                        <div class="radio-card-link">
                            <span class="pr-3">View Episodes</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="19" viewBox="0 0 23 19">
                                <g data-name="Group 3141" transform="translate(0.01)">
                                    <rect width="23" height="15" rx="2" transform="translate(-0.01 4)" fill="#004FAC" />
                                    <rect width="21" height="2" rx="1" transform="translate(1.99)" fill="#004FAC" />
                                    <path d="M0,0H4A0,0,0,0,1,4,0V2A0,0,0,0,1,4,2H1A1,1,0,0,1,0,1V0A0,0,0,0,1,0,0Z" transform="translate(3.99) rotate(90)" fill="#004FAC" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(14.637 8.5) rotate(90)" fill="#fff" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(17.637 8.5) rotate(90)" fill="#fff" />
                                    <rect class="vertical-bars" width="6" height="1.5" rx="0.75" transform="translate(20.637 8.5) rotate(90)" fill="#fff" />
                                    <circle cx="3.5" cy="3.5" r="3.5" transform="translate(2.99 8)" fill="#004FAC" stroke="#fff" stroke-width="1" />
                                </g>
                            </svg>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        
        
    </div>
</section>

<?php include "components/footer.php" ?>