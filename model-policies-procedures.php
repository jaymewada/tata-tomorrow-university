<?php include "components/header.php" ?>
<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php" class="active">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content white-element-lg">
    <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element mppw">
    <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">
        <div class="container">
            <div class="row position-relative" style="z-index:2;">
                <div class="col-md-8 col-lg-6 text-center mx-auto mb-3 mb-md-5">
                    <div>TATA CODE OF CONDUCT</div>
                    <header class="section-header section-header-sm text-light mb-0">Model Policies & Procedures</header>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Whistle-blower Policy</header>
                            <p>We are committed to provide channels and mechanism to receive and address any concern or complaint for unethical conduct.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Anti-Bribery & Anti-Corruption Police</header>
                            <p>We do not tolerate bribery or corruption in any form. This commitment underpins everything we do.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>


                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Conflict of Interest Procedure</header>
                            <p>We act in company’s best interest and ensure that personal associations does not have Conflict of Interest with conduct of company’s business.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>


                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Anti-Money Laundering Policy</header>
                            <p>We comply with Anti-Money Laundering regulations to prevent and check any breaches in the conduct of our businesses.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Gifts and Hospitality Policy</header>
                            <p>Gifts or hospitality that may create appearance of, or an actual, conflict of interest or illicit payment are unacceptable.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="shadow-link-card h-100">
                        <div class="shadow-link-content">
                            <header class="shadow-link-card-header">Gifts Disposal Procedure</header>
                            <p>Gifts offered in contravention to the policy are handled through the procedures set for declining or its disposal.</p>
                            <span>Know More</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="section-download-ltc">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
                <img src="img/backgrounds/coc.png" class="w-50 mb-5 mb-md-0" alt="">
            </div>
            <div class="col-md-6 align-self-center">
                <img src="img/backgrounds/code-of-conduct-element.svg" class="download-ltc-element" width="200" alt="">
                <header class="section-header my-3">Tata Code of Conduct 2015</header>
                <p>The Tata Code of Conduct outlines our commitment to each of our stakeholders</p>
                <a href="" class="btn-cta-navy d-flex align-items-center justify-content-center px-3" style="max-width: 230px;">
                    <i class="material-symbols-outlined">download</i>&nbsp;
                    Download</a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>