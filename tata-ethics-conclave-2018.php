<?php include "components/header.php" ?>
<br class="d-md-none">
<header class="section-header global-header-padding text-center m-0">Tata Ethics Conclave</header>


<ul class="page-navigation-list my-3">
    <li>
        <a href="tata-ethics-conclave-2018">
            <strong>2018</strong>
        </a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2021">2021</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2022">2022</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2023">2023</a>
    </li>
</ul>

<section class="section-ethics-conclave tec-2018">
    <div class="container">

        <img src="img/backgrounds/blue-d.svg" class="tec-d-1">
        <img src="img/backgrounds/bars-white.svg" class="tec-bars" data-aos="fade-in">
        <img src="img/backgrounds/blue-d.svg" class="tec-2018-2">
        <div class="row">
            <div class="col-lg-6 text-center mb-4">
                <img src="img/backgrounds/tec-2018.png" class="w-75 mx-auto" alt="">
            </div>
            <div class="col-lg-6 align-self-center">
                <p>The theme of TEC-2018 was to deliberate on and understand leading practices to improve the ethical culture within organisations. The conclave began with an address by S Padmanabhan, Group Chief Ethics Officer, followed by a keynote address on ‘Building Ethical Cultures’ by Leo Martin, Co-founder and Director, GoodCorporation, UK.</p>
                <p>R Nanda (Tata Chemicals) shared key findings from the Annual Compliance Report Cycle – 2018, derived from declarations made by 59 group companies. This was followed by an informative and insightful session on the impending impact of ‘Recent Amendments to the Prevention of Corruption Act’ by RS Bhatti, Chief Vigilance Officer, Airports Authority of India. Next, Arpinder Singh, Partner and Head, Forensic and Integrity Services, EY, shared recent trends in ethics and compliance, thereby setting the tone for the participants to think through and consider proactive measures to address imminent ethics-related risks in their organisations</p>

                <img src="img/backgrounds/tec-2018-2.png" class="img-fluid mb-3 my-md-4" alt="">

                <p>Ethics Leaders from TCS, Tata Power and Tata - AIA Life shared Case-related Challenges and Learnings followed by discussion on ethical dilemmas often faced by companies and their employees through three skit-based scenarios. Kashmira Mewawala (Tata Capital) then shared valuable professional experiences and actions taken in her company, based on her sustained engagement with its top leadership team. The conclave closed with an open house session hosted by S Padmanabhan, where matters of interest to the audience were candidly discussed.</p>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>