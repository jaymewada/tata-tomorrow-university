<?php include "components/header.php" ?>
<br class="d-md-none">
<header class="section-header global-header-padding text-center m-0">Tata Ethics Conclave</header>


<ul class="page-navigation-list my-3">
    <li>
        <a href="tata-ethics-conclave-2018">2018</a>

    </li>
    <li>
        <a href="tata-ethics-conclave-2021">
            <strong>2021</strong>
        </a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2022">2022</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2023">2023</a>
    </li>
</ul>

<section class="section-ethics-conclave">
    <img src="img/backgrounds/blue-d.svg" class="tec-d-1">
    <img src="img/backgrounds/bars-white.svg" class="tec-bars" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center align-self-center mb-4">
                <img src="img/backgrounds/tec-2021.png" class="w-75 mx-auto" alt="">
            </div>
            <div class="col-lg-6 align-self-center">
                <p>The theme of Tata Ethics Conclave 2021 was to deliberate Pivotal role of Ethics for Organisation’s Resilience in Crisis. The virtual conclave was broadcasted live from Taj Mahal Palace Hotel, Mumbai over 2 days on 21st and 22nd January 2021 and saw enthusiastic participation of 550+ ethics community members across 70+ group companies and invitees. The conclave was addressed by notable speakers from Tata group, Industry Leaders and Thought Leaders. In addition to the thought-provoking interactive sessions, select value chain partners and generation next employees shared their thoughts through video messages.</p>
            </div>
        </div>
    </div>
</section>


<section class="section-graphical-scribe">
    <img src="img/backgrounds/ethics-conclave-bars.svg" class="gs-bars" data-aos="fade-in">
    <div class="container-sm" style="max-width:1100px;">
        <header class="section-header text-center">Graphical scribe and <br> recording of the sessions</header>

        <div class="row">
            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/tc-banner-1.png" class="img-fluid">
                    </a>
                    <strong>Address by N. Chandrasekaran, Tata Group Chairman</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/tc-banner-2.png" class="img-fluid">
                    </a>
                    <strong>Ethics Journey @Tata - S. Padmanabhan, Tata Sons Pvt Ltd.</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/tc-banner-3.png" class="img-fluid">
                    </a>
                    <strong>Positive impact of responsible value chain partnership - Peter Betzel, CEO and Preet Dhupar, CFO, IKEA India</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/tc-banner-4.png" class="img-fluid">
                    </a>
                    <strong>Building cultural strength for organisation’s resilience - Philippa Foster Back CBE</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/tc-banner-5.png" class="img-fluid">
                    </a>
                    <strong>Underlying role of Ethics in crisis <br> – Jagdeep Singh, Partner, EY</strong>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include "components/footer.php" ?>