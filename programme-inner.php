<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin">
    <img src="img/banners/pi-banneer-3.jpg" />
</section>

<section class="banner-grid-content">
    <div class="banner-grid-item">
        <img src="img/icons/icon-in-person.svg" class="grid-item-icon" height="25" width="25" alt="">
        <div>In-Person</div>
    </div>
    <div class="banner-grid-item">
        <img src="img/icons/icon-rupee.svg" class="grid-item-icon" height="25" width="25" alt="">
        <div>25,000</div>
    </div>
    <div class="banner-grid-item">
        <img src="img/icons/icon-calendar.svg" class="grid-item-icon" height="25" width="25" alt="">
        <div>1 September 2022</div>
    </div>
    <div class="banner-grid-item">
        <img src="img/icons/icon-timer.svg" class="grid-item-icon" height="25" width="25" alt="">
        <div>6 sessions | 2.5 hrs each</div>
    </div>
</section>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list pi-navigation-list">
            <li>
                <a class="hash-link" href="#programme-brief">Brief</a>
            </li>
            <li>
                <a class="hash-link" href="#aim">Aim</a>
            </li>
            <li class="border-right-sm-0">
                <a class="hash-link" href="#outcomes">Outcomes</a>
            </li>
            <li>
                <a class="hash-link" href="#learner-profile">Learner Profile</a>
            </li>
            <li>
                <a class="hash-link" href="#structure">Structure</a>
            </li>
            <li>
                <a class="hash-link" href="#investment">Investment</a>
            </li>
            <li>
                <a class="hash-link" href="#faculty">Faculty</a>
            </li>
            <li>
                <a class="hash-link" href="#spotlight">Spotlight</a>
            </li>
            <li>
                <a class="hash-link" href="#testimonials">Testimonials</a>
            </li>
            <li>
                <a class="hash-link" href="#team">Team</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-brief" id="programme-brief">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-lg-8 mx-auto">
                <header class="section-header text-center mb-4">Programme Brief</header>
                <div class="global-content-width-600">
                    <p>The Human resource function has been evolving over the years with a gradual shift from managing mundane administrative responsibilities to becoming skilled business consultants. The HR professionals, even more in today’s environment, are expected to manage the challenges of transforming the workplace and bring in a strategic view on talent practices.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-aim programme-aim msc" id="aim">
    <img src="img/backgrounds/section-left-arrow-orange.svg" class="pi-aim-element-1" data-aos="fade-right">
    <img src="img/backgrounds/orange-vertical-lines.svg" class="pi-aim-element-2" data-aos="fade-down">
    <div class="container">
        <header class="section-header">Programme Aim</header>
        <div class="row">

            <div class="col-md-6 mb-4">
                <div class="bullet-card full-height">
                    <div class="bullet-card-content">
                        <span>1</span>
                        <p>Equip them with key leadership and enterprise skills of the current and the future </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="bullet-card full-height">
                    <div class="bullet-card-content">
                        <span>2</span>
                        <p>Prepare them for understanding the business environment and thereby acting as true Human capital consultant to the business leaders</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="bullet-card full-height">
                    <div class="bullet-card-content">
                        <span>3</span>
                        <p>To acquire latest knowledge and perspective on emerging HR concepts</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="bullet-card full-height">
                    <div class="bullet-card-content">
                        <span>4</span>
                        <p>Network and exchange ideas with other HR peers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-learning-outcome msc" id="outcomes">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-6">
                <header class="section-header">Learning Outcomes</header>
                <img src="img/backgrounds/bars-orange-sm.svg" class="img-fluid pb-4" data-aos="fade-in">
            </div>
            <div class="col-md-8 col-lg-5">
                <div class="outcome-content">
                    <strong>Learn about unstructured data sources, heterogenous complexity of the data and challenges.</strong>
                    <p>Understand how to tap into unstructured data such as Customer feedback, Social Media free
                        text, data ons/security lapses, Quality data, Call centre voice data, store traffic data.
                    </p>
                    <p> Learn a practical step-by-step approach to unstructured data analysis along with relevant tools and technologies.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-pi-attend msc" id="learner-profile">
    <div class="container">
        <header class="section-header">Who Should Attend</header>
        <div class="row">
            <div class="col-md-6 col-lg-5">
                <ul class="bullet-list">
                    <li>
                        <p>Mid to senior level managers from across functional areas (such as Sales support, Marketing, Customer Support, Customer experience management, HR, Stores etc.), with experience of at least 8-plus years, who typically deal with high volume of unstructured data.</p>
                    </li>
                    <li>
                        <p>Data analysts who wish to strengthen their domain knowledge.</p>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-5 offset-lg-1">
                <strong class="mb-3 d-block">Note:</strong>
                <ul class="bullet-list">
                    <li>
                        <p>Knowledge of statistics or programming skills are not required, and coding sessions are not included.</p>
                    </li>
                    <li>
                        <p>An exposure to tools & technologies will be provided via demonstrations and hands-on sessions where feasible. </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="pi-attend-element-1" data-aos="fade-in">
        <svg xmlns="http://www.w3.org/2000/svg" width="144" height="71.049" viewBox="0 0 144 71.049">
            <g id="Group_4548" data-name="Group 4548" transform="translate(-64 -383.998)">
                <line y2="71.049" transform="translate(64.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(77.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(90.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(103.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(116.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(129.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(142.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(155.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(168.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(181.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(194.5 383.998)" stroke="#b74615" stroke-width="1" />
                <line y2="71.049" transform="translate(207.5 383.998)" stroke="#b74615" stroke-width="1" />
            </g>
        </svg>
    </div>
</section>

<section class="section-learning-themes msc" id="structure">
    <img src="img/backgrounds/bars-yellow-sm.svg" class="pi-learning-themes-element-1" data-aos="fade-in">
    <div class="container">
        <header class="section-header text-center">Learning Themes</header>
        <div class="row">
            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 1 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Path of HR consulting </p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 2 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Cultivate the Talent Strategy: People Analytics & HR solutions</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 3 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Untraveled wilderness: Future of work and future skills</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 4 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Scouting for organizational structures to meet business needs</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 5 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Knowing your terrain: Commercial awareness for HR professionals</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 6 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Backpacking for building a high performance culture</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 7 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Understanding Employee Relations landscape</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 8 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Communication for influencing</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 9 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Discover self</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 10 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Learning compass of the organization: Evolving into a learning organization</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 11 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Personal Branding to reach your destination</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 12 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Moving towards future proofing HR for the workforce of future</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 13 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Leadership learnings from ancient Art</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 14 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Program management</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 15 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Changing Paths: HR as a change leader</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 16 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>A real guide: Becoming an effective Mentor</p>
                    </div>
                </a>
            </div>

            <div class="col-lg-6">
                <a href="" class="horizontal-image-card">
                    <div class="horizontal-card-text"> Module 17 </div>
                    <div class="horizontal-card-content horizontal-learning-card-content">
                        <p>Immersion session</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section-learning-pedagogy msc">
    <div class="container">
        <header class="section-header text-center">Learning Pedagogy</header>

        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header">Methodology</div>
                    <p>Blended learning over a period of 10 weeks. This course offers 9 weeks of virtual learning and 3 days in-person learning @ TMTC</p>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header">Certification</div>
                    <p>On successful completion of the course, participants shall be certified through a joint certificate by University of Cambridge Judge Business School and TATA Management Training Centre</p>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header">Duration</div>
                    <p>Spread across 2.5 months, with 6-8 hrs of learning every week </p>
                </div>
            </div>

            <div class="col-md-6 mb-4">
                <div class="learning-pedagogy-card">
                    <div class="pedagogy-card-header">Continuous learning</div>
                    <p>Post completion of the program, the participants will immerse into an extended learning journey for another 6 months</p>
                </div>
            </div>
        </div>
    </div>
    <img src="img/backgrounds/orange-rod-msc.svg" class="programme-inner-rod" data-aos="fade-in">
</section>

<section class="section-investment" id="investment">
    <div class="container">
        <header class="section-header text-center">Investment</header>

        <div class="investment-content-wrapper">
            <div class="row">
                <div class="col-md-4 align-self-center">
                    <div class="investment-price-card">
                        <img src="img/icons/icon-rupee.svg" height="" width="75" alt="">
                        <strong>25,000</strong>
                        <small>(Plus applicale taxes per person)</small>
                    </div>
                </div>
                <div class="col-md-8 align-self-center">
                    <div>The program fee also includes an annual access to Tata Tomorrow University ‘My Learning Cloud’ platform.</div>
                    <time>Last Date for Nomination: 25 June 2022</time>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-cancellation-policy">
    <div class="container">
        <div class="global-content-width-650">
            <header class="section-header section-header-sm text-center">Cancellation Policy</header>
            <p>Cancellation more than 20 days prior to the start of the programme will be accepted at no charge. Cancellations 10- 20 days prior to the programme will be subject to a payment of 50% of the programme fee (plus applicable taxes), and cancellations received less than 10 days prior to the programme start date will be subject to payment of 100% of the programme fee (plus applicable taxes).</p>
            <p>Please Note: All cancellations must be confirmed via email to the program coordinator.</p>
        </div>
    </div>
</section>

<section class="section-faculty section-pi-faculty msc" id="faculty">
    <img src="img/backgrounds/pi-faculty-bg-msc.svg" class="pi-faculty-element-1" data-aos="fade-in">
    <img src="img/backgrounds/orange-triangle-right-msc.svg" class="bg-triangle-right">
    <div class="container">
        <header class="section-header text-center">Faculty & Expert</header>
        <div class="programme-faculty-slider dots-dashed">
            <div>
                <div class="vertical-card mx-auto" style="max-width:calc(100% - 30px);">
                    <img src="img/backgrounds/pexpert1.png" class="img-fluid" alt="">
                    <div class="vertical-card-content text-center">
                        <div class="vertical-card-header">Faculty Name</div>
                        <strong>Faculty or Expert Designation, followed by Institution Name</strong>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
                    </div>
                </div>
            </div>

            <div>
                <div class="vertical-card mx-auto" style="max-width:calc(100% - 30px);">
                    <img src="img/backgrounds/pexpert2.png" class="img-fluid" alt="">
                    <div class="vertical-card-content text-center">
                        <div class="vertical-card-header">Faculty Name</div>
                        <strong>Faculty or Expert Designation, followed by Institution Name</strong>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
                    </div>
                </div>
            </div>

            <div>
                <div class="vertical-card mx-auto" style="max-width:calc(100% - 30px);">
                    <img src="img/backgrounds/pexpert3.png" class="img-fluid" alt="">
                    <div class="vertical-card-content text-center">
                        <div class="vertical-card-header">Faculty Name</div>
                        <strong>Faculty or Expert Designation, followed by Institution Name</strong>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
                    </div>
                </div>
            </div>

            <div>
                <div class="vertical-card mx-auto" style="max-width:calc(100% - 30px);">
                    <img src="img/backgrounds/pexpert2.png" class="img-fluid" alt="">
                    <div class="vertical-card-content text-center">
                        <div class="vertical-card-header">Faculty Name</div>
                        <strong>Faculty or Expert Designation, followed by Institution Name</strong>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
                    </div>
                </div>
            </div>

            <div>
                <div class="vertical-card mx-auto" style="max-width:calc(100% - 30px);">
                    <img src="img/backgrounds/pexpert3.png" class="img-fluid" alt="">
                    <div class="vertical-card-content text-center">
                        <div class="vertical-card-header">Faculty Name</div>
                        <strong>Faculty or Expert Designation, followed by Institution Name</strong>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <img src="img/backgrounds/mustard-rod.svg" class="bg-mustard-rod-right" data-aos="fade-in">
</section>

<section class="section-highlights section-programme-spotlight msc" id="spotlight">
    <img src="img/backgrounds/orange-spotlight-bg.svg" class="pi-spotlight-element-1" data-aos="fade-right">
    <img src="img/backgrounds/orange-vertical-lines.svg" class="pi-spotlight-element-2" data-aos="fade-down">
    <div class="container-fluid">
        <header class="section-header text-center">Spotlight</header>
        <div class="gallery-image-slider dots-dashed">
            <div>
                <div class="gallery-image-slide">
                    <img src="img/backgrounds/hightlights.png" class="img-fluid" alt="">
                </div>
            </div>

            <div>
                <div class="gallery-image-slide">
                    <img src="img/backgrounds/highlights2.png" class="img-fluid" alt="">
                </div>
            </div>

            <div>
                <div class="gallery-image-slide">
                    <img src="img/backgrounds/highlights3.png" class="img-fluid" alt="">
                </div>
            </div>

            <div>
                <div class="gallery-image-slide">
                    <img src="img/backgrounds/highlights4.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-pi-testimonial msc" id="testimonials">
    <div class="container">
        <header class="section-header text-center mb-4">Testimonials</header>


        <div class="testimonial-slider-wrapper">
            <img src="img/icons/orange-testimonial-quote.svg" width="100" height="75" class="testimonial-slider-fixed-quote">
            <div class="testimonial-slider dots-dashed">
                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <img src="" alt="">
                            <p>“The program touches upon the various functional aspects – like current HR challenges faced by the Senior HR leadership team in terms of their business cycle and talent. Secondly, it is very forward looking, a futuristic thought-provoking program. It emphasised a lot on thought leadership, being very reflective in nature.”</p>

                            <strong>Full Name</strong>
                            <strong>Designation, Company Name</strong>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <img src="" alt="">
                            <p>The faculty was terrific — knowledgeable, deeply involved, patient and supportive. By encouraging interaction, they made even an intense session interesting. Heartfelt thanks!</p>

                            <strong>Full Name</strong>
                            <strong>Designation, Company Name</strong>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <img src="" alt="">
                            <p>“The program touches upon the various functional aspects – like current HR challenges faced by the Senior HR leadership team in terms of their business cycle and talent. Secondly, it is very forward looking, a futuristic thought-provoking program. It emphasised a lot on thought leadership, being very reflective in nature.”</p>

                            <strong>Full Name</strong>
                            <strong>Designation, Company Name</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-programme-team msc" id="team">
    <div class="container">
        <header class="section-header text-center">Meet the Team</header>
        <div class="row justify-content-center">

            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/pi-team1.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Chandna Sethi</h5>
                        <strong>Programme Director</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/pi-team2.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Zeeba Irani</h5>
                        <strong>Programme Coordinator</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>