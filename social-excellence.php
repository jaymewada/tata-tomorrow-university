<?php include "components/header.php" ?>

<div class="section-tbeg-tabs">
    <header class="section-header text-center">Tata Business Excellence Group</header>

    <nav class="tbeg-pages-navigation" style="grid-template-columns:auto auto auto auto auto;">
        <div>
            <a href="business-excellence.php">Business Excellence</a>
        </div>
        <div>
            <a href="cyber-excellence.php">Cyber Excellence</a>
        </div>

        <div>
            <a href="data-excellence.php">Data Excellence</a>
        </div>
        <div>
            <a href="safety-excellence.php" >Safety Excellence</a>
        </div>
        <div>
            <a href="social-excellence.php" class="active">Social Excellence</a>
        </div>
    </nav>

    <div class="tbeg-tabs-content-wrapper">
    <img src="img/backgrounds/bars-white-sm.svg" class="tbeg-self-paced-element" data-aos=fade-in" >
        <div class="container">
            <header class="section-header section-header-sm">Self-Paced Modules</header>
            <div class="row mx-auto justify-content-center" style="max-width:675px;">
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Introduction to Tata Affirmative Action Programme (TAAP)</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>

                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>TAAP Assessment Framework</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-tbeg-classroom">
<img src="img/backgrounds/tbeg-classroom-element-1.svg" class="tbeg-classroom-element-1" data-aos="fade-in">
<img src="img/backgrounds/tbeg-classroom-element-2.svg" class="tbeg-classroom-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header section-header-sm">Classroom Programmes</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Advanced Team Leaders Programme</h6>
                    <p>Enables leaders to reflect on the overall process of TAAP Assessment, improvements, priorities and opportunities for the next assessment cycle to elevate overall experience of TAAP Assessment process.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Affirmative Action Assessor Programme</h6>
                    <p>Geared towards preparing Assessors pool to participate in external TAAP Assessment and also play role of catalyst to embed TAAP Policy within their own ecosystem.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            
            <div class="w-100"></div>

            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Experienced Assessor Affirmative Programme</h6>
                    <p>Designed for experienced Assessors to get acquainted with the updates in assessment process, new focus areas and changes in the criteria. The programme is specially curated and focused on the competency requirements of the upcoming assessment cycle.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Affirmative Action Orientation and Capability Building Programme </h6>
                    <p>Designed to strengthen the capabilities of Tata employees on Tata Affirmative Action Programme (TAAP). This programme enhances their understanding and enable them to play effective catalyst role to embed TAAP principles across the organisation.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-programme">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-assesor-programme.svg" height="70" alt="">
                    <div>
                        <strong>Assesor Programme</strong>
                        <p>Enables participants to become a Certified Assessor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-champion-programme.svg" height="70" alt="">
                    <div>
                        <strong>Champion Programme</strong>
                        <p>Enables understanding and application of the model in own organisation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-tbeg-contact" id="contact-us">
<img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <strong class="text-navy-2 fw-500">Archana Lawande</strong>
                        <div class="text-muted my-10">archanalawande@tata.com</div>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>