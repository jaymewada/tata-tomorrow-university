<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin">
    <img src="img/banners/vitality-600.png" class="img-fluid" alt="">
</section>

<div class="banner-shadow-content text-center">
    Our People Vitality is a proactive and futuristic approach to having energized and engaged teams. As companies adopt a people-first strategy as an important aspect of business plans, this is an ideal opportunity to create a workforce which is happy, hopeful, inspired, physically and mentally fit. To take on challenges and forge ahead. Vitality is about replenishing more than you deplete, it is always a state of surplus!
</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list">
            <li>
                <a class="hash-link" href="#conversations">Conversations</a>
            </li>
            <li>
                <a class="hash-link" href="#playlist">Playlist</a>
            </li>
            <li>
                <a class="hash-link" href="#support">Support</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-wellness-conversation" id="conversations">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-lg-6 mx-auto text-center mb-5">
                <header class="section-header mb-3">Vitality Conversations</header>
                <p class="section-description mb-0">Log onto the live webinar sessions capturing diverse facets of wellness through conversations, right from digital health to decision making, and from happiness to self-care. We’ve got you covered.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <span class="feature-ribbion">Powered by TCS Cares</span>
                        <img src="img/backgrounds/wl1.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">Decision Making: An Art or a Skill?</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>

                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Retd. Major Deepshikha Gupta</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>12 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <span class="feature-ribbion">Powered by TCS Cares</span>
                        <img src="img/backgrounds/wl2.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">The Art of Happiness</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Retd. Major Deepshikha Gupta</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>13 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <img src="img/backgrounds/wl3.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">Champions of Wellbeing</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>

                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Sneha Satheendran</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>14 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <span class="feature-ribbion">Powered by TCS Cares</span>
                        <img src="img/backgrounds/wl4.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">Becoming a Resilient You</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>

                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Neha Garg</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>15 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <span class="feature-ribbion">Powered by TCS Cares</span>
                        <img src="img/backgrounds/wl5.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">Self Care on the Go</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>

                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Retd. Major Deepshikha Gupta</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>27 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-40">
                <div class="programme-card vitality-programme-card">
                    <div class="programme-card-image">
                        <img src="img/backgrounds/wl6.svg" class="img-fluid w-100" alt="">
                    </div>
                    <div class="programme-card-content">
                        <p class="fw-500 mb-1">Eating for Good Health</p>
                    </div>

                    <div class="programme-card-fixed-content">
                        <ul class="list-unstyled">
                            <li>
                                <img src="img/icons/icon-speaker.svg" class="icon" height="22" width="22" alt="">
                                <span>Neha Garg</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-calendar.svg" class="icon" height="22" width="21" alt="">
                                <span>28 Oct 2022</span>
                            </li>

                            <li>
                                <img src="img/icons/icon-clock.svg" class="icon" height="22" width="22" alt="">
                                <span>4 to 5:15 pm</span>
                            </li>
                        </ul>
                        <a href="">See More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-wellness-playlist" id="playlist">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-lg-6 mx-auto text-center mb-5">
                <header class="section-header mb-3">Vitality Playlist</header>
                <p class="section-description mb-0">Grab your headphones and tune in to a curated Learning Latitude playlist, with audio and video content on everything from Health & Wellness to Managing Self & Others and much more.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Happiness - How to Lead a Fulfilling Life</p>
                        <small>DVR Seshadhri - Professor, Indian School of Business</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Food as Medicine</p>
                        <small>Dr. Biswaroop Roy Chowdhury - International Medical NutritionistDr.</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Altering the Narrative of your Life!</p>
                        <small>Vandana Saxena Poria OBE, FCA - THE HUMAN ALARM CLOCK & CO.</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Sports for Fitness</p>
                        <small>Aparna Popat - Aparna - Olympian, TEDxSpeaker, Sports Leader</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Beyond Brain and Mind: The Exciting Future of Consciousness</p>
                        <small>Sangeetha Menon, ,Ph.D, Dean, School of Humanities Professor & Head, NIAS Consciousness Studies Programme</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Your Mind Matters</p>
                        <small>Alpana Sawant, Consultant & Senior Counsellor - 1to1help</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>An Everyday Practice for Rejuvenation- Why Sleep is Important, and How to Get it Right</p>
                        <small>Major Deepshikha Gupta (Retd), Senior Counsellor, 1to1help.net</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Digital Health for our Young Ones</p>
                        <small>Manopriya Tuli, Team Lead counsellor, 1to1 help</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Mindfulness and Emotional Intelligence at Work</p>
                        <small>Suhasini Seshadri - Leadership and Personal Mastery Coach Consultant | Podcaster- Inspired Living</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Champions of Well-being</p>
                        <small>Sneha Satheendran, Counsellor & Assistant Team Lead, 1to1 help</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Eating for Good Health</p>
                        <small>Amy Jones, Associate and Specialist Dietition - 1to1 Help</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>


                    <div>
                        <p>Adaptability-The Essential Future-proofing Skill</p>
                        <small>Sneha Satheendran, Counsellor & Assistant Team Lead - 1to1help</small>
                    </div>

                </a>
            </div>

            <div class="col-md-6 mb-30">
                <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox class="wellness-playlist-card">

                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="62" viewBox="0 0 62 62">
                        <g transform="translate(-634 -459)">
                            <circle cx="31" cy="31" r="31" transform="translate(634 459)" fill="#f8ce46" />
                            <path d="M12,0,24,20H0Z" transform="translate(678 478) rotate(90)" fill="#fff" />
                        </g>
                    </svg>
                    <div>
                        <p>Happiness at Work</p>
                        <small>Komal B, Counsellor - 1to1help</small>
                    </div>

                </a>
            </div>


        </div>
    </div>
</section>

<section class="section-wellness-support" id="support">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <header class="section-header text-center text-white">Wellness <br class="d-none d-lg-block"> Support</header>
            </div>
            <div class="col-lg-8">
                <p>Leverage the extension of our MoU on Tata EAP, Employee Assistance Programme, for Tata companies and our employees. This programme offers confidential counselling services to employees and their family members for emotional health and wellness. We have now extended our group-level MSA with revised commercials that will help companies partner and utilise the benefits effectively. Apart from counselling services, managerial support, self-help online library, e-workshops and sensitisation sessions and self-help assessment tools are also accessible via this arrangement.</p>
                <p>For more details, please reach out to Renuka Shetty at rshetty@tata.com</p>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>