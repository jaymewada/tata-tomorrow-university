<?php include "components/header.php" ?>
<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php" class="active">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content white-element">
    <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element">
    <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-6 mx-auto">
                    <header class="section-header section-header-sm text-light">Leadership of Business</header>
                    <p>The Tata group’s Leadership of Business Ethics (LBE) framework offers guidance for continuously developing, strengthening and measuring the culture of business ethics for Tata companies. The LBE framework enables Tata companies to chart out their action plan for ethics initiatives effectively and efficiently on a year-on-year basis. The LBE framework’s four pillars are based on following:</p>
                    <br>
                </div>
                <div class="col-md-10 col-lg-6 mx-auto">
                    <div class="ethics-survey-wrapper">
                        <ul class="blue-dot-list has-connecting-line">
                            <li>
                                <h5 class="dot-list-header">Leadership</h5>
                                <p>Leadership commitment and action.</p>
                            </li>

                            <li>
                                <h5 class="dot-list-header">Compliance Structure</h5>
                                <p>Goverance Structure in organisation</p>
                            </li>
                            <li>
                                <h5 class="dot-list-header">Communication & Training</h5>
                                <p>Level of communication & training.</p>
                            </li>
                            <li>
                                <h5 class="dot-list-header">Measurement of Effectiveness</h5>
                                <p>Mechanism available for stakeholders to seek guidance for matters related to ethics.</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>