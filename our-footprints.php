<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	 <img src="img/banners/our-footprints.png">
</section>

<section class="section-page-nav">
	<div class="container">
		<div class="about-page-navigation-list">
			<ul class="page-navigation-list m-0">
				<li>
					<a href="about">Director’s Note</a>
				</li>
				<li>
					<a href="our-journey">Our Journey</a>
				</li>
				<li>
					<a href="our-world">Our World</a>
				</li>
				<li class="is-active">
					<a href="our-footprints">Our Footprints</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<section class="section-journeys">
	<img src="img/backgrounds/gold-d-left.svg" class="journeys-element-1 d-none d-md-block">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<header class="section-header m-0">Our Learning <br>Journeys</header>
				<img src="img/backgrounds/bars-gold.svg" class="my-4" data-aos="fade-in">
			</div>
			<div class="col-md-8">
				<div class="global-content-width-600 ml-0">
					<div class="fw-500 pb-2">Tata Management Training Centre: </div>
					<p class="mb-4">Our flagship four-tier leadership development programmes cater to executives at different levels in their career, while our open programmes focus on capability building in specific areas. Currently we offer open programmes in Digital Transformation, Strategy & Innovation, Sustainability, Supply Chain Management, Sales, Marketing, Customer Centricity, Human Resource Management, Finance, Ethics, Leadership and other relevant disciplines. Managers can nominate teams or individual employees for any of our open programmes, subject to eligibility. We also work with companies to create high-impact learning experiences that are tailored to their specific capability development needs.</p>
					<div class="fw-500 pb-2">Tata Tomorrow University:</div>
					<p>This is a one-stop digital destination with a vast repository of multimedia resources that Tata employees can learn from in their own time, at their own pace, including webinars, podcasts, stories, gamified exhibits, coaching modules and much more.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-tmtc-slider">
	<header class="section-header text-center text-light mb-3">Life @TMTC</header>
	<img src="img/backgrounds/white-d.svg" width="40" class="tmtc-slider-element-1">
	<img src="img/backgrounds/white-rod.svg" class="tmtc-slider-element-2" data-aos="fade-in">

	<div class="gallery-image-slider dots-dashed has-image-caption active-dots-white inactive-dots-black">
		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-1.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Annual Day 2022-ThrowBall</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-2.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Annual Party for Support Staff-2023-2</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-3.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Annual Party for Supprt Staff-2023</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-4.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Diwali Celebration 2022</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-5.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Diwali Celebration-3- 2022</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-6.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Diwali Celebration-3- 2022</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-7.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Lt. Col. Ravishankar Farewell 2</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-8.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Lt. Col. Ravishankar Farewell</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-9.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Prize Distribution for the winners-2023 Annual Day</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-10.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>TMTC Annual Day 2022_Tambola Games</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-11.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>TMTC Annual Day 2023-Cricket Match</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-12.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Ganesh Puja-17 Sept 2021</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-13.png" class="img-fluid">
				<div class="gallery-image-caption">

					<p>Belbin Team Management-Oct 2021</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-14.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Group Photo_BlueMint-Batch4</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-15.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Gyansarovar-Guiding User</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-16.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>JLR Graduation program-Dec 2021</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-17.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Prof Das Addressing TGSLS-2023</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-18.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Prof Krishna Palepu Addressing TGSLS- 2023</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-19.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>Rishi & Ranveer from TGeLS-2019 at Gyansarovar</p>
				</div>
			</div>
		</div>

		<div>
			<div class="gallery-image-slide">
				<img src="img/backgrounds/la-tmtc-2-.png" class="img-fluid">
				<div class="gallery-image-caption">
					<p>TGSLS-2023 Batch</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-outreach">
	<img src="img/backgrounds/gold-d-left.svg" class="outreach-element-1 d-none d-md-block">
	<div class="container">
		<div class="row">
			<header class="section-header">Community <br> Outreach</header>
		</div>
	</div>

		 
		<div>
			<div class="community-outreach-slider dots-dashed">
				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>
						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-1.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>
						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-2.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>

						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-3.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>
						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-4.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>
						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-5.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="row">
						<div class="col-md-5 order-2 order-md-1 align-self-center">
							<div class="outreach-content-card">
								<h6>Initiative</h6>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, </p>
							</div>
						</div>
						<div class="col-md-7 order-1 order-md-2">
							<div class="gallery-image-slide">
								<img src="img/backgrounds/co-6.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>

<section class="section-about-testimonials" id="testimonials">
	<img src="img/backgrounds/bars-gold.svg" class="about-testimonial-element-1" data-aos="fade-in">
	<header class="section-header text-center mb-4 text-light">Testimonials</header>
	<div class="testimonial-slider-wrapper">
		<img src="img/icons/gold-testimonial-quote.svg" width="100" height="75" class="testimonial-slider-fixed-quote">
		<div class="testimonial-slider dots-dashed">
			<div>
				<div class="testimonial-slide">
					<div class="testimonial-slide-content">
						<img src="" alt="">
						<p>“The program touches upon the various functional aspects – like current HR challenges faced by the Senior HR leadership team in terms of their business cycle and talent. Secondly, it is very forward looking, a futuristic thought-provoking program. It emphasised a lot on thought leadership, being very reflective in nature.”</p>

						<strong>Full Name</strong>
						<strong>Designation, Company Name</strong>
					</div>
				</div>
			</div>

			<div>
				<div class="testimonial-slide">
					<div class="testimonial-slide-content">
						<img src="" alt="">
						<p>The faculty was terrific — knowledgeable, deeply involved, patient and supportive. By encouraging interaction, they made even an intense session interesting. Heartfelt thanks!</p>

						<strong>Full Name</strong>
						<strong>Designation, Company Name</strong>
					</div>
				</div>
			</div>

			<div>
				<div class="testimonial-slide">
					<div class="testimonial-slide-content">
						<img src="" alt="">
						<p>“The program touches upon the various functional aspects – like current HR challenges faced by the Senior HR leadership team in terms of their business cycle and talent. Secondly, it is very forward looking, a futuristic thought-provoking program. It emphasised a lot on thought leadership, being very reflective in nature.”</p>

						<strong>Full Name</strong>
						<strong>Designation, Company Name</strong>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>



<?php include "components/footer.php" ?>