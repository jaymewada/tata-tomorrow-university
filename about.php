<?php include "components/header.php" ?>

<section class="section-banner-video global-header-margin">
	<video controls autoplay loop muted class="video-banner" poster="img/banners/about-banner.png">
		<source src="video/Campus-trim.mp4" type="video/mp4">
		<source src="video/Campus-trim.mp4" type="video/ogg">
		Your browser does not support the video tag.
	</video>
</section>

<section class="section-page-nav">
	<div class="container">
		<div class="about-page-navigation-list">
			<ul class="page-navigation-list m-0">
				<li class="is-active">
					<a href="directors-note">Director’s Note</a>
				</li>
				<li>
					<a href="our-journey">Our Journey</a>
				</li>
				<li>
					<a href="our-world">Our World</a>
				</li>
				<li>
					<a href="our-footprints">Our Footprints</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<section class="section-director">
	<img src="img/backgrounds/gold-d-left.svg" width="40" class="about-director-element-1">
	<header class="section-header section-header-sm text-center m-0">Message from the Director</header>
	
	<div class="about-intro-card">
		<div class="row">
			<div class="col-md-5 pr-md-0">
				<div class="director-img-wrapper">
					<img src="img/backgrounds/director.png" class="img-fluid" alt="" />
				</div>
			</div>
			<div class="col-md-7 align-self-center">
				<div class="pl-md-3">
					<h5>Anand Shankar</h5>
					<p style="max-width:365px;" class="m-0">
						Anand is an experienced strategist and global expert in Human Resources transformation and the future of
						work. He has extensive knowledge of global talent, with particular expertise in the Asia-Pacific and Middle
						East & Africa geographies. A Marshall Goldsmith-certified Coach and Mentor, Anand helps organisations with
						leadership development, people enablement and organisational design and structuring.
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="director-player-wrapper">
		<img src="img/backgrounds/director-element-1.svg" class="dirctor-player-element" data-aos="fade-up" />
		<div class="d-player-container">
			<!-- Add class 'pause' to change the icon -->
			<button id="btn-about-transcript" class=""></button>
			<svg xmlns="http://www.w3.org/2000/svg" width="459.073" height="54.593" viewBox="0 0 459.073 54.593">
				<g id="Group_6704" data-name="Group 6704" transform="translate(-1276.5 1845.5)">
					<g id="Group_6714" data-name="Group 6714">
						<line
						id="Line_834"
						data-name="Line 834"
						y2="30.814"
						transform="translate(1299.279 -1833.611)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_842"
						data-name="Line 842"
						y2="38.074"
						transform="translate(1364.614 -1837.24)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_846"
						data-name="Line 846"
						y2="30.814"
						transform="translate(1408.171 -1802.796) rotate(180)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_836"
						data-name="Line 836"
						y2="29.038"
						transform="translate(1288.389 -1832.723)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_843"
						data-name="Line 843"
						y2="29.038"
						transform="translate(1375.503 -1832.723)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_845"
						data-name="Line 845"
						y1="38.074"
						transform="translate(1397.282 -1837.24)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_837"
						data-name="Line 837"
						y2="16.334"
						transform="translate(1277.5 -1826.37)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_844"
						data-name="Line 844"
						y2="17"
						transform="translate(1386.573 -1826.704)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_835"
						data-name="Line 835"
						y2="23.593"
						transform="translate(1310.168 -1830)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_841"
						data-name="Line 841"
						y2="23.593"
						transform="translate(1353.725 -1830)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_847"
						data-name="Line 847"
						y2="23.593"
						transform="translate(1419.06 -1806.407) rotate(180)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_839"
						data-name="Line 839"
						y2="23.593"
						transform="translate(1331.946 -1830)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_838"
						data-name="Line 838"
						y2="32.629"
						transform="translate(1321.057 -1834.518)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_849"
						data-name="Line 849"
						y2="52.593"
						transform="translate(1440.839 -1844.5)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_840"
						data-name="Line 840"
						y2="32.629"
						transform="translate(1342.836 -1834.518)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
						<line
						id="Line_848"
						data-name="Line 848"
						y2="32.629"
						transform="translate(1429.95 -1801.889) rotate(180)"
						fill="none"
						stroke="#fff"
						stroke-linecap="round"
						stroke-width="2"
						/>
					</g>
					<g id="Group_6713" data-name="Group 6713">
						<g id="Group_6712" data-name="Group 6712">
							<line
							id="Line_869"
							data-name="Line 869"
							y2="38.074"
							transform="translate(1615.067 -1837.24)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_873"
							data-name="Line 873"
							y2="30.814"
							transform="translate(1658.624 -1802.796) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_870"
							data-name="Line 870"
							y2="29.038"
							transform="translate(1625.956 -1832.723)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_862"
							data-name="Line 862"
							y2="29.038"
							transform="translate(1582.399 -1832.723)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_863"
							data-name="Line 863"
							y2="16.334"
							transform="translate(1593.289 -1826.37)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_853"
							data-name="Line 853"
							y2="23.593"
							transform="translate(1484.396 -1830)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_860"
							data-name="Line 860"
							y2="32.629"
							transform="translate(1560.621 -1834.518)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_872"
							data-name="Line 872"
							y1="38.074"
							transform="translate(1647.735 -1837.24)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_861"
							data-name="Line 861"
							y1="38.074"
							transform="translate(1571.51 -1837.24)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_852"
							data-name="Line 852"
							y1="21.74"
							transform="translate(1473.507 -1829.074)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_879"
							data-name="Line 879"
							y1="21.74"
							transform="translate(1723.96 -1829.074)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_871"
							data-name="Line 871"
							y2="30.814"
							transform="translate(1636.846 -1833.611)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_868"
							data-name="Line 868"
							y2="23.593"
							transform="translate(1604.178 -1830)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_874"
							data-name="Line 874"
							y2="23.593"
							transform="translate(1669.513 -1806.407) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_851"
							data-name="Line 851"
							y2="23.593"
							transform="translate(1462.617 -1806.407) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_878"
							data-name="Line 878"
							y2="23.593"
							transform="translate(1713.07 -1806.407) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_880"
							data-name="Line 880"
							y1="13"
							transform="translate(1734.573 -1824.704)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_859"
							data-name="Line 859"
							y2="23.593"
							transform="translate(1549.731 -1806.407) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_876"
							data-name="Line 876"
							y2="52.593"
							transform="translate(1691.292 -1844.5)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_855"
							data-name="Line 855"
							y2="52.593"
							transform="translate(1506.174 -1844.5)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_856"
							data-name="Line 856"
							y2="52.593"
							transform="translate(1517.064 -1844.5)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_875"
							data-name="Line 875"
							y2="32.629"
							transform="translate(1680.403 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_857"
							data-name="Line 857"
							y2="32.629"
							transform="translate(1527.953 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_850"
							data-name="Line 850"
							y2="32.629"
							transform="translate(1451.728 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_877"
							data-name="Line 877"
							y2="32.629"
							transform="translate(1702.181 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_854"
							data-name="Line 854"
							y2="32.629"
							transform="translate(1495.285 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
							<line
							id="Line_858"
							data-name="Line 858"
							y2="32.629"
							transform="translate(1538.842 -1801.889) rotate(180)"
							fill="none"
							stroke="#00064d"
							stroke-linecap="round"
							stroke-width="2"
							/>
						</g>
					</g>
				</g>
			</svg>
		</div>
	</div>
</section>

<div class="transcript-wrapper w-100">
	<div class="collapse px-3" id="transcript-content">
		<header class="section-header section-header-sm transcript-content-header">
			Transcript of Message from Director
		</header>
		<div class="transcript-content-wrapper transcript-content-black">
			<ul>
				<li>
					<span>0s</span>
					<div>Anand Shankar:</div>
					<p>Hello</p>
				</li>
				
				<li class="is-active">
					<span>1.6s</span>
					<p>
						Welcome to Tata Management Training Centre, both to our campus in Pune and to our digital destination, Tata
						Tomorrow University.
					</p>
				</li>
				
				<li>
					<span>6.0s</span>
					<p>
						Tata Management Training Centre or TMTC was founded almost six decades ago to provide management training to
						Tata leaders at various stages of their career, and to foster unity and knowledge sharing within the group
						leadership.
					</p>
				</li>
				
				<li>
					<span>12.2s</span>
					<p>
						While executive education is widespread today, this was a pioneering initiative when TMTC was set up. Over
						the years, TMTC has remained true to its founding values, designing programmes and offerings to solve for
						problems that have not yet occured, and build capabilities in areas that are nascent or yet to emerge.
					</p>
				</li>
				
				<li>
					<span>20.4s</span>
					<p>
						Today, TMTC offers more than 150 programmes every year, across an ever-expanding range of subjects. Online,
						Tata Tomorrow University makes learning accessible to all Tata employees, across levels, and around the
						world. It clocked 220,000 (two hundred and twenty thousand) learning hours in 2022 alone.
					</p>
				</li>
				
				<li>
					<span>25.6s</span>
					<p>
						Every year, we help hundreds of leaders across group companies find the best versions of themselves,
						re-evaluate their roles through different lenses, pick up new skill sets and engage with fresh thought.
					</p>
				</li>
				
				<li>
					<span>30s</span>
					<p>
						We accomplish this with our focus on immersive learning, capability building and partnerships with
						best-in-class global institutions and experts.
					</p>
				</li>
				
				<li>
					<span>35s</span>
					<p>
						I urge you to spend some time on our campus and on Tata Tomorrow University. Whether it is leadership
						skills, coaching, inclusion, digital acumen, storytelling or networking, I am sure there is something on
						offer for each one of us.
					</p>
				</li>
				
				<li>
					<span>38s</span>
					<p>
						Warmly,<br />
						Anand Shankar <br />
						Director, Tata Management Training Centre <br />
						April 2023
					</p>
				</li>
			</ul>
		</div>
	</div>
	
	<button class="btn-toggle-transcript" style="background: #b9a25a" data-toggle="collapse" href="#transcript-content">
		<span>View Transcript</span>
	</button>
</div>

<?php include "components/footer.php" ?>
