<?php include "components/header.php" ?>
<section class="section-ltc-tabs">
    <header class="section-header text-center">Living the Code</header>
    <nav class="ltc-pages-navigation">
        <div>
            <a href="best-practices-sharing.php">Best Practices Sharing</a>
        </div>
        <div>
            <a href="model-policies-procedures.php">Model Policies & Procedures</a>
        </div>
        <div>
            <a href="annual-compliance-reporting.php" class="active">Annual Compliance Reporting & Maturity Assessment</a>
        </div>
        <div>
            <a href="business-ethics-framework.php">Leadership of Business Ethics Framework</a>
        </div>
        <div>
            <a href="ethics-survey.php">Ethics Survey</a>
        </div>
        <div>
            <a href="training-and-capability-building.php">Training & Capability Building</a>
        </div>
    </nav>

    <div class="ltc-tabs-content white-element">
    <img src="img/backgrounds/blue-d.svg" class="ltc-best-practices-element acr">
    <img src="img/backgrounds/bars-white.svg" class="ltc-tabs-bars" data-aos="fade-in">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-6 mx-auto">
                    <header class="section-header section-header-sm text-light">Annual Compliance Reporting & Maturity Assessment</header>
                    <p>Annual Compliance Reporting (ACR) process entails assessment of activities undertaken by Tata companies for the 4 pillars of Leadership of Business Ethics Framework i.e. Leadership, Compliance Structure, Communication & Training and Measurement of Effectiveness. Major Tata companies participate every year since its inception in 2014.</p>
                    <p>The online ACR questionnaire seek information about the status of business ethics actions during the year. Based on the assessment, feedback is provided to every company to help in furthering business ethics related activities and processes. Companies are rated on 4 level of maturity ratings (Basic, Evolving, Established and Advanced) for each of the 4 pillars of Leadership of Business Ethics Framework.</p>
                    <br>
                </div>
                <div class="col-md-10 col-lg-6 mx-auto">
                    <div class="annual-compliance-wrapper">
                        <ul class="green-connecting-list has-icon">
                            <li>
                                <img src="img/icons/icon-green-basic.svg" alt="" height="65" width="66">
                                <h4 class="gcl-header">Basic</h4>
                                <p>Basic Inconsistent application of the compliance programme</p>
                            </li>

                            <li>
                                <img src="img/icons/icon-green-evolve.svg" alt="" height="65" width="66">
                                <h4 class="gcl-header">Evolving</h4>
                               <p>Application of a few parameters of the compliance programme</p>
                            </li>

                            <li>
                                <img src="img/icons/icon-green-established.svg" alt="" height="65" width="66">
                                <h4 class="gcl-header">Established</h4>
                               <p>Application of many parameters of the compliance programme</p>
                            </li>

                            <li>
                                <img src="img/icons/icon-green-advanced.svg"  height="65" width="66" alt="">
                                <h4 class="gcl-header">Advanced</h4>
                                <p>Consistent application of most parameters of the compliance programme</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php include "components/footer.php" ?>