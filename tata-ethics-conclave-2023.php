<?php include "components/header.php" ?>

<br class="d-md-none">
<header class="section-header global-header-padding text-center m-0">Tata Ethics Conclave</header>
<ul class="page-navigation-list my-3">
    <li>
        <a href="tata-ethics-conclave-2018">2018</a>

    </li>
    <li>
        <a href="tata-ethics-conclave-2021">2021</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2022">2022</a>
    </li>
    <li>
        <a href="tata-ethics-conclave-2023">
            <strong>2023</strong>
        </a>
    </li>
</ul>

<section class="section-ethics-conclave">
<img src="img/backgrounds/blue-d.svg" class="tec-d-1">
<img src="img/backgrounds/bars-white.svg" class="tec-bars" data-aos="fade-in">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center align-self-center mb-4">
                <img src="img/backgrounds/tec-2023.png" class="w-75 mx-auto" alt="">
            </div>
            <div class="col-lg-6 align-self-center">
                <p>The Tata Ethics Conclave 2023 was held on 14th February 2023 at Taj President, Mumbai and the theme was ‘Ethics in the era of emerging technologies and digitalization’. The conclave was addressed by Tata Group Chairman, Tata Group Chief Ethics Officer and notable speakers from Tata Group, Industry Leaders, and Thought Leaders.
                    Recognitions were bestowed to 12 Ethics Community members for their contributions towards driving Leadership of Business Ethics framework along with the winning and 3 finalist teams of the 2nd edition of Ethics in Action Case Study Competition. The conclave was attended in-person by senior Tata leaders and 160+ Tata Group Ethics Community members, additionally 250+ participants joined virtually.</p>
            </div>
        </div>
    </div>
</section>


<section class="section-graphical-scribe single-line-header">

    <div class="container-sm" style="max-width:1100px;">
    <img src="img/backgrounds/ethics-conclave-bars.svg" class="gs-bars" data-aos="fade-in">
        <header class="section-header text-center">Recording of the sessions</header>


        <div class="row">
            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-1.png" class="img-fluid">
                    </a>
                    <strong>Group Chairman’s Address - N. Chandrasekaran, Chairman, Tata Sons Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-2.png" class="img-fluid">
                    </a>
                    <strong>S. Padmanabhan, Update on Ethics Journey - 
S Padmanabhan, Group Chief Ethics Officer,Tata Sons Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-3.png" class="img-fluid">
                    </a>
                    <strong>Digital Ethics and Trustworthy Artificial Intelligence -Vishal Jain, Partner, Risk Advisory, Deloitte India</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-4.png" class="img-fluid">
                    </a>
                    <strong>Owning responsibility and ethics for new technologies-Sandeep Alur, Director, Microsoft Technology Center</strong>
                </div>
            </div>


            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-5.png" class="img-fluid">
                    </a>
                    <strong>Panel Discussion 1 - Combating digital misinformation: Reimagining social media and digital strategies.
Moderator:  Sreelakshmi Hariharan, Business Head - Diagnostics & Head - Brand Marketing, Tata 1mg <br>
Panel members: <br>
- Poornima Sampath, Senior Vice President and Chief Legal Officer, Tata Digital <br>
- Abonty Banerjee, Chief Marketing and Digital Officer, Tata Capital <br>
- Deepa Krishnan, Director - Marketing, Category, Loyalty and Digital, Tata Starbucks Private Limited</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-6.png" class="img-fluid">
                    </a>
                    <strong>Symposium session - Managing data privacy risks for value creation <br>
                        - Arun Prabhu, Partner (Head - TMT), Cyril Amarchand Mangaldas <br>
                        - Delizia Diaz, Group Privacy Compliance Director, Jaguar Land Rover</strong>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="tec-card">
                    <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" data-fancybox="" class="video-card">
                        <img src="img/backgrounds/rs-7.png" class="img-fluid">
                    </a>
                    <strong>Panel Discussion 2 - Leveraging digital to drive ethical culture in the organisation
                        Moderator: Jagdeep Singh, Partner, EY
                        <br> Panel members: <br>
                        - Prabha Thomas, Chief Risk and Compliance Officer, Tata Consultancy Services Limited <br>
                        - Vinod Bhat, Chief Information Officer and Chief Ethics Counsellor, Tata SIA Airlines Limited <br>
                        - Sudhansu Misra, Chief Human Resource Officer, Infiniti Retail Limited</strong>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>