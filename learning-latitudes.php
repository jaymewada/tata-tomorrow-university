<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/learning-latitude-banner.png" alt="" />
</section>

<div class="banner-shadow-content text-center">
	TMTC brings you a multi-disciplinary webinar series designed in collaboration with thought leaders and domain experts.
	These are carefully curated sessions that offer powerful insights on themes that are critical to business success and
	personal development.
</div>


<section class="section-webinar-listing">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-lg-12 mx-auto">
				<!-- ALL AVAILABLE FILTERS -->
				<div class="ll-filters-wrapper">
					<button class="is-active">Upcoming Webinars</button>

					<span>|</span>

					<button>Past Webinars</button>

					<span class="d-none d-lg-inline">|</span>

					<div class="dropdown custom-dropdown">
						<button class="dropdown-toggle w-100" data-toggle="dropdown" aria-expanded="false">Browse by category</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">ALL</a>
							<a class="dropdown-item is-active" href="#">Analytics</a>
							<a class="dropdown-item" href="#">Business Environment</a>
							<a class="dropdown-item" href="#">CEO Live</a>
							<a class="dropdown-item" href="#">Commercial Acumen</a>
							<a class="dropdown-item" href="#">Communication</a>
							<a class="dropdown-item" href="#">Customer Centricity</a>
							<a class="dropdown-item" href="#">CXO Live</a>
							<a class="dropdown-item" href="#">Digital Transformation</a>
							<a class="dropdown-item" href="#">Diversity & Inclusion</a>
							<a class="dropdown-item" href="#">Employee Engagement</a>
							<a class="dropdown-item" href="#">Environment & Ecology</a>
						</div>
					</div>

					<span>|</span>

					<div class="dropdown custom-dropdown">
						<button class="dropdown-toggle w-100" data-toggle="dropdown" aria-expanded="false">Browse by Time</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">Week</a>
							<a class="dropdown-item" href="#">Month</a>
							<a class="dropdown-item is-active" href="#">Year</a>
						</div>
					</div>
				</div>


				<!-- FILTERS APPLIED  -->
				<div class="filter-applied-wrapper">
					<label class="mb-0 fs-14">Filters Applied: </label>
					<button class="btn-remove-filter">
						Upcoming Webinars
						<span>&times;</span>
					</button>

					<button class="btn-remove-filter">
						Analytics
						<span>&times;</span>
					</button>
				</div>
				<!-- FILTERS APPLIED  -->
				<div class="filter-applied-wrapper mt-n4">
					<div class="dropdown custom-dropdown custom-dropdown-white global-card-box-shadow">
						<button class="dropdown-toggle d-flex align-items-center justify-content-between" data-toggle="dropdown" aria-expanded="false" style="min-width:150px;">2022</button>
						<div class="dropdown-menu">
							<a class="dropdown-item is-active" href="#">2023</a>
							<a class="dropdown-item" href="#">2022</a>
							<a class="dropdown-item" href="#">2021</a>
						</div>
					</div>
				</div>

				<!-- WEBINAR LISTING -->
				<div class="accordion webinar-listing-wrapper" id="webinar-parent-wrapper">
					<img src="img/backgrounds/ll-element-1.svg" class="ll-element-1" data-aos="fade-in" />
					<img src="img/backgrounds/bars-blue.svg" class="ll-element-2" data-aos="fade-in" />

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-1" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">The Trouble with “Don’t Worry, Be Happy”</h5>

									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Maj. Deepshika Gupta - Senior Counsellor, 1 to 1 help.net</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-1" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-2" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">Cultural Intelligence - Your Key to Success in a Globalised World</h5>
									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Jugal Choudhary - Consultant | Coach | Adjunct faculty (IIM-A, IIM-B, IIM-U & Others)</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-2" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-9" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">The Trouble with “Don’t Worry, Be Happy”</h5>

									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Maj. Deepshika Gupta - Senior Counsellor, 1 to 1 help.net</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-9" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-8" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">Cultural Intelligence - Your Key to Success in a Globalised World</h5>
									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Jugal Choudhary - Consultant | Coach | Adjunct faculty (IIM-A, IIM-B, IIM-U & Others)</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-8" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-3" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">The Trouble with “Don’t Worry, Be Happy”</h5>
									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Maj. Deepshika Gupta - Senior Counsellor, 1 to 1 help.net</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-3" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>

					<div class="webinar-accordian">
						<div class="webinar-date">27 Jan <br />2023</div>
						<div class="webinar-card-content">
							<a aria-expanded="false" data-toggle="collapse" data-target="#webinar-4" class="webinar-card-accordian-trigger">
								<div>
									<h5 class="wc-title">Cultural Intelligence - Your Key to Success in a Globalised World</h5>
									<table>
										<tr>
											<td>
												<strong>Speaker</strong>
											</td>
											<td>
												<span>Jugal Choudhary - Consultant | Coach | Adjunct faculty (IIM-A, IIM-B, IIM-U & Others)</span>
											</td>
										</tr>
									</table>
								</div>
							</a>

							<div id="webinar-4" class="collapse" aria-labelledby="headingOne" data-parent="#webinar-parent-wrapper">
								<div class="webinar-card-accordian-content">
									<table>
										<tr>
											<td>
												<strong>Category</strong>
											</td>
											<td>
												<span>Leadership</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event URL</strong>
											</td>
											<td>
												<span>https:/tata.zoom.us/webiar/register/WN_8cLNIPy-QD2T_kY1QNJNg</span>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td>
												<strong>Event Password</strong>
											</td>
											<td><span>1234</span></td>
										</tr>
									</table>

									<a href="">View Recording</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>