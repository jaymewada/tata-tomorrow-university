<?php include "components/header.php" ?>

<div class="section-tbeg-tabs">
    <header class="section-header text-center">Tata Business Excellence Group</header>
    
    <nav class="tbeg-pages-navigation" style="grid-template-columns:auto auto auto auto auto;">
        <div>
            <a href="business-excellence.php">Business Excellence</a>
        </div>
        <div>
            <a href="cyber-excellence.php">Cyber Excellence</a>
        </div>
        
        <div>
            <a href="data-excellence.php" class="active">Data Excellence</a>
        </div>
        <div>
            <a href="safety-excellence.php">Safety Excellence</a>
        </div>
        <div>
            <a href="social-excellence.php">Social Excellence</a>
        </div>
    </nav>
    
    <div class="tbeg-tabs-content-wrapper">
        <img src="img/backgrounds/bars-white-sm.svg" class="tbeg-self-paced-element" data-aos="fade-in">
        <div class="container">
            <header class="section-header section-header-sm">Self-Paced Modules</header>
            <div class="row mx-auto justify-content-center" style="max-width:675px;">
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Data Excellence Web Module</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-tbeg-classroom">
    <img src="img/backgrounds/tbeg-classroom-element-1.svg" class="tbeg-classroom-element-1" data-aos="fade-in">
    <img src="img/backgrounds/tbeg-classroom-element-2.svg" class="tbeg-classroom-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header section-header-sm">Classroom Programmes</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Data Excellence Assessor Programme</h6>
                    <p>Data Excellence is geared towards continuously driving the ecosystem to support data-driven decision-making and helping Tata companies to become more agile and future ready. The capability building programmes are aimed at preparing business users and potential Assessors to understand the core elements of Data Excellence and how effective data management principles and practices can drive business impact. </p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Data Excellence Champion Programme</h6>
                    <p>Designed for senior and middle management across levels and functions who are business users of organisational data, to leverage the TCS DATOM™ framework to enhance the Data Excellence journey in one’s own organisation.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-programme">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-assesor-programme.svg" height="70" alt="">
                    <div>
                        <strong>Assesor Programme</strong>
                        <p>Enables participants to become a Certified Assessor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-champion-programme.svg" height="70" alt="">
                    <div>
                        <strong>Champion Programme</strong>
                        <p>Enables understanding and application of the model in own organisation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-contact" id="contact-us">
    <img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <strong class="text-navy-2 fw-500">Remya Mudliar</strong>
                        <div class="text-muted my-10">remya.mudliar@tata.com</div>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>