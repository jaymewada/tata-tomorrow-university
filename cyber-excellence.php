<?php include "components/header.php" ?>

<div class="section-tbeg-tabs">
    <header class="section-header text-center">Tata Business Excellence Group</header>
    
    <nav class="tbeg-pages-navigation" style="grid-template-columns:auto auto auto auto auto;">
        <div>
            <a href="business-excellence.php">Business Excellence</a>
        </div>
        <div>
            <a href="cyber-excellence.php" class="active">Cyber Excellence</a>
        </div>
        
        <div>
            <a href="data-excellence.php">Data Excellence</a>
        </div>
        <div>
            <a href="safety-excellence.php">Safety Excellence</a>
        </div>
        <div>
            <a href="social-excellence.php">Social Excellence</a>
        </div>
    </nav>
</div>

<section class="section-tbeg-classroom">
    <img src="img/backgrounds/tbeg-classroom-element-1.svg" class="tbeg-classroom-element-1" data-aos="fade-in">
    <img src="img/backgrounds/tbeg-classroom-element-2.svg" class="tbeg-classroom-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header section-header-sm">Classroom Programmes</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Cyber Excellence Assessor Programme</h6>
                    <p>Designed for senior and middle management executives to understand Tata Cyber Excellence Framework and become Cyber Excellence Assessors, thereby helping group companies with Cyber Excellence assessment and build a roadmap to reach a higher level of excellence.</p>
                    <img src="img/icons/tbeg-assesor-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Cyber Excellence Champion Programme</h6>
                    <p>Designed for Senior and Middle Management within the Information Technology function to leverage the Tata Cyber Excellence Framework to enhance the Cyber Excellence journey in one’s organisation.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-programme">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-assesor-programme.svg" height="70" alt="">
                    <div>
                        <strong>Assesor Programme</strong>
                        <p>Enables participants to become a Certified Assessor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-champion-programme.svg" height="70" alt="">
                    <div>
                        <strong>Champion Programme</strong>
                        <p>Enables understanding and application of the model in own organisation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-contact" id="contact-us">
    <img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <strong class="text-navy-2 fw-500">Sushant Mallik</strong>
                        <div class="text-muted my-10">sushant.mallik@tata.com</div>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>