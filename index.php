<?php include "components/header.php" ?>

<section class="section-hero">
	<img src="img/backgrounds/hero-element-1.svg" class="hero-bar-element" />
	<div class="hero-content-wrapper">
		<div class="hero-center-content">
			<header class="section-header">
				Tata Tomorrow <br />University is the <br class="d-md-none" />
				digital <br class="d-none d-sm-block" />
				arm of Tata Management <br />
				Training Centre
			</header>

			<div class="hero-content-cta">
				<a href="" class="btn-cta-navy">Learn Now</a>
				<a href="" class="btn-cta-navy">Learn at TMTC</a>
			</div>
		</div>
	</div>

	<a href="#trending" class="scroll-down-link hash-link">
		<img src="img/backgrounds/scroll-down.svg" class="scroll-down" />
	</a>

	<a href="" class="hero-learn-now">
		<img src="img/backgrounds/learn-now-vertical.svg" class="" alt="" />
	</a>
</section>

<section class="section-events">
	<img
		src="img/backgrounds/upcoming-event-element-2.svg"
		class="upcoming-event-element-2 duration-1s"
		data-aos="fade-up"
		data-aos-duration="5000"
	/>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-lg-4">
				<div class="section-header-wrapper text-light">
					<header class="section-header text-light">
						Upcoming <br />
						Programmes
					</header>
					<p class="home-section-description">TMTC programmes scheduled to start soon.</p>
				</div>
				<img src="img/backgrounds/upcoming-event-element-1.svg" class="upcoming-event-element-1" data-aos="fade-in" />
			</div>

			<div class="col-md-9 col-lg-8">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="maroon">
							<time>10<sup>th</sup> & 11<sup>th</sup> noV 2022</time>
							<h4 class="events-card-header">Tapping Unstructured Data for Insights</h4>
							<span>See More</span>
						</a>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="purple">
							<time>12<sup>th</sup> & 13<sup>th</sup> Nov 2022</time>
							<h4 class="events-card-header">HR Pathfinder</h4>
							<span>See More</span>
						</a>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="green">
							<time>14<sup>th</sup> & 16<sup>th</sup> Nov 2022</time>
							<h4 class="events-card-header">Masterclass on Ethics</h4>
							<span>See More</span>
						</a>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="maroon">
							<time>17<sup>th</sup> & 18<sup>th</sup> Nov 2022</time>
							<h4 class="events-card-header">Financial Accumen for Leaders</h4>
							<span>See More</span>
						</a>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="maroon">
							<time>8<sup>th</sup> & 9<sup>th</sup> Nov 2022</time>
							<h4 class="events-card-header">Communicating for Impact</h4>
							<span>See More</span>
						</a>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-4 mb-sm-30 mb-40">
						<a class="events-card has-cta-link" border="green-2">
							<time>10<sup>th</sup> & 11<sup>th</sup> Nov 2022</time>
							<h4 class="events-card-header">Coachworks Pro</h4>
							<span>See More</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-trending" id="trending">
	<img src="img/backgrounds/yellow-d.svg" class="trending-now-slider-element-1" />
	<img
		src="img/backgrounds/trending-now-slider-element-2.svg"
		class="trending-now-slider-element-2 duration-1s"
		data-aos="fade-up"
	/>
	<div class="container-fluid">
		<div class="section-header-wrapper mb-3">
			<header class="section-header">Trending</header>
			<p class="home-section-description">Popular and recent offerings.</p>
		</div>

		<div class="home-trending-slider dots-dashed">
			<div>
				<div class="home-trending-slide">
					<div class="row">
						<div class="col-md-8 col-lg-7 align-self-center">
							<div class="trending-slide-image">
								<img src="img/cards/trending-now-slide-1.png" class="img-fluid" alt="" />
							</div>
						</div>
						<div class="col-md-4 col-lg-5 align-self-center">
							<div class="trending-slide-content">
								<p>
									This programme will help you to operate more effectively in the now–virtual world of sales and
									customer management. You will learn to leverage the online medium.
								</p>
								<!-- <time>14th & 16th Nov 2022</time> -->
								<a href="" style="width: 200px" class="btn-cta-navy">See More</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div>
				<div class="home-trending-slide">
					<div class="row">
						<div class="col-md-8 col-lg-7 align-self-center">
							<div class="trending-slide-image">
								<img src="img/cards/trending-now-slide-2.png" class="img-fluid" alt="" />
							</div>
						</div>
						<div class="col-md-4 col-lg-5 align-self-center">
							<div class="trending-slide-content">
								<p>
									This programme will help you to operate more effectively in the now–virtual world of sales and
									customer management. You will learn to leverage the online medium.
								</p>
								<!-- <time>14th & 16th Nov 2022</time> -->
								<a href="" style="width: 200px" class="btn-cta-navy">See More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-learn-now">
	<div class="container-fluid">
		<div class="section-header-wrapper">
			<header class="section-header">Learn Now</header>
			<p class="home-section-description">Learning resources you can explore in your own time, at your own pace.</p>
		</div>

		<div class="single-learn-now-wrapper">
			<div class="row">
				<div class="col-md-2">
					<span>Listen</span>
				</div>
				<div class="col-md-10">
					<h5 class="single-learn-now-header">Podcasts</h5>
					<div class="learn-now-slider home-webinar-slider">
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/podcast-slider-card-1.png" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/podcast-slider-card-2.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/podcast-slider-card-3.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/podcast-slider-card-4.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="single-learn-now-wrapper">
			<div class="row">
				<div class="col-md-2">
					<span>Watch</span>
				</div>
				<div class="col-md-10">
					<h5 class="single-learn-now-header">Webinars & Courses</h5>
					<div class="learn-now-slider home-webinar-slider">
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/webinar-slider-card-1.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/webinar-slider-card-2.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/webinar-slider-card-3.png" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/webinar-slider-card-4.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="single-learn-now-wrapper">
			<div class="row">
				<div class="col-md-2">
					<span>Interact</span>
				</div>
				<div class="col-md-10">
					<h5 class="single-learn-now-header">Journeys</h5>
					<div class="learn-now-slider home-webinar-slider">
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-1.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-2.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-3.png" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-4.svg" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-5.png" width="350" />
									</div>
								</a>
							</div>
						</div>
						<div>
							<div class="px-3">
								<a href="" class="">
									<div class="webinar-card-image">
										<img src="img/cards/journeys-slider-card-6.png" width="350" />
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-tmtc">
	<div class="section-header-wrapper text-center">
		<header class="section-header">
			Learn at Tata <br class="d-lg-none" />
			Management Training Centre
		</header>

		<p class="home-section-description global-content-width-550 px-3">
			Executive learning and development programmes open to Tata employees at different levels through classroom,
			online, and hybrid modes.
		</p>
	</div>

	<div class="tmtc-inner-section pt-0">
		<div class="container">
			<div class="section-header-wrapper text-center">
				<h6 class="section-header section-header-sm">Open Programmes</h6>
				<p class="home-section-description global-content-width-550">
					Structured learning courses on areas of critical importance attended by cohorts from different Tata companies.
				</p>
			</div>
			<div class="row">
				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#A3AA48">Commercial Acumen</span>
						</div>
						<div class="tmtc-card-banner" style="background: #0f2a4c">
							<img src="img/cards/card-banner-ca.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#4C914D">Communication</span>
						</div>
						<div class="tmtc-card-banner" style="background: #b74615">
							<img src="img/cards/card-banner-communication.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#B74615">Digital Acumen</span>
						</div>
						<div class="tmtc-card-banner" style="background: #01064e">
							<img src="img/cards/card-banner-da.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#FFAE00">Diversity, Equity, & Inclusion</span>
						</div>
						<div class="tmtc-card-banner" style="background: #ffae00">
							<img src="img/cards/card-banner-dei.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#388554">Ethics & Governance</span>
						</div>
						<div class="tmtc-card-banner" style="background: #388554">
							<img src="img/cards/card-banner-eg.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#8C408B">Human Capital Management</span>
						</div>
						<div class="tmtc-card-banner" style="background: #faf3e9">
							<img src="img/cards/card-banner-hcm.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#F7C06E">Innovation</span>
						</div>
						<div class="tmtc-card-banner" style="background: #2f5e8c">
							<img src="img/cards/card-banner-inv.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#E0A784">Leading Self & others</span>
						</div>
						<div class="tmtc-card-banner" style="background: #e0a784">
							<img src="img/cards/card-banner-lso.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#BF572D">Marketing, Sales, Customers</span>
						</div>
						<div class="tmtc-card-banner" style="background: #f7c869">
							<img src="img/cards/card-banner-msc.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#A88D42">Strategy</span>
						</div>
						<div class="tmtc-card-banner" style="background: #a88d42">
							<img src="img/cards/card-banner-stg.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#8C9720">Sustainability</span>
						</div>
						<div class="tmtc-card-banner" style="background: #8c9720">
							<img src="img/cards/card-banner-stb.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#F7C06E">Supply Chain Management</span>
						</div>
						<div class="tmtc-card-banner" style="background: #a24a3e">
							<img src="img/cards/card-banner-scm.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="tmtc-inner-section flagshi-ins">
		<div class="container position-relative">
			<div class="section-header-wrapper text-center">
				<h6 class="section-header section-header-sm text-center">
					Tata Flagship Leadership <br class="d-lg-none" />
					Development Programmes
				</h6>
				<img src="img/backgrounds/leadership-element-1.svg" class="leadership-element-1" data-aos="fade-in" />
				<div class="row">
					<div class="col-6 col-md-4 col-lg-3">
						<a href="" class="tmtc-card">
							<div class="tmtc-card-title">
								<span arrow-color="#183474">Tata Group Strategic Leadership Seminar</span>
							</div>
							<div class="tmtc-card-banner" style="background: #183474">
								<img src="img/cards/card-banner-sls.svg" class="img-fluid" />
							</div>
						</a>
					</div>

					<div class="col-6 col-md-4 col-lg-3">
						<a href="" class="tmtc-card">
							<div class="tmtc-card-title">
								<span arrow-color="#385598">Tata Group Executive Leadership Seminar</span>
							</div>
							<div class="tmtc-card-banner" style="background: #385598">
								<img src="img/cards/card-banner-els.svg" class="img-fluid" alt="" />
							</div>
						</a>
					</div>

					<div class="col-6 col-md-4 col-lg-3">
						<a href="" class="tmtc-card">
							<div class="tmtc-card-title">
								<span arrow-color="#386C93">Tata Group eMerging Leadership Seminar</span>
							</div>
							<div class="tmtc-card-banner" style="background: #386c93">
								<img src="img/cards/card-banner-emls.svg" class="img-fluid" alt="" />
							</div>
						</a>
					</div>

					<div class="col-6 col-md-4 col-lg-3">
						<a href="" class="tmtc-card">
							<div class="tmtc-card-title">
								<span arrow-color="#61A6AA">Blue Mint</span>
							</div>
							<div class="tmtc-card-banner" style="background: #61a6aa">
								<img src="img/cards/card-banner-bm.svg" class="img-fluid" alt="" />
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tmtc-inner-section flagshi-ins">
		<div class="container position-relative">
			<div class="section-header-wrapper text-center">
				<h6 class="section-header section-header-sm text-center">Custom Solutions</h6>
				<p class="home-section-description global-content-width-550">
					Learning and development programmes specifically tailored to specific needs of individual Tata companies.
				</p>
			</div>

			<img src="img/backgrounds/custom-solutions-element-1.svg" class="custom-solutions-element-1" data-aos="fade-in" />

			<div class="row">
				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#1058B2">Custom Programmes</span>
						</div>
						<div class="tmtc-card-banner" style="background: #1058b2">
							<img src="img/cards/card-banner-cp.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#73361B">Assessment & Development Centre</span>
						</div>
						<div class="tmtc-card-banner" style="background: #73361b">
							<img src="img/cards/card-banner-ac-dc.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#365A58">Tata Induction</span>
						</div>
						<div class="tmtc-card-banner" style="background: #365a58">
							<img src="img/cards/card-banner-ti.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>

				<div class="col-6 col-md-4 col-lg-3">
					<a href="" class="tmtc-card">
						<div class="tmtc-card-title">
							<span arrow-color="#B7A873">Coachworks</span>
						</div>
						<div class="tmtc-card-banner" style="background: #32475c">
							<img src="img/cards/card-banner-cw.svg" class="img-fluid" alt="" />
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-cta-slider">
	<div class="cta-small-slider">
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-1.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-2.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-3.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-4.png" alt="" />
			</div>
		</div>
	</div>

	<div class="cta-slider-wrapper">
		<div class="cta-center-slider">
			<div>
				<div class="cta-center-img">
					<img src="img/backgrounds/cta-center-slide-1.png" alt="" />
				</div>
			</div>
			<div>
				<div class="cta-center-img">
					<img src="img/backgrounds/cta-center-slide-2.png" alt="" />
				</div>
			</div>
			<div>
				<div class="cta-center-img">
					<img src="img/backgrounds/cta-center-slide-3.png" alt="" />
				</div>
			</div>
			<div>
				<div class="cta-center-img">
					<img src="img/backgrounds/cta-center-slide-4.png" alt="" />
				</div>
			</div>
			<div>
				<div class="cta-center-img">
					<img src="img/backgrounds/cta-center-slide-5.png" alt="" />
				</div>
			</div>
		</div>

		<div class="cta-slider-center-content">
			<header class="section-header">
				A unique community of <br />
				learners & educators
			</header>
			<p class="section-description text-center">
				We believe that it is essential to nurture the “Tata Way” of leadership. We offer thought leadership and
				therefore a focus on emergent business themes and future-ready competencies that will be vital to business
				sustainability.
			</p>
			<div class="cta-slider-buttons-wrapper">
				<a href="" class="btn-cta-navy">About Us</a>
				<a href="" class="btn-cta-navy d-flex align-items-center justify-content-center">
					<i class="material-symbols-outlined">download</i>&nbsp; Download Calendar
				</a>
				<a href="" class="btn-cta-navy">Resources</a>
			</div>
		</div>
	</div>

	<div class="cta-small-slider">
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-4.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-3.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-2.png" alt="" />
			</div>
		</div>
		<div>
			<div class="cta-small-slide">
				<img src="img/backgrounds/cta-slide-1.png" alt="" />
			</div>
		</div>
	</div>
</section>

<section class="section-learning-arenas">
	<div class="container">
		<img src="img/backgrounds/learning-arenas-element-1.svg" class="learning-arenas-element-1" data-aos="fade-in" />
		<img
			src="img/backgrounds/learning-arenas-element-2.svg"
			class="learning-arenas-element-2 duration-1s"
			data-aos="fade-up"
		/>

		<div class="section-header-wrapper text-center">
			<header class="section-header text-center">Learning Arenas</header>
			<p class="home-section-description global-content-width-550">
				Delve into the unique ‘Tata way’ of life and work.
			</p>
		</div>
		<div class="row">
			<div class="col-md-6 mb-40">
				<div class="learning-card">
					<div class="learning-card-image">
						<img src="img/backgrounds/living-the-code-logo.svg" alt="" />
					</div>
					<h4>Living the Code</h4>
					<p>
						Tata has always been a values-driven organisation. These values continue to direct the growth and business
						of Tata companies. Our purpose is to improve the quality of life of the communities we serve globally,
						through long-term stakeholder value creation based on ‘Leadership with Trust’. ​
					</p>
					<a href="living-the-code" class="btn-cta-navy">Learn More</a>
				</div>
			</div>

			<div class="col-md-6 mb-40">
				<div class="learning-card">
					<div class="learning-card-image">
						<img src="img/backgrounds/oneworld-logo.svg" alt="" />
					</div>
					<h4>ONEderful World</h4>
					<p>
						Diversity is what defines us at the Tata group, and it is reflected in our core values. ONEderful World is a
						powerful collection of 12 simple, impactful insights that highlight nuances of exclusion and inclusion in
						worlds that we live in.
					</p>
					<a href="onederful-world" class="btn-cta-navy">Learn More</a>
				</div>
			</div>

			<div class="col-md-6 mb-40">
				<div class="learning-card">
					<div class="learning-card-image">
						<img src="img/logo/tbeg-logo.svg" alt="" />
					</div>
					<h4>Tata Business Excellence Group</h4>
					<p>
						Tata Business Excellence Group (TBExG) is entrusted to build and nurture an institutionalized approach to
						drive the business excellence movement at the Tata group. It set standards of excellence and partner with
						group companies in their journey to achieve world class performance.
					</p>
					<a href="tbeg" class="btn-cta-navy">Learn More</a>
				</div>
			</div>

			<div class="col-md-6 mb-40">
				<div class="learning-card">
					<div class="learning-card-image">
						<img src="img/backgrounds/wellweing-logo.svg" alt="" />
					</div>
					<h4>Vitality</h4>
					<p>
						Our People Vitality is a proactive and futuristic approach to having energized and engaged teams and
						creating a workforce which is happy, hopeful, inspired, physically and mentally fit. It is about
						replenishing more than you deplete, it is always a state of surplus!
					</p>
					<a href="vitality" class="btn-cta-navy">Learn More</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-partners">
	<header class="section-header text-center">Our Partners from Leading Institutions</header>
	<br />
	<div class=" ">
		<div class="partners-logo-slider">
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-1.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-2.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-3.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-4.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-5.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-6.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-7.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-8.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-9.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-10.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-11.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-12.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-13.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-14.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-15.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-16.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-17.svg" alt="" />
				</div>
			</div>
			<div>
				<div class="px-3 px-lg-4">
					<img src="img/logo/partner-18.svg" alt="" />
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-site-contact home-site-contact">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-5">
				<div class="pr-md-5">
					<svg xmlns="http://www.w3.org/2000/svg" width="55.76" height="66" viewBox="0 0 55.76 66">
						<path
							id="Polygon_3"
							data-name="Polygon 3"
							d="M33,0,66,55.76H0Z"
							transform="translate(55.76) rotate(90)"
							fill="#efc07b"
						/>
					</svg>
					<header class="section-header">Write to us to know more</header>
					<p class="section-description">
						If you have queries about the programmes or would like to know how to nominate/get nominated, send us a
						message!
					</p>

					<img src="img/backgrounds/home-contact-element.svg" class="img-fluid pb-5" data-aos="fade-in" />
				</div>
			</div>
			<div class="col-lg-7">
				<form action="" class="site-contact-form">
					<div class="row">
						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Your Name" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Last Name" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Company" />
							</div>
						</div>

						<div class="col-md-6">
							<div class="d-flex w-100">
								<div class="digital-patterns-select-wrapper">
									<select name="" id="" class="digital-patterns-select" style="width: 80px">
										<option value="">+91</option>
										<option value="">+91</option>
										<option value="">+91</option>
									</select>
								</div>

								<div class="digital-patterns-input-wrapper ml-auto" style="width: calc(100% - 95px)">
									<input type="text" class="digital-patterns-input" placeholder="Phone Number" />
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="digital-patterns-input-wrapper">
								<input type="text" class="digital-patterns-input" placeholder="Email Address" />
							</div>
						</div>

						<div class="col-md-12">
							<div class="digital-patterns-textarea-wrapper">
								<textarea class="digital-patterns-textarea" rows="6">Message</textarea>
							</div>
						</div>

						<div class="col-md-12">
							<button class="btn-cta-gold box-shadow-none">Send Message</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>
