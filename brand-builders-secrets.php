<?php include "components/header.php" ?>
<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/brand-builders-secrets-banner.png" alt="" />
</section>

<div class="banner-shadow-content text-center">
	Secrets are cherished and coveted in equal measures. Some could destroy, while some could help build empires. In Brand
	Builders’ Secrets [formerly known as CXO Secrets], we bring you wisdom collected and distilled from celebrated
	thinkers and leaders from the business world. Hear from the best how you can turn your weaknesses into strengths and
	challenges into opportunities. Listen carefully as they unfold the secrets of their success. Who know, it may help you
	build your next breakout initiative!
</div>

<section class="section-episode-listing">
	<div class="container">
		<ul class="nav season-listing-navigation season-listing-purple">
			<li>
				<button class="active" id="season-1-tab" data-toggle="tab" data-target="#season-1" type="button">
					season 1
				</button>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane fade show active" id="season-1">
				<div class="row justify-content-center">
				<div class="col-12 col-sm-6 col-md-6 col-xl-4 mb-50">
						<div>
							<a href="brand-builders-episode.php" class="podcast-card podcast-card-purple">
								<div class="podcast-card-image">
									<img src="img/backgrounds/bb-trailer-card.png" class="img-fluid" alt="" />
								</div>
								<div class="podcast-card-content-container">
									<div class="podcast-card-header">
										<img src="img/backgrounds/brandbuilder-podcast-icon.svg" width="30" height="28" alt="" />
										<span class="se">S1: EP0</span>
									</div>

									<div class="podcast-card-content">
										<div>
											<!-- <h6 class="podcast-card-title">Anubhuti Banerjee</h6> -->
											<p class="podcast-card-description">Trailer for <br> Brand Builder’s Secrets</p>
										</div>
									</div>

									<div class="podcast-card-footer">
										<span>Listen Now</span>&nbsp;&nbsp;
										<svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
											<g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
												<g
													id="Ellipse_98"
													data-name="Ellipse 98"
													transform="translate(424 2476)"
													fill="none"
													stroke="#fff"
													stroke-width="1"
												>
													<circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
													<circle cx="9.5" cy="9.5" r="9" fill="none" />
												</g>
												<path
													id="Polygon_19"
													data-name="Polygon 19"
													d="M3.091,0,6.183,5.358H0Z"
													transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)"
													fill="#fff"
												/>
											</g>
										</svg>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php for ($x = 1; $x <= 11; $x++) { ?>
					<div class="col-12 col-sm-6 col-md-6 col-xl-4 mb-50">
						<div>
							<a href="brand-builders-episode.php" class="podcast-card podcast-card-purple">
								<div class="podcast-card-image">
									<img src="img/backgrounds/brand-builder-user.png" class="img-fluid" alt="" />
								</div>
								<div class="podcast-card-content-container">
									<div class="podcast-card-header">
										<img src="img/backgrounds/brandbuilder-podcast-icon.svg" width="30" height="28" alt="" />
										<span class="se">S1: EP<?php echo $x; ?></span>
									</div>

									<div class="podcast-card-content">
										<div>
											<h6 class="podcast-card-title">Anubhuti Banerjee</h6>
											<p class="podcast-card-description">Learning the ABC of LGBTQIA+</p>
										</div>
									</div>

									<div class="podcast-card-footer">
										<span>Listen Now</span>&nbsp;&nbsp;
										<svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
											<g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
												<g
													id="Ellipse_98"
													data-name="Ellipse 98"
													transform="translate(424 2476)"
													fill="none"
													stroke="#fff"
													stroke-width="1"
												>
													<circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
													<circle cx="9.5" cy="9.5" r="9" fill="none" />
												</g>
												<path
													id="Polygon_19"
													data-name="Polygon 19"
													d="M3.091,0,6.183,5.358H0Z"
													transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)"
													fill="#fff"
												/>
											</g>
										</svg>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include "components/footer.php" ?>
