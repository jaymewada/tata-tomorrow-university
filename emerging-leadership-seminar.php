<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/tata-emerging-banner.png" class="d-none d-lg-block" alt="">
    <img src="img/banners/tata-emerging-seminar-mobile-banner.png" class="d-block d-lg-none">
</section>

<div class="banner-shadow-content text-center">
    Part of the Tata group’s four-tier leadership development framework, the seminar is especially designed for high-potential employees transitioning into senior management roles. The programme helps participants understand and appreciate the inter-linkages and impact of various business functions on the overall strategy of the organization and enhances their people management capabilities. The seminar also provides participants opportunities to build a network with peers from group companies and learn from experiences shared by senior Tata leaders.
</div>

<section class="section-page-nav">
    <div class="container">
        <ul class="page-navigation-list els">
            <li>
                <a class="hash-link" href="#objectives">Objectives</a>
            </li>
            <li>
                <a class="hash-link" href="#aim">Aim</a>
            </li>
            <li>
                <a class="hash-link" href="#learner-profile">Learner Profile</a>
            </li>
            <li>
                <a class="hash-link" href="#key-outcomes">Key Outcomes</a>
            </li>
            <li>
                <a class="hash-link" href="#structure">Structure</a>
            </li>
            <li>
                <a class="hash-link" href="#faculty">Faculty</a>
            </li>
            <li>
                <a class="hash-link" href="#highlights">Highlights</a>
            </li>
            <li>
                <a class="hash-link" href="#gallery">Gallery</a>
            </li>
            <li>
                <a class="hash-link" href="#testimonials">Testimonials</a>
            </li>
            <li>
                <a class="hash-link" href="#team">Team</a>
            </li>
        </ul>
    </div>
</section>

<section class="section-els-objectives" id="objectives">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header mb-4">Objectives</header>
                <img src="img/backgrounds/bars-blue.svg" class="img-fluid mb-4" data-aos="fade-in">
            </div>
            <div class="col-md-9 col-lg-8">
                <p>In today’s rapidly changing business environment, organisations need leaders who are strategic thinkers, influencers, innovators, effective communicators, and growth drivers. As one prepares to take on a senior role in the organisation, an individual needs to be equipped with the functional, managerial and leadership skills required to succeed. With this focus, the seminar is designed to develop participants in the space of Strategic Thinking, Sustainability, Innovation, Transformational Leadership, Finance, Marketing, Behavioural Effectiveness, Digital Transformation, Operations, Executive Presence and Data and Analytics</p>

                <p>Moreover, the Action Learning Project (ALP) is one of the key aspects of this seminar, which allows participants to use various management concepts and frameworks and understand their application in real-world business situations.</p>
            </div>
        </div>
    </div>
</section>

<section class="section-els-aims" id="aim">

<img src="img/backgrounds/els-aim-bg.svg" class="els-aims-element-1 duration-1s" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pb-30">
                <header class="section-header ">Aim</header>
                <img src="img/backgrounds/emerging-leadership-aim-element.svg" class="img-fluid" data-aos="fade-in">
            </div>
            <div class="col-md-8 col-lg-6">
                <ul class="home-list">
                    <li>
                        <img src="img/icons/els-aim1.svg" alt="">
                        <p>Appreciate the impact of the macroeconomic environment on business. </p>
                    </li>

                    <li>
                        <img src="img/icons/els-aim2.svg" alt="">
                        <p>Acquire deeper knowledge of various functional levers that drive business performance and their interdependence.</p>
                    </li>

                    <li>
                        <img src="img/icons/els-aim3.svg" alt="">
                        <p>Learn how to analyse the business environment to create sources of competitive advantage. </p>
                    </li>

                    <li>
                        <img src="img/icons/els-aim4.svg" alt="">
                        <p>Understand how competitive advantage translates into strategy to be future ready. </p>
                    </li>

                    <li>
                        <img src="img/icons/els-aim5.svg" alt="">
                        <p>Build the skills required to transform your leadership style, manage talent, and communicate more effectively. </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-els-attend" id="learner-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header text-light">Who should <br class="d-none d-md-block"> attend?</header>
            </div>
            <div class="col-md-9 col-lg-8">

            <div class="global-content-width-650">
                <p>The seminar is best suited for managers with significant people responsibilities and those who are poised for broader general management roles. The seminar is best suited for managers with significant people responsibilities and those who are poised for broader general management roles. </p>
                <p>Note:
                    <br>
                    Participation in this seminar is by invitation only.
                </p>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="section-els-outcomes" id="key-outcomes">
    <div class="container">
        <header class="section-header">Key Outcomes</header>
        <div class="row">
            <div class="col-md-6">
                <ul class="dash-list">
                    <li>
                        <p>Participants gain knowledge on various business and functional levers that drive organisational performance.</p>
                    </li>
                    <li>
                        <p>They learn how to create efficiencies in businesses and business processes through live Action Learning Projects.</p>
                    </li>
                    <li>
                        <p>They explore their behavioural effectiveness and learn ways to hone their people and leadership skills.</p>
                    </li>

                </ul>
            </div>
            <div class="col-md-6">
                <ul class="dash-list">
                    <li>
                        <p>They learn how to facilitate complex communication using data to enable decisions within and across different stakeholder groups.</p>
                    </li>
                    <li>
                        <p>Participants are challenged to make realistic strategic design, marketing, and business decisions in a competitive, fast-paced market via business simulation game. This simulation helps them recognise the links between strategic choices pertaining to various functional levers.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section-els-learning-pedagogy" id="structure">
    <div class="container">
        <header class="section-header text-center">Structure</header>

        <div class="row">
            <div class="col-md-6 mb-40">
                <div class="shadow-card full-height mb-0 disable-hover-effect">
                    <div class="shadow-card-content ">
                        <div class="shadow-card-header mb-2">Methodology</div>
                        <p>The pedagogy is a blend of case analysis, faculty-led application-based class discussions, presentations, and interactions with industry experts and leaders. Participants will also have the opportunity to collaborate on group projects with team members from various Tata group entities. </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="shadow-card full-height mb-0 disable-hover-effect">
                    <div class="shadow-card-content ">
                        <div class="shadow-card-header mb-2">Topics</div>
                        <p>Key topics will include Strategic Orientation, Sustainability, Innovation, Transformational Leadership, Finance, Marketing, Behavioural Effectiveness, Digital Transformation, Operations, Executive Presence and Data and Analytics. </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 mb-40">
                <div class="shadow-card full-height mb-0 disable-hover-effect">
                    <div class="shadow-card-content ">
                        <div class="shadow-card-header mb-2">Sessions</div>
                        <p>A blended learning approach with one week of online and two weeks of in-person classes at the TMTC campus. The structure allows participants to understand various management concepts and frameworks and their application in real-world business situations. </p>
                    </div>
                </div>
            </div>

        </div>

        <img src="img/backgrounds/bars-blue.svg" class="section-background-bars" data-aos="fade-in">
    </div>
</section>

<section class="section-els-experts" id="faculty">
    <!-- <img src="img/backgrounds/els-expert-bg.svg" class="els-experts-element" alt=""> -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4">
                <header class="section-header">Faculty & <br class="d-none d-md-block"> Experts</header>
            </div>
            <div class="col-md-9 col-lg-8">
                
            <div class="global-content-width-650 bg-white p-3">
                <img src="img/backgrounds/els-expert.svg" class="img-fluid pb-4" alt="">

                <p>The seminar will feature faculty from Tata Management Training Centre and Carnegie Mellon University. </p>
                <p>Carnegie Mellon is a renowned global research university known for its innovation, diverse student and faculty bodies, and partnerships with leading companies such as Google and Disney. Carnegie Mellon’s faculty emphasises hands-on learning and solving major challenges in science, technology, and society. </p>
            </div>
            </div>
        </div>
    </div>
</section>

<section class="section-els-highlights" id="highlights">
    <img src="img/backgrounds/els-highlight.png" class="img-fluid d-none d-md-block" alt="">
    <img src="img/backgrounds/els-highlight-mobile.png" class="img-fluid d-md-none w-100" alt="">
</section>

<section class="section-els-gallery" id="gallery">
    <div class="container-fluid">

        <header class="section-header text-center mb-3">Gallery</header>
        <nav class="nav gallery-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-els-gallery-image-tab" data-toggle="tab" data-target="#nav-els-gallery-image" type="button" role="tab" aria-controls="nav-els-gallery-image" aria-selected="true">Images</button>
            <button class="nav-link" id="nav-els-gallery-video-tab" data-toggle="tab" data-target="#nav-els-gallery-video" type="button" role="tab" aria-controls="nav-els-gallery-video" aria-selected="false">Videos</button>
        </nav>

        <div class="tab-content gallery-tabs-content" id="nav-tabContent">
            <!-- IMAGE TAB CONTENT -->
            <div class="tab-pane fade show active" id="nav-els-gallery-image" role="tabpanel">
                <div class="gallery-image-slider dots-dashed">
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-1.png" class="img-fluid" alt="">
                    </div>
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-2.png" class="img-fluid" alt="">
                    </div>
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-3.png" class="img-fluid" alt="">
                    </div>
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-4.png" class="img-fluid" alt="">
                    </div>
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-5.png" class="img-fluid" alt="">
                    </div>
                    <div class="gallery-image-slide">
                        <img src="img/gallery/emerging-gallery-6.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <!-- VIDEO TAB CONTENT -->
            <div class="tab-pane fade" id="nav-els-gallery-video" role="tabpanel">
                <div class="gallery-image-slider dots-dashed">
                    <div>
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/tata-emerging-seminar-mobile-banner.png" class="img-fluid" alt="">
                        </a>
                    </div>

                    <div>
                        <a href="https://www.youtube.com/watch?v=rudVQof0Vjw" class="gallery-video-card" data-fancybox>
                            <img src="img/banners/tata-emerging-seminar-mobile-banner.png" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-els-tesimonial" id="testimonials">
    <div class="container">
        <header class="section-header text-center mb-3">Testimonials</header>

        <div class="testimonial-slider-wrapper">
            <img src="img/icons/quote-blue.svg" class="testimonial-slider-fixed-quote" alt="">
            <div class="testimonial-slider dots-dashed">
                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">Great event by great faculty members. Thanks to TMTC. Excellent initiative to broaden the thought process.</p>
                            <strong class="fw-500">- Voices of the 39th batch of TGeLS, February 2022</strong>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">Got insight on how to be a future-ready leader. Understanding the Balance Sheet was a good learning for me. The case studies were really great.</p>

                            <strong class="fw-500">- Voices of the 39th batch of TGeLS, February 2022</strong>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">The faculties were extremely knowledgeable and intensively involved with participants by showing high level of pertinence ad support. By encouraging interaction, they made even an intense session interesting. Heartfelt experience.</p>

                            <strong class="fw-500">- Voices of the 40th batch of TGeLS, June 2022</strong>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-slide">
                        <div class="testimonial-slide-content">
                            <p class="text-dark">The training programme was excellent, both in terms of design and actual delivery.</p>

                            <strong class="fw-500">- Voices of the 40th batch of TGeLS, June 2022</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-els-team" id="team">
    <div class="container">
        <header class="section-header text-center bg-white">Meet the Team</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team1.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Surabhi Dixit</h5>
                        <strong>Programme Director</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team2.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Shantanu Gokhale</h5>
                        <strong>Programme Director</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="vertical-card has-link mx-auto" style="max-width:calc(100% - 30px);">
                    <div class="verical-card-image">
                        <img src="img/backgrounds/bm-team3.png" class="img-fluid" alt="">
                    </div>
                    <div class="vertical-card-content text-center">
                        <h5 class="vertical-card-header">Prerna Koppikar</h5>
                        <strong>Programme Manager</strong>
                        <a href="">Write to us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>