<?php include "components/header.php" ?>

<div class="section-tbeg-tabs">
    <header class="section-header text-center">Tata Business Excellence Group</header>
    
    <nav class="tbeg-pages-navigation" style="grid-template-columns:auto auto auto auto auto;">
        <div>
            <a href="business-excellence.php">Business Excellence</a>
        </div>
        <div>
            <a href="cyber-excellence.php">Cyber Excellence</a>
        </div>
        
        <div>
            <a href="data-excellence.php">Data Excellence</a>
        </div>
        <div>
            <a href="safety-excellence.php" class="active">Safety Excellence</a>
        </div>
        <div>
            <a href="social-excellence.php">Social Excellence</a>
        </div>
    </nav>
    
    <div class="tbeg-tabs-content-wrapper">
        <img src="img/backgrounds/bars-white-sm.svg" class="tbeg-self-paced-element" data-aos="fade-in">
        <div class="container">
            <header class="section-header section-header-sm">Self-Paced Modules</header>
            <div class="row mx-auto justify-content-center" style="max-width:675px;">
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Awareness Modules</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
                
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Safety Shorts</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Practitioner Modules</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
                <div class="col-md-6 mb-30">
                    <a href="" class="module-card">
                        <h6>Customised Learning Journey</h6>
                        <span>Click here to Launch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-tbeg-classroom">
    <img src="img/backgrounds/tbeg-classroom-element-1.svg" class="tbeg-classroom-element-1" data-aos="fade-in">
    <img src="img/backgrounds/tbeg-classroom-element-2.svg" class="tbeg-classroom-element-2" data-aos="fade-in">
    <div class="container">
        <header class="section-header section-header-sm">Classroom Programmes</header>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>MD’s Safety Programme</h6>
                    <p>An immersive experiential programme designed for MD/CEOs. The programme enables strategising for step change in Safety performance and culture.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Safety Leadership Programme</h6>
                    <p>Designed for leadership teams in an organisation to set team and individual goals to enhance Safety in their respective areas of responsibility.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Managing Safety for Operational Managers</h6>
                    <p>Designed for the Site/ Factory / Branch heads to understand and appreciate the various Safety and Health operational controls that need to be institutionalised for Managing Safety.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Operationalising Safety Standards</h6>
                    <p>Designed for Functional heads to assist the implementation of the Tata Safety standards within the organisation.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Safety Heads & Managers’ Programme</h6>
                    <p>Exclusive programme for Safety Professionals for creating awareness on the changes in legislative requirements on Health and Safety.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-30">
                <div class="static-icon-card">
                    <h6>Certified Internal Examiner on Tata Safety & Health Management System</h6>
                    <p>A programme designed to build <br>a) Deeper understanding and 13 elements on the Tata Safety & Health Management System & <br>b) Explore examination skills for self-assessment.</p>
                    <img src="img/icons/tbeg-champion-programme.svg" class="s-icon" height="40" width="40" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-programme">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-assesor-programme.svg" height="70" alt="">
                    <div>
                        <strong>Assesor Programme</strong>
                        <p>Enables participants to become a Certified Assessor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-30">
                <div class="tbeg-horizonal-programme-card">
                    <img src="img/icons/tbeg-champion-programme.svg" height="70" alt="">
                    <div>
                        <strong>Champion Programme</strong>
                        <p>Enables understanding and application of the model in own organisation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-tbeg-contact" id="contact-us">
    <img src="img/backgrounds/programme-hightlights-bg.svg" class="tbeg-contact-element-1 duration-1s" data-aos="fade-right">
    <img src="img/backgrounds/learning-team-vertical-lines.svg" class="tbeg-contact-element-2 duration-1s" data-aos="fade-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <a href="" class="shadow-link-card" style="max-width:450px;">
                    <div class="shadow-link-content text-center">
                        <header class="section-header section-header-sm">Contact Us</header>
                        <strong class="text-navy-2 fw-500">Sanjay Rajasekaran</strong>
                        <div class="text-muted my-10">srajasekaran@tata.com</div>
                        <p class="text-navy-2 fw-500">www.tatabex.com</p>
                        <span>Write to us</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>