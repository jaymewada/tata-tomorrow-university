<?php include "components/header.php" ?>

<style></style>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/coachworks-banner.png" alt="">
</section>

<div class="banner-shadow-content text-center" style="color: #917e32">
	Coaching is a personal journey to enable leaders understand themselves better and to help them reach their fuller
	potential. Coachworks is an exclusive coaching program for leaders of the Tata Group delivered in partnership with
	top-ranked professional coaches.
</div>

<section class="section-coachworks-tabs">
	<ul class="nav nav-tabs" id="coachworks-tabs">
		<li>
			<button class="active" id="cw-executive-tab" data-toggle="tab" data-target="#cw-executive" aria-selected="true">
				<span> coach<i class="font-italic">works</i> executive </span>
			</button>
		</li>
		<li>
			<button id="cw-groups-tab" data-toggle="tab" data-target="#cw-groups" aria-selected="false">
				<span> coach<i class="font-italic">works</i> groups </span>
			</button>
		</li>
		<li>
			<button id="cs-pro-tab" data-toggle="tab" data-target="#cs-pro" aria-selected="false">
				<span> coach<i class="font-italic">works</i> pro </span>
			</button>
		</li>
		<li>
			<button id="cs-flip-tab" data-toggle="tab" data-target="#cs-flip" aria-selected="false">
				<span> coach<i class="font-italic">works</i> flip </span>
			</button>
		</li>
		<li>
			<button id="cs-snippet-tab" data-toggle="tab" data-target="#cs-snippet" aria-selected="false" style="background: #b7a873">
				<span> coach<i class="font-italic">works</i> snippet </span>
			</button>
		</li>
	</ul>

	<div class="tab-content" id="coachworks-tabsContent">
		<img src="img/backgrounds/yellow-rod-2.svg" class="cw-content-element" data-aos="fade-in" />
		<img src="img/backgrounds/coachworks-element-2.svg" class="cw-content-element-2" data-aos="fade-in"  />

		<!-- COACHWORKS EXECUTIVE CONTENT -->
		<div class="tab-pane fade show active has-bottom-border" id="cw-executive">
			<div class="row">
				<div class="col-md-8 d-flex flex-column justify-content-between">
					<div>
						<h6 class="coachwork-section-header-sm mb-1">For Leadership</h6>
						<h5 class="coachwork-section-header-lg">
							<span style="color:#7F8183;">coach</span><span class="font-italic">works</span>
							executive
						</h5>
						<p class="cw-tab-description">
							Offered as individual sessions, coachworks programme seeks to support senior leadership through a
							one-on-one custom learning journey. Many business leaders find themselves hitting the ceiling of what they
							can achieve. A coach provides necessary support to help a leader grow and manage changes.
						</p>
					</div>

					<br>

					<div>
						<h6 class="coachwork-section-header-sm">Ideal For:</h6>
						<p class="cw-tab-description">Leaders in significant transition/role changes + Very high potential leaders</p>
					</div>

					<br>
				</div>

				<div class="col-md-4 pt-md-5">
					<h6 class="coachwork-section-header-sm">Coaching Areas:</h6>
					<ul class="blue-dot-list" style="padding-left:30px;">
						<li class="my-3">Executive Presence</li>
						<li class="my-3">Interpersonal Skills</li>
						<li class="my-3">Growth Mindset</li>
						<li class="my-3">Communication Skills</li>
						<li class="my-3">Emotional Resilience & Conflict Management</li>
						<li class="my-3">Business Pitches & Dealing With Ambiguity</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- COACHWORKS GROUPS CONTENT -->
		<div class="tab-pane fade has-bottom-border" id="cw-groups">
			<div class="row">
				<div class="col-md-8">
					<div>
						<h6 class="coachwork-section-header-sm mb-1">For High Potential Teams</h6>
						<h5 class="coachwork-section-header-lg">
							<span style="color:#7F8183;">coach</span><span class="font-italic">works</span>
							<span style="color:#000;">groups</span>
						</h5>
						<p class="cw-tab-description">In group coaching, a coach has the ability to create the combined power of both social as well as self-awareness. Any team that needs to outshine, outperform and build more cohesiveness among themselves are an excellent starting point to Group coaching.</p>
					</div>

					<br>

					<div>
						<h6 class="coachwork-section-header-sm">Ideal For:</h6>
						<p class="cw-tab-description">Sales teams, senior leaders, cross functional or members of same team</p>
					</div>

					<br>
				</div>

				<div class="col-md-4 pt-md-5">
					<h6 class="coachwork-section-header-sm">Coaching Areas:</h6>
					<ul class="blue-dot-list" style="padding-left:30px;">
						<li class="my-3">Collaboration</li>
						<li class="my-3">Agility</li>
						<li class="my-3">Accountability</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- COACHWORKS PRO CONTENT -->
		<div class="tab-pane fade has-bottom-border" id="cs-pro">
			<div class="row">
				<div class="col-md-8">
					<div>
						<h6 class="coachwork-section-header-sm mb-1">For Leadership</h6>
						<h5 class="coachwork-section-header-lg">
							<span style="color:#7F8183;">coach</span><span class="font-italic">works</span>
							<span style="color:#000;">pro</span>
						</h5>
						<p class="cw-tab-description">ICF-approved coach certification programme exclusive for Tata leaders to build coaching effectiveness. Take one step closer to your ACC (Associate Coach Certificate) credentials with this high-impact, modular coach training. It will help you to develop as a coach through exposure to global standards and competencies of coaching, and thus acquire industry-wide credibility to coach professionals both within and outside your organisation.</p>
					</div>

					<br>

					<div>
						<h6 class="coachwork-section-header-sm">Ideal For:</h6>
						<p class="cw-tab-description">Sales teams, senior leaders, cross functional or members of same team</p>
					</div>

					<br>
				</div>

				<div class="col-md-4 pt-md-5">
					<h6 class="coachwork-section-header-sm">Learnings:</h6>
					<ul class="blue-dot-list" style="padding-left:30px;">
						<li class="my-3">Learn various Approaches to Coaching</li>
						<li class="my-3">Conduct high-performance Coaching Conversations</li>
						<li class="my-3">Inculcate a Coaching Culture</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- COACHWORKS FLIP CONTENT -->
		<div class="tab-pane fade has-bottom-border" id="cs-flip">
			<div class="row">
				<div class="col-md-8">
					<div>
						<h6 class="coachwork-section-header-sm mb-1">For duos wanting to explore reverse mentorship</h6>
						<h5 class="coachwork-section-header-lg">
							<span style="color:#7F8183;">coach</span><span class="font-italic">works</span>
							<span style="color:#000;">flip</span>
						</h5>
						<p class="cw-tab-description">Inverting the traditional mentorship model, reverse mentoring pairs younger employees – as mentors, with executive team members – as mentees to mentor them on areas of strategic, technological, and cultural relevance.</p>
					</div>

					<br>

					<div>
						<h6 class="coachwork-section-header-sm">Ideal For:</h6>
						<p class="cw-tab-description">A mid-senior level leader, with 15+ years of experience – who becomes a mentee</p>
					</div>

					<br>
				</div>

				<div class="col-md-4 pt-md-5">
					<h6 class="coachwork-section-header-sm">Learnings:</h6>
					<ul class="blue-dot-list" style="padding-left:30px;">
						<li class="my-3">Help in leading young teams</li>
						<li class="my-3">Understanding young consumers</li>
						<li class="my-3">Become future-ready</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- COACHWORKS SNIPPET CONTENT -->
		<div class="tab-pane fade pb-3 has-bottom-border" id="cs-snippet">
			<h5 class="coachwork-section-header-lg">
				<span style="color:#7F8183;">coach</span><span class="font-italic">works</span>
				<span>snippets</span>
			</h5>

			<div class="cws-inner-section">
				<h6 class="coachwork-section-header-sm mb-3">Starting your coaching journey? Here are some articles to get you started.</h6>
				<div class="row">
					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link">
							<div class="cw-card-link-img">
								<img src="img/backgrounds/cw1.png" class="img-fluid" />
							</div>
							<p>Coachworks Explained</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link">
							<div class="cw-card-link-img">
								<img src="img/backgrounds/cw2.png" class="img-fluid" />
							</div>
							<p>What coaching is and what it isn’t</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link">
							<div class="cw-card-link-img">
								<img src="img/backgrounds/cw3.png" class="img-fluid" />
							</div>
							<p>4 Mantras to Get your Coaching Journey Right</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link">
							<div class="cw-card-link-img">
								<img src="img/backgrounds/cw5.png" class="img-fluid" />
							</div>
							<p>How to Make the Most of your Coaching Journey</p>
						</a>
					</div>
				</div>
			</div>

			<div class="cws-inner-section cws-is-mustard">
				<h6 class="coachwork-section-header-sm text-light mb-3" style="border-color:#fff;">See how you are progressing in your coaching journey.</h6>
				<div class="row">
					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link white-hover">
							<div class="cw-card-link-img">
								<img src="img/backgrounds/cw6.png" class="img-fluid" />
							</div>
							<p class="text-light">Navigating the ‘Either Or’ Dilemma</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link white-hover">
							<div class="cw-card-link-img">
							<img src="img/backgrounds/cw7.png" class="img-fluid" />
							</div>
							<p class="text-light">Getting out of your own way</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link white-hover">
							<div class="cw-card-link-img">
							<img src="img/backgrounds/cw8.png" class="img-fluid" />
							</div>
							<p class="text-light">4 Thinking Traps Holding You Back</p>
						</a>
					</div>

					<div class="col-6 col-md-3 col-lg-2">
						<a href="" class="coachwork-card-link white-hover">
							<div class="cw-card-link-img">
							<img src="img/backgrounds/cw9.png" class="img-fluid" />
							</div>
							<p class="text-light">Harnessing the Transformative Power of Triggers</p>
						</a>
					</div>
				</div>
			</div>

			<div class="cws-inner-section">
				<h6 class="coachwork-section-header-sm mb-3">Coaching over? What next?</h6>
				<div class="row">

						<div class="col-6 col-md-3 col-lg-2">
							<a href="" class="coachwork-card-link">
								<div class="cw-card-link-img">
								<img src="img/backgrounds/cw10.png" class="img-fluid" />
								</div>
								<p>What’s the Science Behind Self-Help?</p>
							</a>
						</div>

						<div class="col-6 col-md-3 col-lg-2">
							<a href="" class="coachwork-card-link">
								<div class="cw-card-link-img">
								<img src="img/backgrounds/cw11.png" class="img-fluid" />
								</div>
								<p>Transformation Through Self-Coaching</p>
							</a>
						</div>

						<div class="col-6 col-md-3 col-lg-2">
							<a href="" class="coachwork-card-link">
								<div class="cw-card-link-img">
								<img src="img/backgrounds/cw12.png" class="img-fluid" />
								</div>
								<p>What’s the Science Behind Self-Help?</p>
							</a>
						</div>

						<div class="col-6 col-md-3 col-lg-2">
							<a href="" class="coachwork-card-link">
								<div class="cw-card-link-img">
								<img src="img/backgrounds/cw13.png" class="img-fluid" />
								</div>
								<p>Coaching’s Over – What’s Next?</p>
							</a>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-coachworks-team">
	<div class="container">
		<header class="section-header text-center bg-white">Meet the Team</header>
		<div class="row justify-content-center">
			<div class="col-md-6 col-lg-4">
				<div class="vertical-card has-link mx-auto" style="max-width: calc(100% - 30px)">
					<div class="verical-card-image">
						<img src="img/backgrounds/bm-team1.png" class="img-fluid" alt="" />
					</div>
					<div class="vertical-card-content text-center">
						<h5 class="vertical-card-header">Rhea Bulsara Sidhva</h5>
						<strong>Programme Director</strong>
						<a href="">Write to us</a>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="vertical-card has-link mx-auto" style="max-width: calc(100% - 30px)">
					<div class="verical-card-image">
						<img src="img/backgrounds/bm-team2.png" class="img-fluid" alt="" />
					</div>
					<div class="vertical-card-content text-center">
						<h5 class="vertical-card-header">Prakamya Joshi</h5>
						<strong>Programme Coordinator</strong>
						<a href="">Write to us</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include "components/footer.php" ?>