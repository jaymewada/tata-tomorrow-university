<?php include "components/header.php" ?>

<section class="section-winners-list global-header-margin">
	<div class="container">
		<header class="winners-section-header">
			<a href="/tbeg">
				<img src="img/icons/icon-arrow-left-gold.png" width="20" height="18" />
				<span class="ml-1">Back</span>
			</a>
			<h1 class="section-header text-center">JRDQV Winnners</h1>
		</header>

		<div class="row">

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-1.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2021</span>
						<p>JRDQV Winner – Tata Steel</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-2.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2021</span>
						<p>JRDQV Winner – Tata Elxsi</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-3.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2021</span>
						<p>JRDQV Winner – Tata Chemicals</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-4.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2020</span>
						<p>JRDQV Winner – Tata Power</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-5.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2021</span>
						<p>Benchmark Leader – Tata Steel</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-6.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2019</span>
						<p>Benchmark Leader – Tata Consultancy Services</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-7.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2018</span>
						<p>JRDQV Winner - Tata Sponge</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-8.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2013</span>
						<p>JRDQV Winner - TSPDL</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-9.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2013</span>
						<p>JRDQV Winner - Tata Power Delhi Distribution Limited</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-10.png" alt="" />
						<a href="">Download</a>
					</div>
					<div class="winners-card-content">
						<span>2012</span>
						<p>JRDQV - Indian Hotels</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-11.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2011</span>
						<p>JRDQV Winner - Titan (Jewellery)</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-12.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2011</span>
						<p>JRDQV Winner - Tata Steel (Wires)</p>
					</div>
				</div>
			</div>
			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-13.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2011</span>
						<p>JRDQV Winner - Tata Steel (FAMD)</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-14.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2011</span>
						<p>JRDQV Winner - Rallis</p>
					</div>
				</div>
			</div>
			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-15.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2010</span>
						<p>JRDQV Winner - Tata Steel (Tubes)</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-16.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2009</span>
						<p>JRDQV Winner - Tata Power</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-17.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2008</span>
						<p>JRDQV Winner - Telcon</p>
					</div>
				</div>
			</div>
			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-18.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2007</span>
						<p>JRDQV Winner - Tinplate</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-19.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2007</span>
						<p>JRDQV Winner - Tata Metaliks</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-20.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2007</span>
						<p>JRDQV Winner - Tata Chemicals</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-21.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2006</span>
						<p>JRDQV Winner - Titan (Time Products)</p>
					</div>
				</div>
			</div>


			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-22.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2005</span>
						<p>JRDQV Winner - Tata Motors (CVBU)</p>
					</div>
				</div>
			</div>

			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-23.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2004</span>
						<p>JRDQV Winner - Tata Consultancy Services</p>
					</div>
				</div>
			</div>


			<div class="col-6 col-md-4 col-lg-3 mb-40">
				<div class="winners-card">
					<div class="winners-card-img">
						<img src="img/cards/winner-24.png" alt="" />
						<a href="">Download</a>
					</div>

					<div class="winners-card-content">
						<span>2000</span>
						<p>JRDQV Winner - Tata Steel (Steel SBU)</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include "components/footer.php" ?>