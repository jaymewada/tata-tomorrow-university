</main>

<footer class="site-footer">
    <div class="container site-footer-content">
        <div class="row">
            <div class="col-md-4">
                <div>
                    <strong class="footer-section-header">Resources and Group Websites</strong>
                    <ul class="list-unstyled">
                        <li>
                            <a href="">Tata.com</a>
                        </li>
                        <li>
                            <a href="">Tata World</a>
                        </li>
                        <li>
                            <a href="">Newsletter</a>
                        </li>
                        <li>
                            <a href="">Tata Sustainability Group</a>
                        </li>
                        <li>
                            <a href="">“Gyansarovar” - The TMTC Library</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <address>
                    <strong class="footer-section-header">TATA MANAGEMENT TRAINING CENTRE</strong>
                    <p>
                        A divison of Tata Services Limited <br>
                        1, Mangaldas Road<br>
                        Pune - 411001<br>
                        India.
                    </p>
                </address>
            </div>
            <div class="col-md-4 align-self-center text-center">
                <img src="img/logo/site-logo-full.svg" width="180" alt="">
            </div>
        </div>
    </div>
    <div class="footer-endline">
        <ul class="list-inline m-0">
            <li class="list-inline-item">
                <a href="javascript:;" data-fancybox data-src="#site-disclaimer">Disclaimer</a>
            </li>
            <li class="list-inline-item">
                <a href="">Privacy</a>
            </li>
            <li class="list-inline-item">
                <a href="">Cookie Policy</a>
            </li>
            <li class="list-inline-item">
                <a href="">Customize Cookies</a>
            </li>
            <li class="list-inline-item">
                <p class="footer-copyright">© 2022 - Tata Tomorrow University. All rights reserved.</p>
            </li>
        </ul>
    </div>
</footer>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
</button> -->

<!-- LOGIN MODAL -->
<div class="modal fade" id="signIn" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login to your account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body custom-modal-body">
                <form action="">
                    <div class="digital-patterns-input-wrapper">
                        <input type="email" class="digital-patterns-input" placeholder="Email ID">
                    </div>
                    
                    <div class="digital-patterns-input-wrapper mb-1">
                        <input type="password" id="login-password" class="digital-patterns-input" placeholder="Password" style="padding-right:60px;">
                        <button class="btn-show-password"></button>
                        <a href="" class="forget-password-link">Forgot Password</a>
                    </div>

                    <div class="digital-patterns-input-wrapper">
                        <input type="checkbox" name="" id="rememberLoginInfo">
                        <label for="rememberLoginInfo" class="remember-login">Remember Me</label>
                    </div>

                    <div class="digital-patterns-input-wrapper text-center">
                        <button class="btn-cta-navy w-75 mx-auto">Login</button>
                    </div>
                </form>

                <div>
                    <header class="section-header-underlined">
                        <span>New to Tata Tomorrow University?</span>
                    </header>

                    <div class="text-center">
                        <button class="btn-cta-white w-75 mx-auto" id="triggerSignUpModal">Create Account</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- SIGNUP MODAL -->
<div class="modal fade" id="signUpModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body custom-modal-body">
                <form action="">
                    <div class="digital-patterns-input-wrapper" style="padding-right:50px;">
                        <input type="text" class="digital-patterns-input" placeholder="Full Name*">
                        <button class="btn-info-tooltip"></button>
                        <p class="info-tooltip-content">The details you enter here will be used to generate your certificate(s). You will not be able to edit them post account generation</p>
                    </div>

                    <div class="digital-patterns-input-wrapper mb-3">
                        <input type="email" class="digital-patterns-input" placeholder="Email ID (Please register with your official email ID)*">
                        <small class="otp-notice">A validation OTP will be sent to your email ID</small>
                    </div>
                    
                    <div class="digital-patterns-input-wrapper">
                        <input type="password" class="digital-patterns-input" placeholder="Password*">
                    </div>

                    <div class="digital-patterns-input-wrapper">
                        <input type="password" class="digital-patterns-input" placeholder="Confirm Password">

                        <p class="signup-disclaimer">
                             <span style="opacity:.75;"><span class="text-danger">*</span> mandatory fields</span><br>
                            By creating your account you agree to Tata Tomorrow University <a href="">terms of use</a></p>
                    </div>

                    <div class="digital-patterns-input-wrapper text-center">
                        <button class="btn-cta-navy w-75 mx-auto">Create Account</button>
                    </div>
                </form>

                <div>
                    <header class="section-header-underlined">
                        <span>Already have an account?</span>
                    </header>

                    <div class="text-center">
                        <button class="btn-cta-white w-75 mx-auto" id="triggerSignInModal">Sign in</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="disclaimer-modal" id="site-disclaimer">
    <div class="disclaimer">
        <header class="section-header section-header-sm">Disclaimer</header>
        <p>Tata Sons Private Limited retains copyright on all the text, graphics and trademarks displayed on this site. All the text, graphics and trademarks displayed on this site are owned by Tata Sons Private Limited and used under licence by Tata affiliates.</p>
        <p>You may print copies of the information on this site for your personal use and store the files on your computer for personal use. You may not distribute text or graphics to others without the express written consent of Tata Sons and Tata affiliates. Also, you may not, without our permission, copy and distribute this information on any other server, or modify or reuse text or graphics on this or any another system.</p>
        <p>No reproduction of any part of the site may be sold or distributed for commercial gain, nor shall it be modified or incorporated in any other work, publication or site, whether in hard copy or electronic format, including postings to any other site. Tata Sons Private Limited reserves all other rights.</p>
        <p>Neither Tata Sons and Tata affiliates, nor their or their affiliates’ officers, employees or agents shall be liable for any loss, damage or expense arising out of any access to or use of this site or any site linked to it, including, without limitation, any loss of profit, indirect, incidental or consequential loss.</p>
    </div>
</div>

<script src="js/jQuery-3.5.1.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/fancybox.min.js"></script>
<!-- <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/gsap-latest-beta.min.js"></script> -->
<!-- <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollToPlugin3.min.js"></script> -->
<script src="js/slick.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="js/functions.js"></script>
</body>

</html>
