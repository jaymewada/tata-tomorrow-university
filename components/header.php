<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/responsive-992.css">
    <link rel="stylesheet" href="css/responsive-767.css">

    <link rel="prefetch" href="img/backgrounds/bbs-podcast-hover-bg.png">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;1,400;1,500&family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <title>Tata Tomorrow University</title>

</head>

<body>

    <header class="site-header">
        <div class="site-header-container">
            <div class="logo-row">
                <button class="btn-nav-toggler" id="btn-nav-toggler">
                    <i class="material-symbols-outlined" style="font-size:32px;">menu</i>
                </button>

                <a href="index">
                    <img src="./img/logo/site-logo-full.svg" width="120" height="52" class="site-header-logo" alt="Tata Tomorrow University">
                </a>

                <img src="img/logo/tata-logo-dark.png" width="45" class="d-md-none" alt="">
            </div>

            <nav class="site-header-navigation">
                <ul class="site-header-navigaton-list">
                    <li>
                        <a class="header-navigation-link has-child-menu" data-toggle="" href="#learn-now-child-menu" aria-expanded="false" data-trigger="learn-now">
                            Learn Now
                        </a>

                        <div class="child-menu-container">
                            <div class="child-menu-content collapse" id="learn-now-child-menu">
                                <ul class="child-menu-list">
                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#lesten-now-menu" aria-expanded="false" aria-controls="lesten-now-menu">
                                            Listen
                                        </a>

                                        <div class="collapse" id="lesten-now-menu">
                                            <ul class="sub-child-menu-list child-menu-list child-menu-2-column">
                                                <li>
                                                    <a href="brand-builders-secrets" class="sub-child-menu-link">Brand Builders</a>
                                                </li>
                                                <li>
                                                    <a href="wonderful-collective" class="sub-child-menu-link">ONEderful Collective</a>
                                                </li>
                                                <li>
                                                    <a href="leadercraft" class="sub-child-menu-link">Leadercraft</a>
                                                </li>
                                                <li>
                                                    <a href="radio" class="sub-child-menu-link">Radio</a>
                                                </li>
                                                <li>
                                                    <a href="my-hacks" class="sub-child-menu-link">My Hacks</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#watch-now-menu" aria-expanded="false" aria-controls="watch-now-menu">
                                            Watch
                                        </a>
                                        <div class="collapse" id="watch-now-menu">
                                            <ul class="sub-child-menu-list child-menu-list child-menu-2-column">
                                                <li>
                                                    <a href="learning-latitudes" class="sub-child-menu-link">Learning Latitudes</a>
                                                </li>
                                                <li>
                                                    <a href="tata-ethics-conclave-2023" class="sub-child-menu-link">Tata Ethics Conclave</a>
                                                </li>
                                                <li>
                                                    <a href="my-hacks" class="sub-child-menu-link">My Hacks</a>
                                                </li>
                                                <li>
                                                    <a href="vitality" class="sub-child-menu-link">Vitality Playlists & Conversations</a>
                                                </li>
                                                <li>
                                                    <a href="onederful-world" class="sub-child-menu-link">ONEderful World Films</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#interact-now-menu" aria-expanded="false" aria-controls="interact-now-menu">
                                            Interact
                                        </a>
                                        <div class="collapse" id="interact-now-menu">
                                            <ul class="sub-child-menu-list child-menu-list child-menu-2-column">
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Embrace</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Onederful Essentials Gallery</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Excellence Journeys</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Training & Capability Building</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Now New Next</a>
                                                </li>
                                                <li>
                                                    <a href="your-learning-shelf.php" class="sub-child-menu-link">Your Learning Shelf</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a class="header-navigation-link has-child-menu" data-toggle="" href="#learn-at-tmtc-child-menu" aria-expanded="false" data-trigger="learn-now">
                            Learn at TMTC
                        </a>

                        <div class="child-menu-container">
                            <div class="child-menu-content collapse" id="learn-at-tmtc-child-menu">
                                <ul class="child-menu-list">
                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#open-programmes-menu" aria-expanded="false" aria-controls="open-programmes-menu">
                                            Open Programmes
                                        </a>
                                        <div class="collapse" id="open-programmes-menu">
                                            <ul class="sub-child-menu-list child-menu-list">
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Commercial Acumen</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Diversity, Equity & Inclusion</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Innovation</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Strategy</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Communication</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Ethics & Governance</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Leading Self & Others</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Sustainibility</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Digital Acumen</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Human Capital Management</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Marketing, Sales, Customers</a>
                                                </li>
                                                <li>
                                                    <a href="" class="sub-child-menu-link">Supply Chain Management</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#tata-flagship-programmes" aria-expanded="false" aria-controls="tata-flagship-programmes">
                                            Tata Flagship Leadership Development Programmes
                                        </a>

                                        <div class="collapse" id="tata-flagship-programmes">
                                            <ul class="sub-child-menu-list child-menu-list child-menu-2-column">
                                                <li>
                                                    <a href="strategic-leadership-seminar" class="sub-child-menu-link">Tata Group Strategic <br>Leadership Seminar</a>
                                                </li>
                                                <li>
                                                    <a href="executive-leadership-seminar" class="sub-child-menu-link">Tata Group Executive <br>Leadership Seminar</a>
                                                </li>

                                                <li>
                                                    <a href="emerging-leadership-seminar" class="sub-child-menu-link">Tata Group eMerging <br>Leadership Seminar</a>
                                                </li>

                                                <li>
                                                    <a href="bluemint" class="sub-child-menu-link">Blue Mint</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a class="child-menu-link has-sub-child-menu" data-toggle="collapse" href="#custom-solutions-menu" aria-expanded="false" aria-controls="custom-solutions-menu">
                                            Custom Solutions
                                        </a>
                                        <div class="collapse" id="custom-solutions-menu">
                                            <ul class="sub-child-menu-list child-menu-list child-menu-2-column">
                                                <li>
                                                    <a href="custom-programmes" class="sub-child-menu-link">Custom Programmes</a>
                                                </li>
                                                <li>
                                                    <a href="tata-group-induction" class="sub-child-menu-link">The Tata Group Induction</a>
                                                </li>
                                                <li>
                                                    <a href="assesment-development-center" class="sub-child-menu-link">Assessment & <br>Development Centre</a>
                                                </li>
                                                <li>
                                                    <a href="coachworks" class="sub-child-menu-link">Coachworks</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a class="header-navigation-link" href="about.php">About</a>
                    </li>

                    <li>
                        <a class="header-navigation-link has-child-menu" data-toggle="" href="#learning-areneas-child-menu" aria-expanded="false">
                            Learning Arenas
                        </a>

                        <div class="child-menu-container">
                            <div class="child-menu-content collapse" id="learning-areneas-child-menu">
                                <ul class="child-menu-list child-menu-list">
                                    <li>
                                        <a href="living-the-code" class="child-menu-link">Living the Code</a>
                                    </li>

                                    <li>
                                        <a href="onederful-world" class="child-menu-link">ONEderful World</a>
                                    </li>
                                    <li>
                                        <a href="tbeg" class="child-menu-link">TATA Business Excellence Group</a>
                                    </li>
                                    <li>
                                        <a href="vitality" class="child-menu-link">Vitality</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li>
                    <!-- loginModal -->
                    <!-- singupModal -->
                        <a class="header-navigation-link" href="" data-target="#signIn" type="button" data-toggle="modal">Log In</a>
                    </li>

                    <li>
                        <img src="img/logo/tata-logo-dark.png" width="40" height="35" class="d-none d-md-block ml-3" alt="">
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <main class="site-main digital-patterns-main">