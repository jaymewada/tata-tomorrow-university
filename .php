<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin">
	<h1>Communication</h1>
	<img src="img/backgrounds/page-banner.png" class="img-fluid" alt="" />
</section>


<div class="banner-shadow-content text-center">
	Through faculty‐led sessions, reflective activities, interactions with invited speakers from the social sector, you will learn the management and leadership principles required to lead strategically, build a financially healthy organization, Inspire your teams to to deliver high performance, influence stakeholders, demonstrate impact and develop resilience to deal with challenges.
</div>

<section class="section-page-nav">
	<div class="container">
		<ul class="page-navigation-list">
			<li>
				<a class="hash-link" href="#objectives">Objectives</a>
			</li>
			<li>
				<a class="hash-link" href="#programmes">Programmes</a>
			</li>
			<li>
				<a class="hash-link" href="#team">Team</a>
			</li>
			<li>
				<a class="hash-link" href="#resources">Resources</a>
			</li>
		</ul>
	</div>
</section>


<section class="section-objectives section-learning-objectives" id="objectives">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<header class="section-header text-center">Objectives</header>
			</div>
			<div class="col-md-8">
				<p>
					Through faculty‐led sessions, reflective activities, interactions with invited speakers from the social
					sector, you will learn the management and leadership principles required to lead strategically, build a
					financially healthy organization, Inspire your teams to to deliver high performance, influence stakeholders,
					demonstrate impact and develop resilience to deal with challenges.
				</p>
			</div>
		</div>
	</div>
	<img src="img/backgrounds/yellow-triangle-right.svg" class="bg-triangle-right" alt="">
</section>

<section class="section-programmes" id="programmes">
	<div class="container">
		<header class="section-header text-center">Programmes</header>
		<div class="row">
			<div class="col-md-4 mb-30">
				<div class="programme-card">
					<div class="programme-card-image">
						<img src="img/backgrounds/program1.png" class="img-fluid" alt="" />
					</div>
					<div class="programme-card-content">
						<p>Equip yourself with key leadership and enterprise skills of the current and the future.</p>
						<img src="img/backgrounds/university.png" width="120" alt="">
					</div>

					<div class="programme-card-fixed-content">
						<ul class="list-unstyled">
							<li>
								<img src="img/icons/icon-calendar.svg" class="icon" height="21" width="21" alt="">
								<span>1 Sepetember 2002</span>
							</li>

							<li>
								<img src="img/icons/icon-timer.svg" class="icon" height="22" width="22" alt="">
								<span>6 Sessions | 2.5hrs each</span>
							</li>

							<li>
								<img src="img/icons/icon-in-person.svg" class="icon" height="22" width="22" alt="">
								<span>In person</span>
							</li>
						</ul>
						<a class="programme-card-link">See More</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 mb-30">
				<div class="programme-card">
					<div class="programme-card-image">
						<img src="img/backgrounds/program2.png" class="img-fluid" alt="" />
					</div>
					<div class="programme-card-content">
						<p>Communicate more effectively in any context - interpersonal, organisational or external - and with a variety of stakeholders.</p>
					</div>

					<div class="programme-card-fixed-content">
						<ul class="list-unstyled">
							<li>
								<img src="img/icons/icon-calendar.svg" class="icon" height="21" width="21" alt="">
								<span>1 Sepetember 2022</span>
							</li>

							<li>
								<img src="img/icons/icon-timer.svg" class="icon" height="22" width="22" alt="">
								<span>6 Sessions | 2.5hrs each</span>
							</li>

							<li>
								<img src="img/icons/icon-in-person.svg" class="icon" height="22" width="22" alt="">
								<span>Virtual</span>
							</li>
						</ul>
						<a class="programme-card-link">See More</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 mb-30">
				<div class="programme-card">
					<div class="programme-card-image">
						<img src="img/backgrounds/program3.png" class="img-fluid" alt="" />
					</div>
					<div class="programme-card-content">
						<p>Learn how to hire the best fit talent for your team.</p>
					</div>

					<div class="programme-card-fixed-content">
						<ul class="list-unstyled">
							<li>
								<img src="img/icons/icon-calendar.svg" class="icon" height="21" width="21" alt="">
								<span>1 Sepetember 2022</span>
							</li>

							<li>
								<img src="img/icons/icon-timer.svg" class="icon" height="22" width="22" alt="">
								<span>6 Sessions | 2.5hrs each</span>
							</li>

							<li>
								<img src="img/icons/icon-in-person.svg" class="icon" height="22" width="22" alt="">
								<span>Hybrid</span>
							</li>
						</ul>
						<a href="">See More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-team section-learning-teams" id="team">
	<div class="container">
		<header class="section-header text-center text-light">Meet The Team</header>
		<br />

		<div class="row">
			<div class="col-md-10 mx-auto">
				<div class="row">
					<div class="col-md-6 mb-40">
						<div class="team-card">
							<div class="team-card-image">
								<img src="img/backgrounds/team-memeber.png" class="img-fluid" alt="" />
							</div>
							<div class="team-card-content">
								<h4 class="team-card-name">Chandna Sethi</h4>
								<p class="team-card-designation">Programme Director</p>
								<p>
									Chandna is an Organizational & Leadership development professional with an engineering background and a master’s degree from London School of Economics in Organizational Psychology. She brings in strong expertise in curating behavioral interventions which help teams thrive collectively. Surabhi is a Certified practitioner of multiple psychometric tools, a certified Six Sigma professional, with experience in multi-geography, multi-cultural setting.
								</p>
							</div>
							<a href="">Write to us</a>
						</div>
					</div>

					<div class="col-md-6 mb-40">
						<div class="team-card">
							<div class="team-card-image">
								<img src="img/backgrounds/pteam3.png" class="img-fluid" alt="" />
							</div>
							<div class="team-card-content">
								<h4 class="team-card-name">Chandna Sethi</h4>
								<p class="team-card-designation">Programme Director</p>
								<p>
									Zeeba has over 15 years of experience working across e-commerce, retail, and consulting. She has worked on Consumer & Marketing Analytics, Digital Marketing, Product, Retail & Supply Chain Analytics. Her current areas of interest include AI, Computer Vision, Natural Language Processing, IoT, and Web and Social Media Analytics. She is an author of the book ‘Digital Analytics – Data Driven Decision Making in the Digital World’
								</p>
							</div>
							<a href="">Write to us</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-resources" id="resources">
	<div class="container-fluid px-0">
		<header class="section-header text-center">Resources</header>
		<div class="resources-slider">
			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res5.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">3 ways building digital acumen can impact business success</p>
				</a>
			</div>

			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res6.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">Unlocking success in digital transformations</p>
				</a>
			</div>

			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res7.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">Digital transformations: The five talent factors that matter most</p>
				</a>
			</div>

			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res5.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">3 ways building digital acumen can impact business success</p>
				</a>
			</div>

			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res6.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">Unlocking success in digital transformations</p>
				</a>
			</div>

			<div class="px-2">
				<a href="" class="webinar-card" style="max-width:375px;">
					<div class="webinar-card-image">
						<img src="img/backgrounds/res7.png" class="img-fluid" alt="" />
					</div>
					<p class="webinar-card-title">Digital transformations: The five talent factors that matter most</p>
				</a>
			</div>
		</div>

		<div class="col-12 text-right mt-4">
			<img src="img/backgrounds/yellow-rod.svg" width="175" alt="">
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>