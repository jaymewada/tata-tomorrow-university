<?php include "components/header.php" ?>
<section class="section-podcast-banner global-header-margin digital-patterns-banner">
    <img src="img/banners/leadercraft-bannner.png" class="img-fluid" alt="">
</section>

<div class="banner-shadow-content" style="max-width:1000px;">
    <div class="row">
        <div class="col-md-6">
            LeaderCraft is an exciting podcast channel for anyone who wants to be a better leader, irrespective of age, function or career level. Listen in as leaders from within the Tata group and beyond — sportspersons, authors, coaches and many other inspiring personalities — give us their unfiltered story and a first-person account of what worked for them.
        </div>
        <div class="col-md-6">
            What makes this series unique are the simple, practical ideas that you can implement right away. It’s straight from the heart, and gives you direct access to what leaders think, say and do. If you want to add to your leadership quotient,<br>tune in!
        </div>
    </div>
</div>

<section class="section-episode-listing">
    <div class="container">
        <ul class="nav season-listing-navigation season-listing-red">
            <li>
                <button class="active" id="season-1-tab" data-toggle="tab" data-target="#season-1" type="button">season 1</button>
            </li>
            <li>
                <button id="season-2-tab" data-toggle="tab" data-target="#season-2" type="button" role="tab">season 2</button>
            </li>
            <li>
                <button id="season-3-tab" data-toggle="tab" data-target="#season-3" type="button" role="tab">season 3</button>
            </li>
            <li>
                <button id="season-4-tab" data-toggle="tab" data-target="#season-4" type="button" role="tab">season 4</button>
            </li>
        </ul>

        <div class="tab-content">
            <!-- SEASON 1 -->
            <div class="tab-pane fade show active" id="season-1">
                <div class="row justify-content-center">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                        <div class="col-12 col-sm-6 col-md-6 col-xl-4 px-lg-2 mb-50">
                            <div>
                                <a href="leadercraft-episode" class="podcast-card podcast-card-lg podcast-card-red">
                                    <div class="podcast-card-image">
                                        <img src="img/backgrounds/leadercraft-user.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="podcast-card-content-container">
                                        <div class="podcast-card-header">
                                            <img src="img/backgrounds/podcast-leadercraft-logo.svg" alt="">
                                            <span class="se">S1: EP<?php echo $x ?></span>
                                        </div>

                                        <div class="podcast-card-content ">
                                            <h6 class="podcast-card-title" title="The Art of Relationships">The Art of Relationships</h6>

                                            <div>
                                                <strong class="podcast-card-author">Ms. Anita Rajan</strong>
                                                <p class="podcast-card-designation" title="Chairman, Board of Governors, Indian Institute of Technology, Palakkad">Chairman, Board of Governors, Indian Institute of Technology, Palakkad</p>
                                            </div>
                                        </div>

                                        <div class="podcast-card-footer">
                                            <span>Listen Now</span>&nbsp;&nbsp;
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                                <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                    <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
                                                        <circle cx="9.5" cy="9.5" r="9" fill="none" />
                                                    </g>
                                                    <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <!-- SEASON 2 -->
            <div class="tab-pane fade" id="season-2">
                <div class="row justify-content-center">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                        <div class="col-12 col-sm-6 col-md-6 col-xl-4 px-lg-2 mb-50">
                            <div>
                                <a href="leadercraft-episode" class="podcast-card podcast-card-lg podcast-card-red">
                                    <div class="podcast-card-image">
                                        <img src="img/backgrounds/leadercraft-user.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="podcast-card-content-container">
                                        <div class="podcast-card-header">
                                            <img src="img/backgrounds/podcast-leadercraft-logo.svg" alt="">
                                            <span class="se">S1: EP<?php echo $x ?></span>
                                        </div>

                                        <div class="podcast-card-content ">
                                            <h6 class="podcast-card-title" title="The Art of Relationships">Leadership in a Crisis</h6>

                                            <div>
                                                <strong class="podcast-card-author">PB Balaji</strong>
                                                <p class="podcast-card-designation" title="CFO for Tata Motors">CFO for Tata Motors</p>
                                            </div>
                                        </div>

                                        <div class="podcast-card-footer">
                                            <span>Listen Now</span>&nbsp;&nbsp;
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                                <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                    <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
                                                        <circle cx="9.5" cy="9.5" r="9" fill="none" />
                                                    </g>
                                                    <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <!-- SEASON 3 -->
            <div class="tab-pane fade" id="season-3">
                <div class="row justify-content-center">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                        <div class="col-12 col-sm-6 col-md-6 col-xl-4 px-lg-2 mb-50">
                            <div>
                                <a href="leadercraft-episode" class="podcast-card podcast-card-lg podcast-card-red">
                                    <div class="podcast-card-image">
                                        <img src="img/backgrounds/leadercraft-user.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="podcast-card-content-container">
                                        <div class="podcast-card-header">
                                            <img src="img/backgrounds/podcast-leadercraft-logo.svg" alt="">
                                            <span class="se">S3: EP<?php echo $x ?></span>
                                        </div>

                                        <div class="podcast-card-content ">
                                            <h6 class="podcast-card-title" title="Hope is not a Strategy">Hope is not a Strategy</h6>

                                            <div>
                                                <strong class="podcast-card-author">Puneet Chhatwal</strong>
                                                <p class="podcast-card-designation" title="MD & CEO, IHCL ">MD & CEO, IHCL </p>
                                            </div>
                                        </div>

                                        <div class="podcast-card-footer">
                                            <span>Listen Now</span>&nbsp;&nbsp;
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                                <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                    <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
                                                        <circle cx="9.5" cy="9.5" r="9" fill="none" />
                                                    </g>
                                                    <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <!-- SEASON 4 -->
            <div class="tab-pane fade" id="season-4">
                <div class="row justify-content-center">
                    <?php for ($x = 1; $x <= 11; $x++) { ?>
                        <div class="col-12 col-sm-6 col-md-6 col-xl-4 px-lg-2 mb-50">
                            <div>
                                <a href="leadercraft-episode" class="podcast-card podcast-card-lg podcast-card-red">
                                    <div class="podcast-card-image">
                                        <img src="img/backgrounds/leadercraft-user.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="podcast-card-content-container">
                                        <div class="podcast-card-header">
                                            <img src="img/backgrounds/podcast-leadercraft-logo.svg" alt="">
                                            <span class="se">S3: EP<?php echo $x ?></span>
                                        </div>

                                        <div class="podcast-card-content ">
                                            <h6 class="podcast-card-title" title="Hope is not a Strategy">Hope is not a Strategy</h6>

                                            <div>
                                                <strong class="podcast-card-author">Puneet Chhatwal</strong>
                                                <p class="podcast-card-designation" title="Chief Leadership and Diversity Officer">Chief Leadership and Diversity Officer</p>
                                            </div>
                                        </div>

                                        <div class="podcast-card-footer">
                                            <span>Listen Now</span>&nbsp;&nbsp;
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                                                <g id="Group_2913" data-name="Group 2913" transform="translate(-424 -2476)">
                                                    <g id="Ellipse_98" data-name="Ellipse 98" transform="translate(424 2476)" fill="none" stroke="#fff" stroke-width="1">
                                                        <circle cx="9.5" cy="9.5" r="9.5" stroke="none" />
                                                        <circle cx="9.5" cy="9.5" r="9" fill="none" />
                                                    </g>
                                                    <path id="Polygon_19" data-name="Polygon 19" d="M3.091,0,6.183,5.358H0Z" transform="matrix(0.017, 1, -1, 0.017, 437.125, 2482.362)" fill="#fff" />
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>