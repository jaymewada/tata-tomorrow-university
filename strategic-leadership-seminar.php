<?php include "components/header.php" ?>

<section class="section-landing-banner global-header-margin digital-patterns-banner">
	<img src="img/banners/tata-strategic-banner.png" class="d-none d-lg-block" alt="" />
	<img src="img/banners/tata-strategic-mobile-banner.png" class="d-block d-lg-none" alt />
</section>

<div class="banner-shadow-content text-center">
	The Tata Group Strategic Leadership Seminar, developed and delivered in close collaboration with faculty from Harvard
	Business School, aims to fulfil the group’s commitment to continuous learning at the apex level. Our business
	landscape is changing at the speed of thought, which means we must look at the world and ourselves through a new set
	of lenses. This seminar gives an opportunity to participants from diverse sectors, industries, and functions from the
	Tata group to come together to interact, debate, deliberate and, most importantly, learn from some of the best minds
	within the group.
</div>

<section class="section-page-nav">
	<div class="container">
		<ul class="page-navigation-list els">
			<li>
				<a class="hash-link" href="#objectives">Objectives</a>
			</li>
			<li>
				<a class="hash-link" href="#aim">Aim</a>
			</li>
			<li>
				<a class="hash-link" href="#key-outcomes">Key Outcomes</a>
			</li>
			<li>
				<a class="hash-link" href="#learner-profile">Learner Profile</a>
			</li>
			<li>
				<a class="hash-link" href="#structure">Structure</a>
			</li>
			<li>
				<a class="hash-link" href="#faculty">Faculty</a>
			</li>
			<li>
				<a class="hash-link" href="#highlights">Highlights</a>
			</li>
			<li>
				<a class="hash-link" href="#gallery">Gallery</a>
			</li>
			<li>
				<a class="hash-link" href="#testimonials">Testimonials</a>
			</li>
			<li>
				<a class="hash-link" href="#team">Team</a>
			</li>
		</ul>
	</div>
</section>

<section class="section-sls-objectives" id="objectives">
	<img src="img/backgrounds/sls-objective-bg.svg" class="sls-objectives-element-2" data-aos="fade-up" alt="" />
	<div class="container">
		<div class="row">
			<div class="col-md-4 pb-4">
				<header class="section-header mb-4">Objectives</header>
				<img src="img/backgrounds/bars-navy.svg" class="img-fluid" data-aos="fade-in" />
			</div>
			<div class="col-md-8">
				<p>
					Staying true to its core learning engagement and effectiveness, the seminar is designed to cover themes like
					Strategy Thinking, Strategic Execution, Markets and People.
				</p>
				<p>
					The seminar deploys the signature Harvard Case Teaching method, which includes select case studies focused on
					future-ready and future-engaged competencies which are essential to manage the current business environment
					and the growth plans of group companies. These case studies cover Innovation, New Business Models, Creating
					Digital Transformation, Ecosystem, Sustainability, Ethics, Corporate Governance, and Transformation through
					Organisational Leadership.  Individual case preparation and Small Group Case discussions will be critical in
					deepening the learning from the peer network and faculty.
				</p>
				<p>
					In 2022, we added two more components – Executive Coaching Sessions that offers carefully curated coaches and
					a new module on Purpose-Driven Leadership to focus on aligning individual and organisational purpose with
					team’s purpose. Other notable objectives of the seminar include exposing the participants to Value Map
					Projects and Luminary Guest Sessions by senior leaders.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-aims" id="aim">
	<div class="container">
		<div class="row">
			<div class="col-md-4 pb-4">
				<header class="section-header">Aim</header>
				<img src="img/backgrounds/dark-blue-rod.svg" class="img-fluid" data-aos="fade-in" />
			</div>

			<div class="col-md-7">
				<div class="global-content-width-600">
					<ul class="hexagon-list">
						<li>
							<img src="img/icons/sls-aim1.svg" height="55" width="55" alt="" />
							<p>Developing varied perspectives by appreciating complexities of the business environment.</p>
						</li>

						<li>
							<img src="img/icons/sls-aim2.svg" height="55" width="55" alt="" />
							<p>
								Connecting the dots by understanding the interconnectedness and interplay of environmental factors in
								the business ecosystem.
							</p>
						</li>

						<li>
							<img src="img/icons/sls-aim3.svg" height="55" width="55" alt="" />
							<p>
								Creating agility and adaptability in developing organisational capabilities for effective execution of
								business goals.
							</p>
						</li>

						<li>
							<img src="img/icons/sls-aim4.svg" height="55" width="55" alt="" />
							<p>Understanding the role of culture and values in building a sustainable and responsible business.</p>
						</li>

						<li>
							<img src="img/icons/sls-aim5.svg" height="55" width="55" alt="" />
							<p>Sharpening critical personal skills to manage and lead people and teams.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-outcome" id="key-outcomes">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="mb-5 text-center text-md-left">
					<header class="section-header text-light mb-3">Key Outcomes</header>
					<p class="section-description text-light">
						A common thread running throughout the seminar is achieving profitable, <br class="d-none d-lg-block" />
						sustainable business performance. Expected outcomes include:
					</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<ul class="dash-list dash-list-white">
					<li>
						<p>
							Stimulating and challenging the participants to think anew about their roles as strategic and global
							leaders.
						</p>
					</li>
					<li>
						<p>Enhancing the ability to take a holistic approach towards thinking about business strategy.</p>
					</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="dash-list dash-list-white">
					<li>
						<p>Learning about new and innovative business models.</p>
					</li>
					<li>
						<p>Enhancing focus on creating sustainable and profitable businesses, aligned with Tata values.</p>
					</li>
					<li>
						<p>Understanding the nuances of inspirational leadership and change management.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-attend" id="learner-profile">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<header class="section-header text-center text-md-left">
					Who should <br class="d-none d-md-block" />
					attend?
				</header>
				<div data-aos="fade-in">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="170"
						height="63.305"
						viewBox="0 0 170 63.305"
						class="img-fluid mb-4"
					>
						<g id="Group_4765" data-name="Group 4765" transform="translate(-1019.5 -723.3)">
							<line
								id="Line_661"
								data-name="Line 661"
								y2="63.305"
								transform="translate(1020 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_662"
								data-name="Line 662"
								y2="63.305"
								transform="translate(1033 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_663"
								data-name="Line 663"
								y2="63.305"
								transform="translate(1046 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_664"
								data-name="Line 664"
								y2="63.305"
								transform="translate(1059 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_665"
								data-name="Line 665"
								y2="63.305"
								transform="translate(1072 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_666"
								data-name="Line 666"
								y2="63.305"
								transform="translate(1085 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_667"
								data-name="Line 667"
								y2="63.305"
								transform="translate(1098 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_668"
								data-name="Line 668"
								y2="63.305"
								transform="translate(1111 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_669"
								data-name="Line 669"
								y2="63.305"
								transform="translate(1124 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_670"
								data-name="Line 670"
								y2="63.305"
								transform="translate(1137 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_671"
								data-name="Line 671"
								y2="63.305"
								transform="translate(1150 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_694"
								data-name="Line 694"
								y2="63.305"
								transform="translate(1176 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_672"
								data-name="Line 672"
								y2="63.305"
								transform="translate(1163 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
							<line
								id="Line_695"
								data-name="Line 695"
								y2="63.305"
								transform="translate(1189 723.3)"
								fill="none"
								stroke="#183474"
								stroke-width="1"
							/>
						</g>
					</svg>
				</div>
			</div>
			<div class="col-md-8">
				<div class="global-content-width-600">
					<p>
						The seminar is most beneficial for senior executives who have independent P&L responsibilities and are
						tasked with driving change and formulating critical strategic initiatives. The recommended list of
						participants can include current CEOs, heads of large Strategic Business Units (SBUs), heads of functions,
						or senior managers who are likely to take on similar responsibilities in the near future.
					</p>
					<strong>Note:</strong>
					<p>Participation in this seminar is by invitation only.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-pedagogy" id="structure">
	<div class="container">
		<header class="section-header text-center">Structure</header>
		<div class="row justify-content-center">
			<div class="col-md-6 mb-40">
				<div class="shadow-card full-height mb-0 disable-hover-effect">
					<div class="shadow-card-content">
						<div class="shadow-card-header">Methodology</div>
						<p>
							The pedagogy of the seminar will be a combination of executive coaching, case methodology, faculty-led
							discussions, projects, and peer learning. Small group case discussions are an integral part of the
							pedagogy. In addition, there will be interactive guest sessions with senior leadership from the Tata
							group.
						</p>
					</div>
				</div>
			</div>
		</div>

		<img src="img/backgrounds/bars-blue.svg" class="section-background-bars" data-aos="fade-in" />
	</div>
</section>

<section class="section-sls-experts" id="faculty">
	<div class="container" style="max-width: 1024px !important">
		<header class="section-header text-center">Faculty & Experts</header>
		<div class="row justify-content-center">
			<div class="col-md-6 col-lg-5 mb-30">
				<div class="sls-experts-card">
					<img src="img/backgrounds/sls-team1.png" class="img-fluid" alt="" />
					<div class="sls-expert-card-content">
						<h6>Prof. Krishna G. Palepu</h6>
						<span>Ross Graham Walker Professor of Business Administration, Harvard Business School </span>
						<p>
							Prof Palepu joined Harvard Business School (HBS) as faculty in 1983 after a Masters in physics from Andhra
							University, an MBA from IIM Calcutta, and a PhD from MIT. He received an honorary MA from Harvard and an
							honorary doctorate from the Helsinki School of Economics. His current research and teaching focus on the
							globalisation of emerging markets, particularly India and China, and corporate board engagement with
							strategy. He is a co-author of the book, Winning in Emerging Markets– A Road map for Strategy and
							Execution.
						</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-5 mb-30">
				<div class="sls-experts-card">
					<img src="img/backgrounds/sls-team2.png" class="img-fluid" alt="" />
					<div class="sls-expert-card-content">
						<h6>Prof. Das Narayandas</h6>
						<span>Edsel Bryant Ford Professor of Business Administration, Harvard Business School </span>
						<p>
							An alumni of IIT Bombay and IIM Bangalore, Prof Narayandas received a PhD in management from Purdue
							University. He consults with and has developed and executed in-house training programmes for many MNCs,
							including the Tata group, in the areas of B2B marketing, customer satisfaction, loyalty management,
							strategic marketing, pricing, personal selling and sales management. He is also the Senior Associate Dean
							for Harvard Business School (HBS) publishing and Senior Associate Dean, Chair for Executive Education, and
							has co-authored two books and written several articles for reputable publications.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-highlights" id="highlights">
	<img src="img/backgrounds/sls-highlights.png" class="img-fluid d-none d-md-block" alt="" />
	<img src="img/backgrounds/sls-highlights-mobile.png" class="img-fluid d-md-none w-100" alt="" />
</section>

<section class="section-sls-gallery" id="gallery">
	<div class="container-fluid">
		<header class="section-header text-center mb-3">Gallery</header>
		<nav class="nav gallery-tabs" id="nav-tab" role="tablist">
			<button
				class="nav-link active"
				id="nav-els-gallery-image-tab"
				data-toggle="tab"
				data-target="#nav-els-gallery-image"
				type="button"
				role="tab"
				aria-controls="nav-els-gallery-image"
				aria-selected="true"
			>
				Images
			</button>
			<button
				class="nav-link"
				id="nav-els-gallery-video-tab"
				data-toggle="tab"
				data-target="#nav-els-gallery-video"
				type="button"
				role="tab"
				aria-controls="nav-els-gallery-video"
				aria-selected="false"
			>
				Videos
			</button>
		</nav>

		<div class="tab-content gallery-tabs-content" id="nav-tabContent">
			<!-- IMAGE TAB CONTENT -->
			<div class="tab-pane fade show active" id="nav-els-gallery-image" role="tabpanel">
				<div class="gallery-image-slider dots-dashed">
					<div class="gallery-image-slide">
						<img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="" />
					</div>

					<div class="gallery-image-slide">
						<img src="img/backgrounds/bluemint-gallery.png" class="img-fluid" alt="" />
					</div>
				</div>
			</div>
			<!-- VIDEO TAB CONTENT -->
			<div class="tab-pane fade" id="nav-els-gallery-video" role="tabpanel">
				<div class="gallery-image-slider dots-dashed">
					<div>
						<a class="gallery-video-card">
							<img src="img/banners/tata-strategic-mobile-banner.png" class="img-fluid" alt="" />
						</a>
					</div>

					<div>
						<a class="gallery-video-card">
							<img src="img/banners/tata-strategic-mobile-banner.png" class="img-fluid" alt="" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-tesimonial" id="testimonials">
	<div class="container">
		<header class="section-header text-center mb-3">Testimonials</header>
		<div class="testimonial-slider-wrapper">
			<img src="img/icons/quote-dark-blue.svg" class="testimonial-slider-fixed-quote" alt="" />
			<div class="testimonial-slider dots-dashed">
				<div>
					<div class="testimonial-slide">
						<div class="testimonial-slide-content">
							<p class="text-dark">
								I came here reluctantly, but after the seminar, I believe the investment was worth it. It’s almost as if
								they put us in a furnace, and six days later, diamonds were churned out.
							</p>
						</div>
					</div>
				</div>

				<div>
					<div class="testimonial-slide">
						<div class="testimonial-slide-content">
							<p class="text-dark">
								It’s all about purposeful leadership and how we lead as individuals; about business models and win-win
								partnerships; and the value propositions and frameworks.
							</p>
						</div>
					</div>
				</div>

				<div>
					<div class="testimonial-slide">
						<div class="testimonial-slide-content">
							<p class="text-dark">
								The session on Value Maps really brought to life how we could relook at the value we were offering to
								the customer across segments.
							</p>
						</div>
					</div>
				</div>
				<div>
					<div class="testimonial-slide">
						<div class="testimonial-slide-content">
							<p class="text-dark">
								The memories are actually the people, the connections we have made, and some amazing individuals here
								(TMTC) that I have had the opportunity to meet… my experience here has been wonderful!
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-sls-team" id="team">
	<div class="container">
		<header class="section-header text-center bg-white">Meet the Team</header>
		<div class="row justify-content-center">
			<div class="col-md-6 col-lg-4">
				<div class="vertical-card has-link mx-auto" style="max-width: calc(100% - 30px)">
					<div class="verical-card-image">
						<img src="img/backgrounds/bm-team1.png" class="img-fluid" alt="" />
					</div>
					<div class="vertical-card-content text-center">
						<h5 class="vertical-card-header">Radha Ganesh Ram</h5>
						<strong>Seminar Director</strong>
						<a href="">Write to us</a>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="vertical-card has-link mx-auto" style="max-width: calc(100% - 30px)">
					<div class="verical-card-image">
						<img src="img/backgrounds/bm-team2.png" class="img-fluid" alt="" />
					</div>
					<div class="vertical-card-content text-center">
						<h5 class="vertical-card-header">Pearl Mascarenhas</h5>
						<strong>Seminar Manager</strong>
						<a href="">Write to us</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "components/footer.php" ?>
